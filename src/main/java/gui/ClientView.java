package gui;

import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.Image;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.JRadioButton;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JTextPane;

import core.PlayerImpl;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.event.ActionEvent;

/**
 * @author Giorgio
 *
 */
public class ClientView {
	
	
	private static final Logger logger = Logger.getLogger(ClientView.class.getName());

	private JFrame frmCouncilOfFour;
	private JTextField textField;
	private JPasswordField passwordField;
	private JTextField ipPort;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClientView window = new ClientView();
					window.frmCouncilOfFour.setVisible(true);
				} catch (Exception e) {
					logger.log(Level.SEVERE, "Something went wrong", e);
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ClientView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCouncilOfFour = new JFrame();
		frmCouncilOfFour.setResizable(false);
		frmCouncilOfFour.setBounds(new Rectangle(0, 23, 1280, 742));
		frmCouncilOfFour.getContentPane().setFont(new Font("Athelas", Font.PLAIN, 13));
		frmCouncilOfFour.setFont(new Font("Athelas", Font.PLAIN, 13));
		frmCouncilOfFour.setTitle("Council of Four");
		frmCouncilOfFour.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ImageIcon login = new ImageIcon("client/template/LOGIN/Schermata Login.png");
		Image image = login.getImage();
		Image newimg = image.getScaledInstance(1280, 720, java.awt.Image.SCALE_SMOOTH);
		login = new ImageIcon(newimg);

		JPanel southPanel = new JPanel();
		southPanel.setBorder(null);
		southPanel.setOpaque(false);
		frmCouncilOfFour.getContentPane().add(southPanel, BorderLayout.SOUTH);
		FlowLayout fl_southPanel = new FlowLayout(FlowLayout.RIGHT, 150, 15);
		southPanel.setLayout(fl_southPanel);
		
				JPanel registrationPanel = new JPanel();
				southPanel.add(registrationPanel);
				registrationPanel.setOpaque(false);
						registrationPanel.setLayout(new GridLayout(0, 1, 0, 0));
						
						JTextPane txtpnUsername = new JTextPane();
						txtpnUsername.setEditable(false);
						txtpnUsername.setFont(new Font("Dialog", Font.BOLD, 15));
						txtpnUsername.setOpaque(false);
						txtpnUsername.setText("Username");
						registrationPanel.add(txtpnUsername);
				
						textField = new JTextField();
						registrationPanel.add(textField);
						textField.setColumns(10);
								
								JTextPane txtpnPassword = new JTextPane();
								txtpnPassword.setEditable(false);
								txtpnPassword.setFont(new Font("Dialog", Font.BOLD, 15));
								txtpnPassword.setOpaque(false);
								txtpnPassword.setText("Password");
								registrationPanel.add(txtpnPassword);
						
								passwordField = new JPasswordField();
								registrationPanel.add(passwordField);

		JPanel registerLoginPanel = new JPanel();
		registerLoginPanel.setOpaque(false);
		southPanel.add(registerLoginPanel);
		registerLoginPanel.setLayout(new GridLayout(6, 1, 0, 0));
		
		JPanel panel = new JPanel();
		panel.setOpaque(false);
		registerLoginPanel.add(panel);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 5));
		
		JTextPane txtpnServersIpAddress = new JTextPane();
		panel.add(txtpnServersIpAddress);
		txtpnServersIpAddress.setOpaque(false);
		txtpnServersIpAddress.setFont(new Font("Dialog", Font.PLAIN, 13));
		txtpnServersIpAddress.setEditable(false);
		txtpnServersIpAddress.setText("Server's IP address ");
		
		ipPort = new JTextField();
		panel.add(ipPort);
		ipPort.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		panel_1.setOpaque(false);
		registerLoginPanel.add(panel_1);
		panel_1.setLayout(new FlowLayout(FlowLayout.RIGHT, 0, 5));
		
		JTextPane txtpnServersPort = new JTextPane();
		txtpnServersPort.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtpnServersPort.setOpaque(false);
		txtpnServersPort.setText("Server's Port");
		txtpnServersPort.setEditable(false);
		panel_1.add(txtpnServersPort);
		
		textField_1 = new JTextField();
		panel_1.add(textField_1);
		textField_1.setColumns(10);

		JTextPane txtpnDoYouWant = new JTextPane();
		txtpnDoYouWant.setEditable(false);
		txtpnDoYouWant.setFont(new Font("Dialog", Font.PLAIN, 13));
		registerLoginPanel.add(txtpnDoYouWant);
		txtpnDoYouWant.setText("Do you want to Register or Login?");
		txtpnDoYouWant.setOpaque(false);

		JButton registerButton = new JButton("Register");
		registerButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		registerButton.setFont(new Font("Athelas", Font.PLAIN, 13));
		registerLoginPanel.add(registerButton);

		JButton loginButton = new JButton("Login\n");
		loginButton.setFont(new Font("Athelas", Font.PLAIN, 13));
		registerLoginPanel.add(loginButton);

		JPanel socketRmiPanel = new JPanel();
		registerLoginPanel.add(socketRmiPanel);
		socketRmiPanel.setOpaque(false);

		JRadioButton socketRadioButton = new JRadioButton("Socket");
		socketRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}

		});
		socketRadioButton.setFont(new Font("Athelas", Font.PLAIN, 13));
		socketRmiPanel.add(socketRadioButton);

		JRadioButton rmiRadioButton = new JRadioButton("RMI");
		rmiRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}

		});
		rmiRadioButton.setFont(new Font("Athelas", Font.PLAIN, 13));
		socketRmiPanel.add(rmiRadioButton);
		rmiRadioButton.setAlignmentX(Component.CENTER_ALIGNMENT);

		JLabel thumb = new JLabel();
		frmCouncilOfFour.getContentPane().add(thumb, BorderLayout.NORTH);
		thumb.setIcon(login);
		thumb.setSize(1080, 720);
	}

}
