package core;

public class Nobility {

	private Bonus[] bonus;

	/**
	 * Constructor of Nobility
	 * 
	 * @param bonus
	 *            is an array of bonuses that you want to add in the nobility
	 */
	public Nobility(Bonus[] bonus) {
		this.bonus = bonus;
	}

	/**
	 * Get the bonus of a specified level
	 * 
	 * @param level
	 *            is the level of the nobility
	 * @return the bonus in the position of the level
	 */
	public Bonus getBonusOfTheLevel(int level) {
		return bonus[level];
	}

	/**
	 * 
	 * @return bonus
	 */
	public Bonus[] getBonus() {
		return this.bonus;
	}

}
