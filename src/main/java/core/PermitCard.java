package core;

public class PermitCard {

	private String cities;
	private Bonus bonus;
	private Region region;
	private boolean used = false;

	/**
	 * Constructor for PermitCard
	 * 
	 * @param cities
	 *            is a String of the cities
	 * @param bonus
	 *            of the permitCard
	 * @param region
	 *            of the permitCard
	 */
	public PermitCard(String cities, Bonus bonus, Region region) {
		this.cities = cities;
		this.bonus = bonus;
		this.region = region;
	}

	/**
	 * 
	 * @return the bonus obtained from buying the permit
	 */
	public Bonus getBonus() {
		return bonus;
	}

	/**
	 * 
	 * @return the string of cities where it is possible to build
	 */
	public String getCities() {
		return cities;

	}

	/**
	 * 
	 * @return the region of the permit
	 */
	public Region getRegion() {
		return region;

	}

	/**
	 * 
	 * @return used
	 */
	public boolean isUsed() {
		return this.used;
	}

	/**
	 * Set used true
	 */
	public void setUsed() {
		this.used = true;
	}

}
