package core;

import java.util.HashMap;

public class King {

	private City location;
	private Council council;

	/**
	 * Constructor for King
	 * 
	 * @param location
	 *            is the city of the king, aka capital
	 */
	public King(City location) {
		this.location = location;
		Color colorOfTheKing = new Color(location);
		location.setColor(colorOfTheKing);
		council = new Council();
	}

	/**
	 * This method generate an HashMap of the state of the King
	 * 
	 * @return buff
	 */
	public HashMap<String, String> getState() {
		HashMap<String, String> buff = new HashMap<>();
		buff.putAll(this.council.getState());
		buff.put("locationOfTheKing", Character.toString(this.location.getCharOfTheCity()));
		return buff;
	}

	/**
	 * 
	 * @param location
	 *            set the city of the king
	 */
	private void setLocation(City location) {
		this.location = location;
		return;
	}

	/**
	 * 
	 * @return location
	 */
	public City getLocation() {
		return this.location;
	}

	/**
	 * 
	 * @return council
	 */
	public Council getCouncil() {
		return this.council;
	}

	/**
	 * This method move the king from a city to another
	 * 
	 * @param city:
	 *            the city where it will be the king
	 */
	public void move(City city) {
		setLocation(city);
		return;
	}

}
