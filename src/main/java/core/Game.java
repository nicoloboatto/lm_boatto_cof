package core;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import server.GameServer;
import server.UserHandler;

public class Game implements Runnable {

	private static final Logger logger = Logger.getLogger(Game.class.getName());
	private final PlayerImpl[] players;
	public final int maximumEmporium;

	private int onlinePlayers;
	private int moving = 0;
	private int acquisitionTurn = 0;
	private int turnNumber = 0;
	private Lock lock = new ReentrantLock();
	private Condition moveMade = lock.newCondition();
	private HashMap<String, String> currentStateUpdate = new HashMap<>();
	private Market market = new Market();
	private boolean inMarketCollectionRound = false;
	private boolean inMarketAcquisitionRound = false;
	private boolean lastTurn = false;

	private Map map;
	private PlayerImpl winner = null;
	private boolean over = false;

	private static final String MOVING_STRING = "moving";
	public static final String PLAYER_NAME_LOWERCASE = "player";
	public static final String TURN_NUMBER = "turnNumber";
	public static final String STATE = "state";
	public static final String POSSIBLE_MOVES = "possibleMoves";

	/**
	 * Inputs the array of UserHandlers and the configuration of the game and
	 * takes care of assigning a player to each UserHandler
	 * 
	 * 
	 *
	 * @param userHandlers
	 *            are the handlers for the user
	 * @param configuration
	 *            is the properties of the game
	 */
	public Game(UserHandler[] userHandlers, Properties configuration) {
		logger.info("Entered Game constructor");
		this.onlinePlayers = userHandlers.length;
		this.players = new PlayerImpl[userHandlers.length];
		this.maximumEmporium = Integer.parseInt(configuration.getProperty("maxEmporiums"));
		try {

			for (int i = 0; i != userHandlers.length; i++) {
				logger.info("Creating player " + i);
				this.players[i] = new PlayerImpl(i);
				this.players[i].setGame(this);
				if (userHandlers[i] != null)
					currentStateUpdate.put(PLAYER_NAME_LOWERCASE + i + "Username", userHandlers[i].getUsername());
				else
					currentStateUpdate.put(PLAYER_NAME_LOWERCASE + i + "Username", "placeHolder");

			}
		} catch (RemoteException e) {
			logger.log(Level.INFO, "There was an error creating the players", e);
		}
		logger.info("Generating map");
		this.map = new Map(configuration);
		logger.info("Map generated");
		currentStateUpdate.putAll(generateInitialGameState(configuration));
		logger.info("Gamestate generated");
		logger.info("Setting Handlers");
		for (int j = 0; j < players.length; j++) {
			if (userHandlers[j] != null) {
				players[j].setHandler(userHandlers[j]);
			}

		}

		GameServer.addGame(this);

	}

	/**
	 * Manages the top-level run of the game, updates the players at the
	 * beginning and then lets them play until the end.
	 * 
	 * When the game is over the players that are still online are sent back to
	 * the lobby after 20 seconds.
	 * 
	 */
	@Override
	public void run() {
		updatePlayers();
		while (!lastTurn) {
			takeTurns();
			if (!lastTurn && !over)
				market();

		}
		lastTurn();

		try {
			Thread.sleep(20000);
		} catch (Exception e) {
			logger.log(Level.WARNING, "Interrupted!", e);
			Thread.currentThread().interrupt();
		}

		for (PlayerImpl playerImpl : players) {
			if (!playerImpl.isDisconnected()) {
				UserHandler user = playerImpl.getHandler();
				if (user != null)
					GameServer.addUser(user);

			}
		}
		GameServer.removeGame(this);
	}

	/**
	 * Manages the last turn of the game, which starts after a player reaches
	 * the maximum number of emporiums and ends with the player right before
	 * him/her.
	 * 
	 * 
	 */
	private void lastTurn() {

		for (int i = 0; i < players.length && !over; i++) {

			while (moving == i) {

				waitForPlayerToMove(GameServer.getTimeLimit());

				if (!players[moving].hasAction() || players[moving].isDisconnected()) {
					nextTurn();
					if (i == (players.length - 1)) {
						lastTurn = false;
						over = true;
					}
				}

				updatePlayers();

			}

		}

	}

	/**
	 * Manages the regular flow of turns, checking what moves the player can do
	 * before passing on to the next player.
	 *
	 */
	private void takeTurns() {
		moving = 0;

		for (int i = 0; i < players.length && !lastTurn && !over; i++) {

			while (moving == i) {

				waitForPlayerToMove(GameServer.getTimeLimit());

				if (!players[moving].hasAction() || players[moving].isDisconnected()) {
					nextTurn();
				}
				updatePlayers();

			}

		}

	}

	/**
	 * Takes care of the flow of the market, asking players for their offers and
	 * then asking in a random order which offer they want to buy
	 *
	 */
	private void market() {

		for (int i = 0; i < players.length; i++) {

			waitForPlayerToMove(GameServer.getTimeLimit());

			nextMarketCollectionTurn();

			updatePlayers();

		}

		for (acquisitionTurn = 0; acquisitionTurn < players.length; acquisitionTurn++) {

			waitForPlayerToMove(GameServer.getTimeLimit());

			nextMarketAcquisitionTurn();

			updatePlayers();
		}
		market.clear();
	}

	/**
	 * Makes all the necessary changes to get to the next turn and checks if
	 * it's time for the market
	 * 
	 *
	 */
	private void nextTurn() {

		moving++;
		if (moving < players.length) {

			players[moving].startTurn();
			currentStateUpdate.put(MOVING_STRING, Integer.toString(moving));

		} else {

			moving = 0;
			currentStateUpdate.put(MOVING_STRING, Integer.toString(moving));
			if (!lastTurn)
				inMarketCollectionRound = true;

		}

		turnNumber++;
		currentStateUpdate.put(TURN_NUMBER, Integer.toString(turnNumber));

	}

	/**
	 * Makes sure the next player can make an offer and then randomly picks a
	 * new turn to make them buy the offers
	 */
	private void nextMarketCollectionTurn() {

		moving++;

		if (moving < players.length) {

			currentStateUpdate.put(MOVING_STRING, Integer.toString(moving));

		} else {
			inMarketCollectionRound = false;
			inMarketAcquisitionRound = true;
			moving = (int) (Math.random() * (players.length - (0.5)));
			currentStateUpdate.putAll(market.getMarketState());
			currentStateUpdate.put(MOVING_STRING, Integer.toString(moving));
		}
		currentStateUpdate.putAll(market.getMarketState());
		turnNumber++;
		currentStateUpdate.put(TURN_NUMBER, Integer.toString(turnNumber));

	}

	/**
	 * Makes sure every player can buy an offer in the random turn
	 */
	private void nextMarketAcquisitionTurn() {

		moving++;
		if (acquisitionTurn == players.length - 1) {

			inMarketAcquisitionRound = false;
			moving = -1;
			nextTurn();
		}
		moving %= players.length;
		currentStateUpdate.putAll(market.getMarketState());
		currentStateUpdate.put(MOVING_STRING, Integer.toString(moving));
		turnNumber++;
		currentStateUpdate.put(TURN_NUMBER, Integer.toString(turnNumber));

	}

	/**
	 * Waits for the player to make a move or for the Turn Timer to expire.
	 * 
	 * 
	 * @param milliseconds
	 *            the wait time in milliseconds
	 */
	private void waitForPlayerToMove(int milliseconds) {

		if (players[moving].isDisconnected()) {
			currentStateUpdate.put(PLAYER_NAME_LOWERCASE + moving + "State", "OFFLINE");
			return;
		}

		logger.info("Acquiring Game Lock");
		lock.lock();
		logger.info("CLI lobby lock acquired, now awaiting...");
		currentStateUpdate.clear();
		logger.info("Cleared latest update");
		boolean updated = false;
		while (!updated) {
			try {
				new Timer().schedule(new TurnTimer(this), milliseconds);
				moveMade.await();
				currentStateUpdate.putAll(players[moving].getState());
				updated = true;
			} catch (InterruptedException e) {
				logger.log(Level.INFO, "Interrupted!", e);
				Thread.currentThread().interrupt();
			}
		}

		if (players[moving].getEmporiumNumber() == maximumEmporium)
			lastTurn = true;

		logger.info("CLI lobby now awake!");
		lock.unlock();
	}

	/**
	 * 
	 * Takes care of adding final information about the turn to the
	 * currentStateUpdate and sending each player a copy of it, skipping
	 * disconnected players
	 * 
	 * The information added are:
	 * 
	 * Moving player available moves, including market collection and
	 * acquisition
	 *
	 *
	 */
	private void updatePlayers() {

		if (lastTurn) {
			currentStateUpdate.put(STATE, "LAST_TURN");
		} else if (over) {
			decideWinner();
			currentStateUpdate.put(STATE, "OVER");
			currentStateUpdate.put(POSSIBLE_MOVES, "O");
		}

		for (int i = 0; i < players.length; i++) {

			currentStateUpdate.put("hand", players[i].getHand());
			currentStateUpdate.put("number", Integer.toString(i));
			if (i == moving) {

				currentStateUpdate.putAll(players[moving].getState());
				if (inMarketAcquisitionRound) {
					currentStateUpdate.put(POSSIBLE_MOVES, "M");
				} else if (inMarketCollectionRound) {
					currentStateUpdate.put(POSSIBLE_MOVES, "C");
				} else if (!over) {
					String possibleMoves = players[i].calculatePossibleMoves();
					currentStateUpdate.put(POSSIBLE_MOVES, possibleMoves);
				}

			}
			if (!players[i].isDisconnected())
				players[i].update(currentStateUpdate);
			currentStateUpdate.remove(POSSIBLE_MOVES);
		}

	}

	/**
	 * Returns the state of the market
	 * 
	 * @return marketState the current state of the market
	 */
	public HashMap<String, String> getMarketState() {
		return market.getMarketState();
	}

	/**
	 * 
	 * @return the number of the player who is moving at the moment
	 */
	public int getMoving() {
		return moving;
	}

	/**
	 * 
	 * @param offerNumber
	 *            the number of the offer from the market
	 * @return the offer is bought from the market
	 * @throws InvalidMoveException
	 *             in case the player specifies an incorrect offer number
	 */
	public Offer buyOffer(int offerNumber) throws InvalidMoveException {

		return market.buyOffer(offerNumber);
	}

	/**
	 * 
	 * @param offerNumber
	 *            the number of the offer from the market
	 * @return the price of the specified offer
	 * @throws InvalidMoveException
	 *             in case the offer does not exist
	 */
	public int getOfferPrice(int offerNumber) throws InvalidMoveException {
		return market.getOfferPrice(offerNumber);
	}

	/**
	 * 
	 * @param offer
	 *            adds an offer to the market
	 */
	public void addOffer(Offer offer) {
		market.addOffer(offer);
	}

	/**
	 * Decides the winner by maximum Victory points, maximum Helpers and maximum
	 * Political Cards and checks if there is a player with maximum Permit
	 * Cards, if there is it gives 3 victory points to him. It then check the
	 * nobility level and if there is a player with the most Nobility it gives
	 * him 5 victory points, if there are a second or seconds it gives them 2
	 * victory points, if there is more than one player with maximum Nobility it
	 * gives them 5 victory points and none to the seconds
	 */
	public void decideWinner() {
		int[] bonus = { 0, 0, 3, 0, 0, 0, 0, 0, 0 };
		Bonus victoryPoints = new Bonus(bonus);
		giveVictoryPointsByNobility();
		if (checkPermitCard()) {
			findPlayerWithMaxPermitCard().activateBonus(victoryPoints);
			currentStateUpdate.putAll(findPlayerWithMaxPermitCard().getState());
		}
		if (checkVictory())
			this.winner = findPlayerWithMaxVictory();
		else {
			if (checkHelpers())
				this.winner = findPlayerWithMaxHelpers();
			else {
				if (checkPoliticalCards())
					this.winner = findPlayerWithMaxPoliticalCards();
			}
		}
		if (this.winner != null)
			currentStateUpdate.put("winner", winner.getUsername());
		else
			currentStateUpdate.put("winner", "Draw");
	}

	/**
	 * Check if there are two or more players with the same victory points
	 *
	 * @return true if there is one, false otherwise
	 */
	private boolean checkVictory() {
		int maxVictory = getMaxVictory();
		int count = 0;
		for (PlayerImpl x : this.players)
			if (maxVictory == x.getVictory())
				count++;
		return count == 1;
	}

	/**
	 *
	 * @return maxVictory the max of the victory points
	 */
	private int getMaxVictory() {
		int maxVictory = 0;
		for (PlayerImpl x : this.players)
			if (x.getVictory() > maxVictory)
				maxVictory = x.getVictory();
		return maxVictory;
	}

	/**
	 * Check if there are two or more players with the same victory points have
	 * the same number of helpers
	 *
	 * @return true if there is one, false otherwise
	 */
	private boolean checkHelpers() {
		int maxVictory = getMaxVictory();
		int maxHelpers = getMaxHelpers(maxVictory);
		int count = 0;
		for (PlayerImpl x : this.players)
			if (maxVictory == x.getVictory() && maxHelpers == x.getHelpers())
				count++;
		return count == 1;
	}

	/**
	 * Get the max helpers of the players that have same maxVictory
	 * 
	 * @param maxVictory
	 *            is the max of the victory points that a player have
	 * @return maxHelpers is the max of the helpers points that a player have
	 */
	private int getMaxHelpers(int maxVictory) {
		int maxHelpers = 0;
		for (PlayerImpl x : this.players)
			if (maxVictory == x.getVictory() && maxHelpers < x.getHelpers())
				maxHelpers = x.getHelpers();
		return maxHelpers;
	}

	/**
	 * Check if there are two or more players with the same number of
	 * politicalCards with the same helpers with the same victory points
	 *
	 * @return true if there is one, false otherwise
	 */
	private boolean checkPoliticalCards() {
		int maxVictory = getMaxVictory();
		int maxHelpers = getMaxHelpers(maxVictory);
		int maxPoliticalCard = getMaxPoliticalCards(maxVictory, maxHelpers);
		int count = 0;
		for (PlayerImpl x : this.players)
			if (maxVictory == x.getVictory() && maxHelpers == x.getHelpers()
					&& maxPoliticalCard == x.getPoliticalCards().size())
				count++;
		return count == 1;
	}

	/**
	 * Get the max politicalCard of the players that have same maxVictory and
	 * the same maxHelpers
	 *
	 * @param maxVictory
	 *            is the higher number of the victory that a player has
	 * @param maxHelpers
	 *            is the higher number of the helpers that a player has
	 * @return the max of the politicalCards
	 */
	private int getMaxPoliticalCards(int maxVictory, int maxHelpers) {
		int maxPoliticalCards = 0;
		for (PlayerImpl x : this.players)
			if (maxVictory == x.getVictory() && maxHelpers == x.getHelpers()
					&& maxPoliticalCards < x.getPoliticalCards().size())
				maxPoliticalCards = x.getPoliticalCards().size();
		return maxPoliticalCards;
	}

	/**
	 * Find the player with maxVictory, its supposed that there is only one
	 * player with maxVictory
	 *
	 * @return the player with maxVictory
	 */
	private PlayerImpl findPlayerWithMaxVictory() {
		int maxVictory = getMaxVictory();
		for (PlayerImpl x : this.players)
			if (maxVictory == x.getVictory())
				return x;
		return null;
	}

	/**
	 * Find the player with maxHelpers with maxVictory, its supposed that there
	 * is only one player with maxHelper and maxVictory points
	 *
	 * @return the player with maxHelpers
	 */
	private PlayerImpl findPlayerWithMaxHelpers() {
		int maxVictory = getMaxVictory();
		int maxHelpers = getMaxHelpers(maxVictory);
		for (PlayerImpl x : this.players)
			if (maxVictory == x.getVictory() && maxHelpers == x.getHelpers())
				return x;
		return null;
	}

	/**
	 * Find the player with maxPoliticalCards with maxHelpers with maxVictory,
	 * its supposed that there is only one player with maxPoliticalCards with
	 * maxHelper and maxVictory points
	 *
	 * @return the player with maxPoliticalCards
	 */
	private PlayerImpl findPlayerWithMaxPoliticalCards() {
		int maxVictory = getMaxVictory();
		int maxHelpers = getMaxHelpers(maxVictory);
		int maxPoliticalCards = getMaxPoliticalCards(maxVictory, maxHelpers);
		for (PlayerImpl x : this.players)
			if (maxVictory == x.getVictory() && maxHelpers == x.getHelpers()
					&& maxPoliticalCards == x.getPoliticalCards().size())
				return x;
		return null;
	}

	/**
	 * Check if there are two or more players with the same max nobility level
	 *
	 * @return true if there is one, false otherwise
	 */
	private boolean checkNobilityFirst() {
		int maxNobility = getMaxNobilityLevel();
		int count = 0;
		for (PlayerImpl x : this.players)
			if (maxNobility == x.getNobility())
				count++;
		return count == 1;
	}

	/**
	 * Search if there are two or more player with the same second nobility
	 * level and gave them 2 victory points
	 * 
	 * @param bonus
	 *            is the bonus to add for the seconds
	 */
	private void giveVictoryPointToTheSeconds(Bonus bonus) {
		int maxNobility = getMaxNobilityLevel();
		int secondMaxNobility = 0;
		for (PlayerImpl x : this.players)
			if (x.getNobility() != maxNobility && x.getNobility() > secondMaxNobility)
				secondMaxNobility = x.getNobility();
		for (PlayerImpl x : this.players)
			if (x.getNobility() == secondMaxNobility) {
				x.activateBonus(bonus);
				currentStateUpdate.putAll(x.getState());
			}
	}

	/**
	 *
	 * @return the max of the nobility level
	 */
	private int getMaxNobilityLevel() {
		int maxNobility = 0;
		for (PlayerImpl x : this.players)
			if (x.getNobility() > maxNobility)
				maxNobility = x.getNobility();
		return maxNobility;
	}

	/**
	 * Find the player with maxNobility level, its supposed that there is only
	 * one player with maxNobility
	 *
	 * @return the player with maxNobility
	 */
	private PlayerImpl findPlayerWithMaxNobilityLevel() {
		int maxNobility = getMaxNobilityLevel();
		for (PlayerImpl x : this.players)
			if (x.getNobility() == maxNobility)
				return x;
		return null;
	}

	/**
	 * Give 5 victory points to the players with maxNobility level
	 *
	 * @param bonus
	 *            is the bonus that you want to add to the players with
	 *            maxNobility level
	 */
	private void giveVictoryPointToTheFirsts(Bonus bonus) {
		int maxNobility = getMaxNobilityLevel();
		for (PlayerImpl x : this.players)
			if (x.getNobility() == maxNobility) {
				x.activateBonus(bonus);
				currentStateUpdate.putAll(x.getState());
			}
	}

	/**
	 * At the end of the game if there is a player with maxNobility level give
	 * him 5 victory points, and 2 victory points at the second. If there are
	 * two or more player that are second give them 2 victory points. If there
	 * are two or more with the same maxNobility level give 5 victory points to
	 * them and nothing to the second(s).
	 * 
	 * (Public for testing reasons)
	 */
	public void giveVictoryPointsByNobility() {
		int[] bonusFirst = { 0, 0, 5, 0, 0, 0, 0, 0, 0 };
		int[] bonusSecond = { 0, 0, 2, 0, 0, 0, 0, 0, 0 };
		Bonus first = new Bonus(bonusFirst);
		Bonus second = new Bonus(bonusSecond);
		if (checkNobilityFirst()) {
			findPlayerWithMaxNobilityLevel().activateBonus(first);
			currentStateUpdate.putAll(findPlayerWithMaxNobilityLevel().getState());
			giveVictoryPointToTheSeconds(second);
		} else {
			giveVictoryPointToTheFirsts(first);
		}
	}

	/**
	 * Check if there are two or more players with the same maxPermitCard
	 *
	 * @return true if there is only one, false otherwise
	 */
	private boolean checkPermitCard() {
		int count = 0;
		int maxPermitCard = getMaxPermitCard();
		for (PlayerImpl x : this.players)
			if (x.getSizePermitCard() == maxPermitCard)
				count++;
		return count == 1;
	}

	/**
	 *
	 * @return maxPermitCard tha maximum number of permit cards among the
	 *         players
	 */
	private int getMaxPermitCard() {
		int maxPermitCard = 0;
		for (PlayerImpl x : this.players)
			if (x.getSizePermitCard() > maxPermitCard)
				maxPermitCard = x.getSizePermitCard();
		return maxPermitCard;
	}

	/**
	 * Find the player that have maxPermitCard, its supposed that there is only
	 * one player with maxPermitCard
	 *
	 * @return the player
	 */
	private PlayerImpl findPlayerWithMaxPermitCard() {
		int maxPermitCard = getMaxPermitCard();
		for (PlayerImpl x : this.players)
			if (x.getSizePermitCard() == maxPermitCard)
				return x;
		return null;
	}

	/**
	 * Check how many emporium are in a specified city
	 *
	 * @param city
	 *            that you want to search
	 * @return the number of emporium in the city
	 */
	public int checkHowManyHelpersForEmporium(City city) {
		int count = 0;
		for (PlayerImpl x : this.players)
			for (City check : x.getEmporiums())
				if (check == city)
					count++;
		return count;
	}

	/**
	 * This method returns a new HashMap with: the cities, the nobility levels
	 * bonuses, the prizes, the colors of the councils, the colors of the king,
	 * the numberOfPrize, the numberOfNobilityLevel, the numberOfCities and the
	 * information of the visible permitCards and also all the information about
	 * the players
	 *
	 * @param configuration
	 *            the game configuration
	 * @return initialState is an HashMap with all the information needed by the
	 *         client
	 */
	public HashMap<String, String> generateInitialGameState(Properties configuration) {
		String key;
		String connection = "Connection";
		String bonus = "Bonus";
		String color = "Color";
		HashMap<String, String> initialState = new HashMap<>();

		initialState.put(STATE, "PLAYING");
		initialState.put("message", "");
		key = "numberOfPrize";
		initialState.put(key, configuration.getProperty(key));
		key = "numberOfNobilityLevel";
		initialState.put(key, configuration.getProperty(key));
		key = "numberOfCities";
		initialState.put(key, configuration.getProperty(key));

		for (PlayerImpl playerImpl : players) {
			initialState.putAll(playerImpl.getState());
		}

		initialState.put("commandLineMap", map.generateCommandLineMap(configuration));

		initialState.put("numberOfPlayers", Integer.toString(players.length));

		for (int i = 0; i != Integer.parseInt(configuration.getProperty("numberOfCities")); i++) {
			key = "city" + (char) (i + 65) + connection;
			initialState.put(key, configuration.getProperty(key));
			key = "city" + (char) (i + 65) + bonus;
			initialState.put(key, configuration.getProperty(key));
			key = "city" + (char) (i + 65) + color;
			initialState.put(key, configuration.getProperty(key));
		}
		for (int i = 0; i != Integer.parseInt(configuration.getProperty("numberOfNobilityLevel")); i++) {
			key = "nobility" + i + bonus;
			initialState.put(key, configuration.getProperty(key));
		}
		for (int i = 0; i != Integer.parseInt(configuration.getProperty("numberOfPrize")); i++) {
			key = "prize" + i + bonus;
			initialState.put(key, configuration.getProperty(key));
		}
		for (Council c : this.map.getCouncil()) {
			initialState.putAll(c.getState());
		}
		initialState.putAll(this.map.getKing().getState());

		initialState.put(TURN_NUMBER, Integer.toString(0));
		initialState.put(MOVING_STRING, Integer.toString(0));
		initialState.put("lastMove", "OK");

		return initialState;

	}

	/**
	 *
	 * @return players
	 */
	public PlayerImpl[] getPlayers() {
		return this.players;
	}

	/**
	 *
	 * @return map
	 */
	public Map getMap() {
		return this.map;
	}

	/**
	 *
	 * @return winner
	 */
	public PlayerImpl getWinner() {
		return this.winner;
	}

	/**
	 *
	 * @return over
	 */
	public boolean getisOver() {
		return over;
	}

	/**
	 *
	 * @return moveMade the condition on which the game will be waiting to be
	 *         signaled by the player when a move is made
	 */
	public Condition getMoveMadeCondition() {
		return moveMade;
	}

	/**
	 *
	 * @return lock the game lock which makes two players cant' move at the same
	 *         time
	 */
	public Lock getLock() {
		return lock;
	}

	/**
	 *
	 * @return turnNumber
	 */
	public int getTurnNumber() {
		return turnNumber;
	}

	/**
	 * 
	 * Takes care of updating other players when someone disconnects and ends
	 * the game if there is only one player left
	 *
	 * @param playerNumber
	 *            is the number of the player that disconnected
	 */
	public void playerDisconnected(int playerNumber) {
		onlinePlayers--;
		currentStateUpdate.put(PLAYER_NAME_LOWERCASE + playerNumber + "State", "OFFLINE");
		if (onlinePlayers < 2) {
			lastTurn = true;
		}

	}

	/**
	 * Allows the timer to skip to the next turn if it goes off
	 * 
	 */
	protected void skipTurn() {
		players[moving].skipTurn();

	}

}

class TurnTimer extends TimerTask {
	private static final Logger logger = Logger.getLogger(TurnTimer.class.getName());

	private Game game;
	private Lock lock;
	private Condition condition;
	private final int turnNumber;

	/**
	 * @param game
	 *            the game on which the timer works
	 *
	 */
	public TurnTimer(Game game) {
		this.game = game;
		this.lock = game.getLock();
		this.condition = game.getMoveMadeCondition();
		this.turnNumber = game.getTurnNumber();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		if (game.getTurnNumber() == this.turnNumber) {
			logger.info("Turn timer expired!");
			lock.lock();
			game.skipTurn();
			condition.signalAll();
			lock.unlock();
		}

	}

}
