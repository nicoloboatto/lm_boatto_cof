package core;

/**
 * @author B
 *
 */
public class InvalidMoveException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2689541406012032098L;

	/**
	 * 
	 * @param cause the cause of the exception
	 */
	public InvalidMoveException(String cause){
		super("Invalid move! "+ cause);
	}
	
	/**
	 * 
	 * @param cause the cause of the exception
	 * @param thrown the exception that caused the invalid move
	 */
	public InvalidMoveException(String cause, Exception thrown){
		super(cause, thrown);
	}
	
	

}
