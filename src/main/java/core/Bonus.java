package core;

public class Bonus {
	private final int moneyBonus;
	private final int helperBonus;
	private final int victoryBonus;
	private final int politicalBonus;
	private final int nobilityBonus;
	private final int actionBonus;
	private final int emporiumNobilityBonus;
	private final int pickPermitCardBonus;
	private final int permitCardBonus;

	/**
	 * Constructor for Bonus, takes an integer array as an input
	 * 
	 * @param bonus, an array of int, first the money bonus
	 * The second is the helper bonus The third is
	 * the victory bonus The fourth is the political card bonus the fifth is the
	 * nobility bonus the sixth is the action bonus emporiumNobilityBonus
	 * this.pickPermitCardBonus this.permitCardBonus
	 */
	public Bonus(int[] bonus) {
		this.moneyBonus = bonus[0];
		this.helperBonus = bonus[1];
		this.victoryBonus = bonus[2];
		this.politicalBonus = bonus[3];
		this.nobilityBonus = bonus[4];
		this.actionBonus = bonus[5];
		this.emporiumNobilityBonus = bonus[6];
		this.pickPermitCardBonus = bonus[7];
		this.permitCardBonus = bonus[8];
	}

	/**
	 * @return the moneyBonus
	 */
	public int getMoneyBonus() {
		return moneyBonus;
	}

	/**
	 * @return the helperBonus
	 */
	public int getHelperBonus() {
		return helperBonus;
	}

	/**
	 * @return the victoryBonus
	 */
	public int getVictoryBonus() {
		return victoryBonus;
	}

	/**
	 * @return the politicalBonus
	 */
	public int getPoliticalBonus() {
		return politicalBonus;
	}

	/**
	 * @return the nobilityBonus
	 */
	public int getNobilityBonus() {
		return nobilityBonus;
	}

	/**
	 * @return the actionBonus
	 */
	public int getActionBonus() {
		return actionBonus;
	}

	/**
	 * 
	 * @return emporiumNobilityBonus
	 */
	public int getEmporiumNobilityBonus() {
		return emporiumNobilityBonus;
	}

	/**
	 * 
	 * @return pickPermitCard
	 */
	public int getPickPermitCardBonus() {
		return this.pickPermitCardBonus;
	}

	/**
	 * 
	 * @return permitCardBonus
	 */
	public int getPermitCardBonus() {
		return this.permitCardBonus;
	}

	/**
	 * This methods return a string of the bonus for the properties
	 * 
	 * @return the bonus converted to string
	 */
	@Override
	public String toString() {
		String bonusString = new String();
		if (this.moneyBonus != 0)
			bonusString += moneyBonus + "m ";
		if (this.helperBonus != 0)
			bonusString += helperBonus + "h ";
		if (this.victoryBonus != 0)
			bonusString += victoryBonus + "v ";
		if (this.politicalBonus != 0)
			bonusString += politicalBonus + "p ";
		if (this.nobilityBonus != 0)
			bonusString += nobilityBonus + "n ";
		if (this.actionBonus != 0)
			bonusString += actionBonus + "a ";
		return bonusString;
	}

}
