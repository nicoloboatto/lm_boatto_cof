package core;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.logging.Level;
import java.util.logging.Logger;

import client.RemotePlayer;
import server.UserHandler;

public class PlayerImpl extends UnicastRemoteObject implements RemotePlayer {

	
	private static final long serialVersionUID = 5062709247074787124L;
	private static final Logger logger = Logger.getLogger(PlayerImpl.class.getName());
	private transient UserHandler userHandler;
	private boolean disconnected = false;
	private String username;
	private int skippedTurns = 0;

	/**
	 * These keep track of moves available to the player
	 * 
	 * 
	 */
	private int mainAction;
	private int quickAction;
	private int getBonusOfAnEmporium;
	private int pickAPermitCard;
	private int getPermitCardBonus;

	private transient Lock gameLock;
	private transient Condition moveMade;

	private HashMap<String, String> stateUpdate = new HashMap<>();
	private final int number;
	private transient Game game;
	private int money;
	private int helpers;
	private int victory;
	private int nobility;
	private ArrayList<Character> politicalCards = new ArrayList<>();
	private ArrayList<City> emporiums = new ArrayList<>();
	private transient Market market;
	private ArrayList<PermitCard> permitCards = new ArrayList<>();
	
	private static final String MESSAGE = "message";
	public static final String PLAYER_NAME_LOWERCASE = "player";
	public static final String HELPERS_STRING = "Helpers";
	public static final String MONEY_STRING = "Money";
	public static final String EMPORIUMS_STRING = "Emporiums";
	public static final String PERMITCARD = "PermitCard";
	public static final String LAST_MOVE = "lastMove";
	public static final String INVALID_MOVE = "Player made an invalid move";

	/**
	 * Constructs a Player and assigns it a specified number, which is used to
	 * calculate its initial values
	 * 
	 * @param number
	 *            of the player
	 * @throws RemoteException
	 *             RemoteException in case there are errors in the remote
	 *             implementation
	 */
	public PlayerImpl(int number) throws RemoteException {
		this.money = 10 + number;
		if (this.money > 20)
			this.money = 20;
		this.helpers = 1 + number;
		this.number = number;
		this.politicalCards = new ArrayList<>();
		for (int i = 0; i != 6; i++)
			this.politicalCards.add(pickCard());
		this.permitCards = new ArrayList<>();
		this.nobility = 0;
		this.victory = 0;
		this.getBonusOfAnEmporium = 0;
		this.pickAPermitCard = 0;
		this.getPermitCardBonus = 0;
		this.emporiums = new ArrayList<>();
		if (number == 0) {
			mainAction = 1;
			quickAction = 1;
		} else {
			mainAction = 0;
			quickAction = 0;
		}
		stateUpdate.put(PLAYER_NAME_LOWERCASE + number + MONEY_STRING, Integer.toString(money));
		stateUpdate.put(PLAYER_NAME_LOWERCASE + number + HELPERS_STRING, Integer.toString(helpers));
		stateUpdate.put(PLAYER_NAME_LOWERCASE + number + "Victory", Integer.toString(victory));
		stateUpdate.put(PLAYER_NAME_LOWERCASE + number + "Nobility", Integer.toString(nobility));
		String emporiumInTheCity = new String();
		stateUpdate.put(PLAYER_NAME_LOWERCASE + number + EMPORIUMS_STRING, emporiumInTheCity);

	}

	/**
	 * 
	 * @return userHandler
	 */
	public UserHandler getHandler() {
		return userHandler;
	}

	/**
	 * This method generates a HashMap of the state of the player
	 * 
	 * @return an HashMap
	 */
	public HashMap<String, String> getState() {
		HashMap<String, String> temporaryState = new HashMap<>();
		temporaryState.putAll(stateUpdate);
		stateUpdate.clear();
		return temporaryState;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see core.Player#buyPermitCard(java.lang.String, int, char[])
	 */
	@Override
	public void buyPermitCard(String nameOfTheRegion, int position, char[] politicalCard) {

		gameLock.lock();
		try {

			mainActionDone();
			removeMoney(buyWithPoliticalCard(politicalCard));
			removePoliticalCard(politicalCard);
			Council council = game.getMap().getCouncilByName(nameOfTheRegion);
			PermitCard permitCard = council.removePermitCard(position);
			addPermitCard(permitCard);
			activateBonus(permitCard.getBonus());
			mainAction--;

			stateUpdate.putAll(council.getState());
			stateUpdate.putAll(getPermitCardState());
			stateUpdate.put(LAST_MOVE, "OK");

		} catch (InvalidMoveException exc) {
			logger.log(Level.WARNING, INVALID_MOVE, exc);
			stateUpdate.put(MESSAGE, exc.getMessage());
			stateUpdate.put(LAST_MOVE, "NOPE");
		}
		moveMade.signalAll();
		gameLock.unlock();

	}

	/**
	 * Activate a bonus
	 * 
	 * @param bonus
	 *            that you want to activate
	 */
	public void activateBonus(Bonus bonus) {
		addMoney(bonus);
		stateUpdate.put(PLAYER_NAME_LOWERCASE + number + MONEY_STRING, Integer.toString(money));

		addHelpers(bonus);
		stateUpdate.put(PLAYER_NAME_LOWERCASE + number + HELPERS_STRING, Integer.toString(helpers));

		addVictory(bonus);
		stateUpdate.put(PLAYER_NAME_LOWERCASE + number + "Victory", Integer.toString(victory));

		addNobility(bonus);
		stateUpdate.put(PLAYER_NAME_LOWERCASE + number + "Nobility", Integer.toString(nobility));

		for (int i = 0; i != bonus.getPoliticalBonus(); i++)
			this.politicalCards.add(pickCard());
		addMainAction(bonus);
		addGetBonusOfAnEmporium(bonus);
		addPickAPermitCard(bonus);
		addGetPermitCardBonus(bonus);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see core.Player#buyKing(char[], char)
	 */
	@Override
	public void buyKing(char[] politicalCard, char city) {

		gameLock.lock();

		try {
			mainActionDone();
			removeMoney(buyWithPoliticalCard(politicalCard));
			removeMoney(howFarIsTheCity(game.getMap().searchCityByChar(city)) * 2);
			removeHelpers(this.game.checkHowManyHelpersForEmporium(game.getMap().searchCityByChar(city)));
			removePoliticalCard(politicalCard);
			activateBonusOfTheCitiesNear(game.getMap().searchCityByChar(city));
			putEmporiumInCollection(game.getMap().searchCityByChar(city));
			game.getMap().getKing().move(game.getMap().searchCityByChar(city));
			checkLastEmporium();
			if (game.getMap().searchCityByChar(city).getColor().checkColor(this.emporiums)) {
				activateBonus(game.getMap().searchCityByChar(city).getColor().getBonus());
				if (!game.getMap().getPrize().getBonus().isEmpty()
						&& !"capital".equals(game.getMap().searchCityByChar(city).getColor().getName()))
					activateBonus(game.getMap().getPrize().getBonus().get(0));
			}
			if (game.getMap().searchCityByChar(city).getRegion().checkCities(this.emporiums)) {
				activateBonus(game.getMap().searchCityByChar(city).getRegion().getBonus());
				if (!game.getMap().getPrize().getBonus().isEmpty())
					activateBonus(game.getMap().getPrize().getBonus().get(0));
			}
			mainAction--;

			stateUpdate.putAll(game.getMap().getKing().getState());

			stateUpdate.put(PLAYER_NAME_LOWERCASE + number + EMPORIUMS_STRING, getCitiesWithAnEmporium());
			stateUpdate.put(LAST_MOVE, "OK");

		} catch (InvalidMoveException exc) {
			logger.log(Level.WARNING, INVALID_MOVE, exc);
			stateUpdate.put(MESSAGE, exc.getMessage());
			stateUpdate.put(LAST_MOVE, "NOPE");
		}

		moveMade.signalAll();
		gameLock.unlock();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see core.Player#slideCouncil(char, java.lang.String)
	 */
	@Override
	public void slideCouncil(char color, String nameOfTheRegionOfTheCouncil) {
		gameLock.lock();

		try {
			mainActionDone();
			addMoney(4);
			Council council = game.getMap().getCouncilByName(nameOfTheRegionOfTheCouncil);
			council.slideOn(color);
			mainAction--;

			stateUpdate.putAll(council.getState());
			stateUpdate.put(PLAYER_NAME_LOWERCASE + number + MONEY_STRING, Integer.toString(money));
			stateUpdate.put(LAST_MOVE, "OK");
		} catch (InvalidMoveException exc) {
			logger.log(Level.WARNING, INVALID_MOVE, exc);
			stateUpdate.put(MESSAGE, exc.getMessage());
			stateUpdate.put(LAST_MOVE, "NOPE");
		}
		moveMade.signalAll();
		gameLock.unlock();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see core.Player#putEmporiumInACity(int, char)
	 */
	@Override
	public void putEmporiumInACity(int numberOfThePermitCard, char charOfTheCity) {

		gameLock.lock();

		try {
			mainActionDone();
			removeHelpers(this.game.checkHowManyHelpersForEmporium(game.getMap().searchCityByChar(charOfTheCity)));
			activateBonusOfTheCitiesNear(game.getMap().searchCityByChar(charOfTheCity));
			putEmporiumInCollection(game.getMap().searchCityByChar(charOfTheCity));
			setUsedPermitCard(this.permitCards.get(numberOfThePermitCard));
			checkLastEmporium();
			if (game.getMap().searchCityByChar(charOfTheCity).getColor().checkColor(this.emporiums)) {
				activateBonus(game.getMap().searchCityByChar(charOfTheCity).getColor().getBonus());
				if (!game.getMap().getPrize().getBonus().isEmpty()
						&& !"capital".equals(game.getMap().searchCityByChar(charOfTheCity).getColor().getName()))
					activateBonus(game.getMap().getPrize().getBonus().get(0));
			}
			if (game.getMap().searchCityByChar(charOfTheCity).getRegion().checkCities(this.emporiums)) {
				activateBonus(game.getMap().searchCityByChar(charOfTheCity).getRegion().getBonus());
				if (!game.getMap().getPrize().getBonus().isEmpty())
					activateBonus(game.getMap().getPrize().getBonus().get(0));
			}
			mainAction--;

			stateUpdate.putAll(getPermitCardState());

			stateUpdate.put(PLAYER_NAME_LOWERCASE + number + EMPORIUMS_STRING, getCitiesWithAnEmporium());
			stateUpdate.put(LAST_MOVE, "OK");

		} catch (InvalidMoveException exc) {
			logger.log(Level.WARNING, INVALID_MOVE, exc);
			stateUpdate.put(MESSAGE, exc.getMessage());
			stateUpdate.put(LAST_MOVE, "NOPE");
		}
		moveMade.signalAll();
		gameLock.unlock();
	}

	/**
	 * This method generate a String with the cities with an emporium
	 * 
	 * @return citiesWithAnEmporium
	 */
	private String getCitiesWithAnEmporium() {
		String citiesWithAnEmporium = new String();
		for (City city : emporiums) {
			citiesWithAnEmporium += Character.toString(city.getCharOfTheCity());
		}
		return citiesWithAnEmporium;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see core.Player#buyHelper()
	 */
	@Override
	public void buyHelper() {
		gameLock.lock();

		try {
			quickActionDone();
			removeMoney(3);
			this.helpers++;
			quickAction--;

			stateUpdate.put(PLAYER_NAME_LOWERCASE + number + MONEY_STRING, Integer.toString(money));
			stateUpdate.put(PLAYER_NAME_LOWERCASE + number + HELPERS_STRING, Integer.toString(helpers));
			stateUpdate.put(LAST_MOVE, "OK");
		} catch (InvalidMoveException exc) {
			logger.log(Level.WARNING, INVALID_MOVE, exc);
			stateUpdate.put(MESSAGE, exc.getMessage());
			stateUpdate.put(LAST_MOVE, "NOPE");
		}
		moveMade.signalAll();
		gameLock.unlock();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see core.Player#refreshPermitsCard(java.lang.String)
	 */
	@Override
	public void refreshPermitCards(String nameOfTheRegionOfTheCouncil) {
		gameLock.lock();

		try {
			quickActionDone();
			removeHelpers(1);
			Council council = game.getMap().getCouncilByName(nameOfTheRegionOfTheCouncil);
			council.refreshPermits();
			quickAction--;

			stateUpdate.putAll(council.getState());
			stateUpdate.put(LAST_MOVE, "OK");
		} catch (InvalidMoveException exc) {
			logger.log(Level.WARNING, INVALID_MOVE, exc);
			stateUpdate.put(MESSAGE, exc.getMessage());
			stateUpdate.put(LAST_MOVE, "NOPE");
		}
		moveMade.signalAll();
		gameLock.unlock();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see core.Player#addAMemberOfTheCouncil(java.lang.String, char)
	 */
	@Override
	public void addAMemberOfTheCouncil(String nameOfTheRegionOfTheCouncil, char color) {
		gameLock.lock();

		try {
			quickActionDone();
			removeHelpers(1);
			Council council = game.getMap().getCouncilByName(nameOfTheRegionOfTheCouncil);
			council.slideOn(color);
			quickAction--;

			stateUpdate.putAll(council.getState());
			stateUpdate.put(PLAYER_NAME_LOWERCASE + number + HELPERS_STRING, Integer.toString(helpers));
			stateUpdate.put(LAST_MOVE, "OK");

		} catch (InvalidMoveException exc) {
			logger.log(Level.WARNING, INVALID_MOVE, exc);
			stateUpdate.put(MESSAGE, exc.getMessage());
			stateUpdate.put(LAST_MOVE, "NOPE");
		}

		moveMade.signalAll();
		gameLock.unlock();

	}

	/**
	 * Quick action number 4
	 * 
	 */
	@Override
	public void buyMainAction() {

		gameLock.lock();

		try {
			quickActionDone();
			removeHelpers(3);
			quickAction--;
			mainAction++;

			stateUpdate.put(PLAYER_NAME_LOWERCASE + number + HELPERS_STRING, Integer.toString(helpers));
			stateUpdate.put(LAST_MOVE, "OK");
		} catch (InvalidMoveException exc) {
			logger.log(Level.WARNING, INVALID_MOVE, exc);
			stateUpdate.put(MESSAGE, exc.getMessage());
			stateUpdate.put(LAST_MOVE, "NOPE");
		}

		moveMade.signalAll();
		gameLock.unlock();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.RemotePlayer#skipQuickAction()
	 */
	@Override
	public void skipAction() throws RemoteException {

		gameLock.lock();

		quickAction = 0;

		stateUpdate.put(LAST_MOVE, "OK");

		moveMade.signalAll();
		gameLock.unlock();

	}

	/**
	 * @param offer
	 *            that is sold
	 */
	public void offerSold(Offer offer) {
		permitCards.remove(offer.getPermitCard());
		if (offer.getPoliticalCards() != null)
			for (char card : offer.getPoliticalCards()) {
				int index = politicalCards.indexOf(card);
				politicalCards.remove(index);
			}

		helpers -= offer.getHelpers();
		activateBonus(offer.getPaymentBonus());

		stateUpdate.putAll(getPermitCardState());
		stateUpdate.put(PLAYER_NAME_LOWERCASE + number + HELPERS_STRING, Integer.toString(helpers));
		stateUpdate.put(PLAYER_NAME_LOWERCASE + number + MONEY_STRING, Integer.toString(money));
		stateUpdate.putAll(game.getMarketState());
		stateUpdate.put(LAST_MOVE, "OK");
	}

	/**
	 * This method put an offer in the market
	 * 
	 * @param politicalCard
	 *            that you want to add to the offer
	 * @param permitCard
	 *            that you want to add to the offer
	 * @param price
	 *            that you want to add to the offer
	 */
	@Override
	public void putOffer(char[] politicalCard, int permitCard, int assistants, int price) {

		gameLock.lock();
		try {

			ArrayList<Character> temporaryCards = (ArrayList<Character>) politicalCards.clone();

			if (politicalCard != null)
				for (Character character : politicalCard) {
					if (temporaryCards.contains(character)) {
						int index = temporaryCards.indexOf(character);
						temporaryCards.remove(index);
					} else {
						throw new InvalidMoveException("The player does not have the speficied cards!");
					}
				}

			if (assistants > this.helpers) {
				throw new InvalidMoveException("The player does not have enough helpers");
			}
			Offer offer;
			if (permitCard == -1) {
				offer = new Offer(this, politicalCard, null, assistants, price);
			} else {
				offer = new Offer(this, politicalCard, this.permitCards.get(permitCard), assistants, price);
			}

			game.addOffer(offer);
			stateUpdate.putAll(game.getMarketState());
			stateUpdate.put(LAST_MOVE, "OK");
		} catch (ArrayIndexOutOfBoundsException | InvalidMoveException e) {
			logger.log(Level.WARNING, INVALID_MOVE, e);
			stateUpdate.put(MESSAGE, e.getMessage());
			stateUpdate.put(LAST_MOVE, "NOPE");
		}
		moveMade.signalAll();
		gameLock.unlock();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see core.Player#buyOffer(core.Offer)
	 */
	@Override
	public void buyOffer(int offerNumber) {

		gameLock.lock();

		try {
			removeMoney(game.getOfferPrice(offerNumber));
			Offer offer = game.buyOffer(offerNumber);
			offer.getSeller().offerSold(offer);
			if (offer.getPermitCard() != null)
				addPermitCard(offer.getPermitCard());
			if (offer.getPoliticalCards() != null) {
				addPoliticalcards(offer.getPoliticalCards());
			}
			helpers += offer.getHelpers();

			stateUpdate.putAll(getPermitCardState());
			stateUpdate.put(PLAYER_NAME_LOWERCASE + number + HELPERS_STRING, Integer.toString(helpers));
			stateUpdate.put(PLAYER_NAME_LOWERCASE + number + MONEY_STRING, Integer.toString(money));
			stateUpdate.putAll(game.getMarketState());
			stateUpdate.put(LAST_MOVE, "OK");

		} catch (InvalidMoveException exc) {
			logger.log(Level.WARNING, INVALID_MOVE, exc);
			stateUpdate.put(MESSAGE, exc.getMessage());
			stateUpdate.put(LAST_MOVE, "NOPE");
		}
		moveMade.signalAll();
		gameLock.unlock();
	}

	private void addPoliticalcards(char[] cards) {
		for (char card : cards) {
			this.politicalCards.add(card);
		}
	}

	/**
	 * This method get the bonus of a city where you have an emporium
	 * 
	 * @param city
	 *            is the char of the city
	 */
	@Override
	public void getBonusOfAnEmporium(char city) {

		if (game.getMap().searchCityByChar(city).getBonus().getNobilityBonus() == 0
				&& this.emporiums.contains(game.getMap().searchCityByChar(city))) {

			activateBonus(game.getMap().searchCityByChar(city).getBonus());

			getBonusOfAnEmporium--;
			stateUpdate.put(LAST_MOVE, "OK");
		} else {
			stateUpdate.put(LAST_MOVE, "NOPE");
		}

	}

	/**
	 * This method get the bonus of a permitCard that you have
	 * 
	 * @param position
	 *            the number of the permitCard from the player list
	 */
	@Override
	public void getPermitCardBonus(int position) {

		if (this.permitCards.get(position) != null) {
			activateBonus(this.permitCards.get(position).getBonus());

			getPermitCardBonus--;
			stateUpdate.put(LAST_MOVE, "OK");
		}

		else {
			stateUpdate.put(LAST_MOVE, "NOPE");
		}
	}

	/**
	 * 
	 * @param region
	 *            the name of the region that contains the card
	 * @param permitCardNumber
	 *            the number of the permitCard to acquire
	 * 
	 */
	@Override
	public void pickPermitCard(String region, int permitCardNumber) {

		if (game.getMap().getCouncilByName(region) != null
				&& permitCardNumber < game.getMap().getCouncilByName(region).getPermitCards().size()) {

			Council council = game.getMap().getCouncilByName(region);
			PermitCard permitCard = council.removePermitCard(permitCardNumber);
			addPermitCard(permitCard);
			activateBonus(permitCard.getBonus());
			pickAPermitCard--;

			stateUpdate.putAll(getPermitCardState());
			stateUpdate.put(LAST_MOVE, "OK");
		}

		else {
			stateUpdate.put(LAST_MOVE, "NOPE");
		}
	}

	/**
	 * 
	 * @param permitCard
	 *            added to a player
	 */
	private void addPermitCard(PermitCard permitCard) {
		this.permitCards.add(permitCard);
	}

	/**
	 * This method created a queue and an explored lists, it use the
	 * "Breadth First Search" or "BFS" style, and activate the bonus of the city
	 * and the bonus of the near city if you have an emporium on it
	 * 
	 * @param city
	 *            is the City where the player has builded an emporium
	 */
	public void activateBonusOfTheCitiesNear(City city) {
		Queue<City> queue = new LinkedList<>();
		ArrayList<City> explored = new ArrayList<>();
		queue.add(city);
		while (!queue.isEmpty()) {
			City current = queue.remove();
			if (!explored.contains(current)) {
				explored.add(current);
				activateBonus(current.getBonus());
				for (int i = 0; i != current.getConnectedCities().length; i++)
					for (int j = 0; j != this.emporiums.size(); j++)
						if (!explored.contains(current.getConnectedCities()[i])
								&& current.getConnectedCities()[i] == this.emporiums.get(j))
							queue.add(current.getConnectedCities()[i]);
			}
		}
	}

	/**
	 * This method check if a permitCard is used, if isnt used set that card
	 * used
	 * 
	 * @param permitCard
	 *            set used from a player
	 * @throws InvalidMoveException
	 *             is called if the permitCard is already used
	 */
	private void setUsedPermitCard(PermitCard permitCard) throws InvalidMoveException {
		if (!permitCard.isUsed())
			permitCard.setUsed();
		else
			throw new InvalidMoveException("PermitCard already used!");
	}

	/**
	 * This method generate an HashMap of the hand of the Player
	 * 
	 * @return politicalCards that is a String
	 */
	public String getHand() {

		String buff = new String();

		for (char card : this.politicalCards) {
			buff += Character.toString(card);
		}

		return buff;
	}

	/**
	 * This method generate an HashMap of the permitCardOfThePlayer
	 * 
	 * @return permitCardState
	 */
	public HashMap<String, String> getPermitCardState() {
		HashMap<String, String> permitCardState = new HashMap<>();

		permitCardState.put(PLAYER_NAME_LOWERCASE + Integer.toString(number) + "PermitCards",
				Integer.toString(permitCards.size()));
		for (int i = 0; i != this.permitCards.size(); i++) {

			permitCardState.put(PLAYER_NAME_LOWERCASE + Integer.toString(number) + PERMITCARD + i + "Cities",
					this.permitCards.get(i).getCities());
			permitCardState.put(PLAYER_NAME_LOWERCASE + Integer.toString(number) + PERMITCARD + i + "Bonus",
					this.permitCards.get(i).getBonus().toString());
			permitCardState.put(PLAYER_NAME_LOWERCASE + Integer.toString(number) + PERMITCARD + i + "Used",
					Boolean.toString(this.permitCards.get(i).isUsed()));

		}

		return permitCardState;
	}

	/**
	 * This method reset mainAction and quickAction and pick a card for the
	 * player
	 */
	public void startTurn() {
		politicalCards.add(pickCard());
		this.mainAction = 1;
		this.quickAction = 1;
	}

	/**
	 * This method skip the turn of the player
	 */
	public void skipTurn() {
		skippedTurns++;
		if (skippedTurns > 2) {
			disconnected();
		}
		mainAction = 0;
		quickAction = 0;
		getBonusOfAnEmporium = 0;
		pickAPermitCard = 0;
		getPermitCardBonus = 0;

	}

	/**
	 * This method calculate the possible moves of the player
	 * 
	 * @return a string of 12 of size (like the number of moves you can do) 0
	 *         you cannot do that move, 1 you can do that move
	 */
	public String calculatePossibleMoves() {
		String move = new String();
		if (hasMainAction()) {
			move += checkFirstMainAction();
			move += checkSecondMainAction();
			move += "1";
			move += checkFourthMainAction();
		} else
			move += "0000";
		if (hasQuickAction()) {
			move += checkFirstQuickAction();
			move += checkSecondQuickAction();
			move += checkThirdQuickAction();
			move += checkFourthQuickAction();
			move += "1";
		} else
			move += "00000";
		move += checkGetBonusOfAnEmporium();
		move += checkPickAPermitCard();
		move += checkGetPermitCardBonus();
		return move;
	}

	/**
	 * This method use the dijkstra algorithm for the minimum path.
	 * 
	 * @param city
	 *            that you want to know how far is from the location of the king
	 * @return how far is the city from the location of the king
	 */
	private int howFarIsTheCity(City city) {
		int[][] graph;
		int src = city.getCharOfTheCity() - 65;
		graph = game.getMap().generateGraphOfCities();
		int[] dist = new int[game.getMap().getCity().length];
		Boolean[] sptSet = new Boolean[game.getMap().getCity().length];
		for (int i = 0; i != game.getMap().getCity().length; i++) {
			dist[i] = Integer.MAX_VALUE;
			sptSet[i] = false;
		}
		dist[src] = 0;
		for (int count = 0; count != game.getMap().getCity().length - 1; count++) {
			int u = minDistance(dist, sptSet);
			sptSet[u] = true;
			for (int v = 0; v != game.getMap().getCity().length; v++)
				if (!sptSet[v] && graph[u][v] != 0 && dist[u] != Integer.MAX_VALUE && dist[u] + graph[u][v] < dist[v])
					dist[v] = dist[u] + graph[u][v];
		}
		src = game.getMap().getKing().getLocation().getCharOfTheCity() - 65;
		return dist[src];
	}

	/**
	 * 
	 * @return true if the Player has a mainAction or a quickAction, false
	 *         otherwise
	 */
	public Boolean hasAction() {
		return hasMainAction() || hasQuickAction() || getBonusOfAnEmporium > 0 || pickAPermitCard > 0
				|| getPermitCardBonus > 0;
	}

	/**
	 * 
	 * @return the list of politicalCards
	 */

	public List<Character> getPoliticalCards() {
		return this.politicalCards;
	}

	/**
	 * 
	 * @return the list of permitCards
	 */

	public List<PermitCard> getPermitCard() {
		return this.permitCards;
	}

	/**
	 * 
	 * @return helpers
	 */
	public int getHelpers() {
		return this.helpers;
	}

	/**
	 * 
	 * @return market
	 */
	public Market getMarket() {
		return this.market;
	}

	/**
	 * 
	 * @return game
	 */
	public Game getGame() {
		return this.game;
	}

	/**
	 * 
	 * @return money
	 */
	public int getMoney() {
		return this.money;
	}

	/**
	 * 
	 * @return victory
	 */
	public int getVictory() {
		return this.victory;
	}

	/**
	 * 
	 * @return emporiums
	 */
	public List<City> getEmporiums() {
		return this.emporiums;
	}

	/**
	 * 
	 * @return nobility
	 */
	public int getNobility() {
		return this.nobility;
	}

	/**
	 * 
	 * @return the number of permitCard
	 */
	public int getSizePermitCard() {
		return permitCards.size();
	}

	/**
	 * 
	 * @param game
	 *            to set
	 */
	public void setGame(Game game) {
		this.game = game;
		this.gameLock = game.getLock();
		this.moveMade = game.getMoveMadeCondition();
	}

	/**
	 * Set an emporium in a city
	 * 
	 * @param city
	 *            where you want to set the emporium
	 */
	public void setEmporium(City city) {
		this.emporiums.add(city);
	}

	/**
	 * @param userHandler
	 *            to set
	 */
	public void setHandler(UserHandler userHandler) {
		if (userHandler != null) {
			this.userHandler = userHandler;
			this.username = userHandler.getUsername();

			try {
				this.userHandler.setPlayer(this);
			} catch (IOException e) {
				logger.log(Level.WARNING, "Player disconnected", e);
				disconnected();
			}
		} else {
			disconnected();
		}

	}

	/**
	 * @return the number
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * Updates the player that the specified things have changed in the game
	 * state
	 * 
	 * @param stateUpdate
	 *            the stateUpdate generated by the latest move
	 */
	public void update(HashMap<String, String> stateUpdate) {
		try {
			userHandler.update(stateUpdate);
		} catch (IOException e) {
			logger.log(Level.WARNING, username + " disconnected", e);
			disconnected();

		}
	}

	/**
	 * 
	 * @return username
	 */
	public String getUsername() {
		return this.username;
	}

	/**
	 * @return the disconnected
	 */
	public boolean isDisconnected() {
		return disconnected;
	}

	/**
	 * Sets disconnected as true, removes the userHandler and tells the game the
	 * player has disconnected
	 */
	public void disconnected() {
		disconnected = true;
		if (userHandler != null)
			userHandler.cleanUp();
		game.playerDisconnected(number);
	}

	/**
	 * @param dist
	 *            is an array needed for the algorithm
	 * @param sptSet
	 *            is an array of boolean to know if you have already searched in
	 *            that position
	 * @return the minumum distance
	 */
	private int minDistance(int[] dist, Boolean[] sptSet) {
		int min = Integer.MAX_VALUE;
		int minIndex = -1;
		for (int v = 0; v < game.getMap().getCity().length; v++)
			if (!sptSet[v] && dist[v] <= min) {
				min = dist[v];
				minIndex = v;
			}
		return minIndex;
	}

	/**
	 * If the emporium that is built is the last one gives that player 3 victory
	 * points.
	 */
	private void checkLastEmporium() {
		if (this.emporiums.size() == game.maximumEmporium) {
			addVictory(3);
		}

	}

	/**
	 * Put an emporium in the first free space and check that the player dont
	 * have already an emporium in that city
	 * 
	 * @param city
	 *            to put an emporium
	 * @throws InvalidMoveException
	 *             is called if the player has already an emporium in that city
	 */
	private void putEmporiumInCollection(City city) throws InvalidMoveException {
		if (!this.emporiums.contains(city))
			setEmporium(city);
		else
			throw new InvalidMoveException("Player already has an emporium in that city");
	}

	/**
	 * 
	 * @param bonus
	 *            that you want to add
	 */
	private void addGetBonusOfAnEmporium(Bonus bonus) {
		this.getBonusOfAnEmporium += bonus.getEmporiumNobilityBonus();
	}

	/**
	 * 
	 * @param bonus
	 *            that you want to add
	 */
	private void addPickAPermitCard(Bonus bonus) {
		this.pickAPermitCard += bonus.getPickPermitCardBonus();
	}

	/**
	 * 
	 * @param bonus
	 *            that you want to add
	 */
	private void addGetPermitCardBonus(Bonus bonus) {
		this.getPermitCardBonus += bonus.getPermitCardBonus();
	}

	/**
	 * 
	 * @return a card
	 */
	private char pickCard() {
		int x = (int) (Math.random() * 6);
		return whichCard(x);
	}

	/**
	 * 
	 * @param bonus
	 *            that you want to add
	 */
	private void addMainAction(Bonus bonus) {
		this.mainAction += bonus.getActionBonus();
	}

	/**
	 * 
	 * @param bonus
	 *            that you want to add
	 */
	private void addMoney(Bonus bonus) {
		this.money += bonus.getMoneyBonus();
		if (this.money > 20)
			this.money = 20;
	}

	/**
	 * 
	 * @param money
	 *            that you want to add
	 */
	private void addMoney(int money) {
		this.money += money;
		if (this.money > 20)
			this.money = 20;
	}

	/**
	 * 
	 * @param bonus
	 *            that you want to add
	 */
	private void addNobility(Bonus bonus) {
		this.nobility += bonus.getNobilityBonus();
		if (this.nobility > 20)
			this.nobility = 20;
	}

	/**
	 * 
	 * @param bonus
	 *            that you want to add
	 */
	private void addVictory(Bonus bonus) {
		this.victory += bonus.getVictoryBonus();
		if (this.victory >= 100) {
			this.victory = 99;
		}
	}

	/**
	 * 
	 * @param victory
	 *            that you want to add
	 */
	private void addVictory(int victory) {
		this.victory += victory;
		if (this.victory >= 100)
			this.victory = 99;
	}

	/**
	 * 
	 * @param bonus
	 *            that you want to add
	 */
	private void addHelpers(Bonus bonus) {
		this.helpers += bonus.getHelperBonus();
	}

	/**
	 * 
	 * @param helpers
	 *            that you want to remove
	 * @throws InvalidMoveException
	 *             is called if the player does not have enough helpers
	 */
	private void removeHelpers(int helpers) throws InvalidMoveException {
		if (helpers <= this.helpers)
			this.helpers -= helpers;
		else
			throw new InvalidMoveException("Not enough helpers");
	}

	/**
	 * 
	 * @param money
	 *            that you want to remove
	 * @throws InvalidMoveException
	 *             is called if the player does not have enough money
	 */
	private void removeMoney(int money) throws InvalidMoveException {
		if (money <= this.money)
			this.money -= money;
		else
			throw new InvalidMoveException("Not enough money");
	}

	/**
	 * 
	 * @param politicalCard
	 *            is the array of the cards that you want to give to buy a
	 *            council, this method delete the cards that you gave from the
	 *            hand of the player
	 * @throws InvalidMoveException
	 *             in case the player does not own the specified cards
	 */
	private void removePoliticalCard(char[] politicalCard) throws InvalidMoveException {
		boolean found;
		ArrayList<Character> checkList = (ArrayList<Character>) this.politicalCards.clone();
		for (int j = 0; j != politicalCard.length; j++) {
			found = false;
			for (int i = 0; i != checkList.size(); i++) {
				if (checkList.get(i) == politicalCard[j]) {
					checkList.remove(i);
					found = true;
					break;
				}
			}
			if (!found) {
				throw new InvalidMoveException("Player does not have the necessary cards");
			}

		}

		this.politicalCards = checkList;

	}

	/**
	 * This method decrease mainAction
	 * 
	 * @throws InvalidMoveException
	 *             is called if the player does not have enough main actions
	 */
	private void mainActionDone() throws InvalidMoveException {
		if (this.mainAction <= 0)
			throw new InvalidMoveException("Not enough main actions");

	}

	/**
	 * This method decrease quickAction
	 * 
	 * @throws InvalidMoveException
	 *             is called if the player does not have enough quick actions
	 */
	private void quickActionDone() throws InvalidMoveException {
		if (this.quickAction <= 0)
			throw new InvalidMoveException("Not enough quick action");

	}

	/**
	 * 
	 * @param politicalCard
	 *            array of the political card
	 * @return how many coin you need to spend
	 */
	private int buyWithPoliticalCard(char[] politicalCard) {
		switch (politicalCard.length) {
		case 1:
			return 10 + howManyMulticolorPoliticalCard(politicalCard);
		case 2:
			return 7 + howManyMulticolorPoliticalCard(politicalCard);
		case 3:
			return 4 + howManyMulticolorPoliticalCard(politicalCard);
		default:
			return howManyMulticolorPoliticalCard(politicalCard);
		}
	}

	/**
	 * 
	 * @param x
	 *            the number between 0 and 6
	 * @return one of the card
	 */
	private char whichCard(int x) {
		char card;
		switch (x) {

		case 0:
			card = 'w';
			break;
		case 1:
			card = 'o';
			break;
		case 2:
			card = 'p';
			break;
		case 3:
			card = 'b';
			break;
		case 4:
			card = 'k';
			break;
		default:
			card = 'x';
		}
		return card;
	}

	/**
	 * 
	 * @param politicalCard
	 *            array of the politicalCard
	 * @return the number of multicolor political cards
	 */
	private int howManyMulticolorPoliticalCard(char[] politicalCard) {
		int i = 0;
		for (char card : politicalCard) {
			if (card == 'x')
				i++;
		}
		return i;
	}

	/**
	 * 
	 * @return true if Player has mainAction, false otherwise
	 */
	private Boolean hasMainAction() {
		return this.mainAction > 0;
	}

	/**
	 * 
	 * 
	 * @return true if Player has quickAction, false otherwise
	 */
	private Boolean hasQuickAction() {
		return this.quickAction > 0;
	}

	/**
	 * This method check if the player can do the first main action. Check if
	 * the player can buy a council with his politicalCard and his money
	 * 
	 * @return "1" if he can buy at least one council, "0" otherwise.
	 */
	private String checkFirstMainAction() {
		char[] cards = new char[this.politicalCards.size()];
		for (int i = 0; i != this.politicalCards.size(); i++)
			cards[i] = this.politicalCards.get(i);
		ArrayList<Character> buff = new ArrayList<>();
		for (int i = 0; i != game.getMap().getCouncil().length; i++) {
			for (int j = 0; j != game.getMap().getCouncil()[i].getColors().length; j++)
				for (int k = 0; k != cards.length; k++)
					if (cards[k] == game.getMap().getCouncil()[i].getColors()[j]) {
						buff.add(cards[k]);
						cards[k] = '.';
					}
			if (buff.size() < 4 && haveMulticolorPC())
				buff.addAll(howManyMulticolorPC());
			char[] check = new char[buff.size()];
			for (int n = 0; n != buff.size(); n++)
				check[n] = buff.get(n);
			if (buyWithPoliticalCard(check) > this.money) {
				buff.clear();
				for (int v = 0; v != this.politicalCards.size(); v++)
					cards[v] = this.politicalCards.get(v);
			} else
				return "1";
		}
		return "0";
	}

	/**
	 * 
	 * @return true if the player has at least one multicolor political card,
	 *         false otherwise
	 */
	private Boolean haveMulticolorPC() {
		for (int i = 0; i != this.politicalCards.size(); i++)
			if (this.politicalCards.get(i) == 'x')
				return true;
		return false;
	}

	/**
	 * 
	 * @return a list of the multicolor political card that has the player
	 */
	private List<Character> howManyMulticolorPC() {
		ArrayList<Character> buff = new ArrayList<>();
		for (int i = 0; i != this.politicalCards.size(); i++)
			if (this.politicalCards.get(i) == 'x')
				buff.add(this.politicalCards.get(i));
		return buff;
	}

	/**
	 * This method check if the player can do the second main action. Check if
	 * the player can buy the king council with his money and politicalCard.
	 * 
	 * @return "1" if the player can buy the king council, "0" otherwise
	 */
	private String checkSecondMainAction() {
		char[] cards = new char[this.politicalCards.size()];
		for (int i = 0; i != this.politicalCards.size(); i++)
			cards[i] = this.politicalCards.get(i);
		ArrayList<Character> buff = new ArrayList<>();
		for (int j = 0; j != game.getMap().getKing().getCouncil().getColors().length; j++)
			for (int k = 0; k != cards.length; k++)
				if (cards[k] == game.getMap().getKing().getCouncil().getColors()[j]) {
					buff.add(cards[k]);
					cards[k] = '.';
				}
		if (buff.size() < 4 && haveMulticolorPC())
			buff.addAll(howManyMulticolorPC());
		char[] check = new char[buff.size()];
		for (int n = 0; n != buff.size(); n++)
			check[n] = buff.get(n);
		if (buyWithPoliticalCard(check) > this.money)
			return "0";
		return "1";
	}

	/**
	 * This method check if the player can do the fourth main action. Check if
	 * the player has at least one permitCard, check if is used or not and if
	 * the player has an emporium in all the cities of the permitCard.
	 * 
	 * @return "1" if the player can do the fourth main action, "0" otherwise
	 */
	private String checkFourthMainAction() {
		if (this.permitCards.isEmpty())
			return "0";
		else
			for (int i = 0; i != this.permitCards.size(); i++)
				if (!this.permitCards.get(i).isUsed())
					for (int j = 0; j != this.permitCards.get(i).getCities().length(); j++)
						if (!this.emporiums.contains(
								this.game.getMap().searchCityByChar(this.permitCards.get(i).getCities().charAt(j)))
								&& this.game.checkHowManyHelpersForEmporium(this.game.getMap().searchCityByChar(
										this.permitCards.get(i).getCities().charAt(j))) <= this.helpers)
							return "1";
		return "0";
	}

	/**
	 * Checks if the player can do the first quick action
	 * 
	 * @return "1" if it is possible, "0" otherwise
	 */
	private String checkFirstQuickAction() {
		if (this.money > 2)
			return "1";
		else
			return "0";
	}

	/**
	 * Checks if the player can do the second quick action
	 * 
	 * @return "1" if it is possible, "0" otherwise
	 */
	private String checkSecondQuickAction() {
		if (this.helpers != 0)
			return "1";
		else
			return "0";
	}

	/**
	 * Checks if the player can do the third quick action
	 * 
	 * @return "1" if it is possible, "0" otherwise
	 */
	private String checkThirdQuickAction() {
		if (this.helpers != 0)
			return "1";
		else
			return "0";
	}

	/**
	 * Checks if the player can do the fourth quick action
	 * 
	 * @return "1" if it is possible, "0" otherwise
	 */
	private String checkFourthQuickAction() {
		if (this.helpers > 2)
			return "1";
		else
			return "0";
	}

	/**
	 * Checks if the player has the special bonus: getBonusOfAnEmporium
	 * 
	 * @return "1" if the player has it, "0" otherwise
	 */
	private String checkGetBonusOfAnEmporium() {
		if (this.getBonusOfAnEmporium > 0)
			return "1";
		else
			return "0";
	}

	/**
	 * Checks if the player has the special bonus: pickAPermitCard
	 * 
	 * @return "1" if the player has it, "0" otherwise
	 */
	private String checkPickAPermitCard() {
		if (this.getPermitCardBonus > 0)
			return "1";
		else
			return "0";
	}

	/**
	 * Checks if the player has the special bonus: getPermitCardBonus
	 * 
	 * @return "1" if the player has it, "0" otherwise
	 */
	private String checkGetPermitCardBonus() {
		if (this.getPermitCardBonus > 0)
			return "1";
		else
			return "0";
	}

	/**
	 * @return the number of the emporiums of the player
	 */
	public int getEmporiumNumber() {
		return emporiums.size();
	}

	/**
	 * 
	 * @return mainAction
	 */
	public int getMainAction() {
		return this.mainAction;
	}

	/**
	 * 
	 * @return quickAction
	 */
	public int getQuickAction() {
		return this.quickAction;
	}

}
