package core;

import java.util.Properties;

public class Map {

	private City[] city;
	private Region[] region;
	private Color[] color;
	private Council[] council;
	private King king;
	private Nobility nobility;
	private Prize prize;
	private String mountain = "mountain";
	private String sea = "sea";
	private String hill = "hill";
	private String numberOfPermitCard = "numberOfPermitCard";
	private String numberOfCities = "numberOfCities";

	/**
	 * Constructor for Map. Map reads in server/propertiesName.properties and
	 * create the cities, the regions, the nobility bonuses, the councils and
	 * the colors, the king and the prize. Also the permitCards are created and
	 * setted in councils. Bonuses and connections of the cities and the bonuses
	 * associated by the permitCards can be modified in the file properties.
	 * 
	 * @param config
	 *            the game configuration file
	 */
	public Map(Properties config) {
		this.city = new City[Integer.parseInt(config.getProperty(numberOfCities))];
		this.region = new Region[Integer.parseInt(config.getProperty("numberOfRegion"))];
		this.council = new Council[Integer.parseInt(config.getProperty("numberOfCouncil"))];
		this.color = new Color[Integer.parseInt(config.getProperty("numberOfColor"))];
		Color yellow = new Color("yellow", 5);
		Color blue = new Color("blue", 2);
		Color grey = new Color("grey", 4);
		Color orange = new Color("orange", 3);
		color[0] = yellow;
		color[1] = blue;
		color[2] = grey;
		color[3] = orange;
		Region seaRegion = new Region(this.sea);
		Region hillRegion = new Region(this.hill);
		Region mountainRegion = new Region(this.mountain);
		region[0] = seaRegion;
		region[1] = hillRegion;
		region[2] = mountainRegion;
		PermitCard[] seaPermitCard = new PermitCard[Integer.parseInt(config.getProperty(numberOfPermitCard)) / 3];
		PermitCard[] hillPermitCard = new PermitCard[Integer.parseInt(config.getProperty(numberOfPermitCard)) / 3];
		PermitCard[] mountainPermitCard = new PermitCard[Integer.parseInt(config.getProperty(numberOfPermitCard)) / 3];
		populateSeaPermitCard(seaRegion, config, seaPermitCard);
		populateHillPermitCard(hillRegion, config, hillPermitCard);
		populateMountainPermitCard(mountainRegion, config, mountainPermitCard);
		Council seaCouncil = new Council(seaRegion, seaPermitCard);
		Council hillCouncil = new Council(hillRegion, hillPermitCard);
		Council mountainCouncil = new Council(mountainRegion, mountainPermitCard);
		this.council[0] = seaCouncil;
		this.council[1] = hillCouncil;
		this.council[2] = mountainCouncil;
		seaRegion.setCouncil(seaCouncil);
		hillRegion.setCouncil(hillCouncil);
		mountainRegion.setCouncil(mountainCouncil);
		populateSeaCity(seaRegion, config);
		populateHillCity(hillRegion, config);
		populateMountainCity(mountainRegion, config);
		setConnectedCity(config);
		king = new King(getCapital(config));
		nobility = new Nobility(generateNobilityBonus(config));
		prize = new Prize(config);
	}

	/**
	 * This method generate the String of the connected cities
	 * 
	 * @param prop
	 *            is the properties of the map
	 * @param number
	 *            is the number of that city
	 * @param region
	 *            is the name of the region
	 * @return the String of the connected cities
	 */
	private String createStringCities(Properties prop, int number, String region) {
		String name = region + number;
		return prop.getProperty(name);
	}

	/**
	 * This method populate the permitCard of the region: sea
	 * 
	 * @param sea
	 *            is the region of the permitCard
	 * @param prop
	 *            is the properties of the map
	 * @param seaPermitCards
	 *            are the permitCard of that region
	 */
	private void populateSeaPermitCard(Region sea, Properties prop, PermitCard[] seaPermitCards) {
		for (int i = 0; i != (Integer.parseInt(prop.getProperty(numberOfPermitCard)) / 3); i++) {
			seaPermitCards[i] = new PermitCard(createStringCities(prop, i, this.sea), createBonus(prop, i, this.sea),
					sea);
		}
	}

	/**
	 * This method populate the permitCard of the region: hill
	 * 
	 * @param hill
	 *            is the region of the permitCard
	 * @param prop
	 *            is the properties of the map
	 * @param hillPermitCard
	 *            are the permitCard of that region
	 */
	private void populateHillPermitCard(Region hill, Properties prop, PermitCard[] hillPermitCard) {
		for (int i = 0; i != (Integer.parseInt(prop.getProperty(numberOfPermitCard)) / 3); i++) {
			hillPermitCard[i] = new PermitCard(createStringCities(prop, i, this.hill), createBonus(prop, i, this.hill),
					hill);
		}
	}

	/**
	 * This method populate the permitCard of the region: mountain
	 * 
	 * @param mountain
	 *            is the region of the permitCard
	 * @param prop
	 *            is the properties of the map
	 * @param mountainPermitCard
	 *            are the permitCard of that region
	 */
	private void populateMountainPermitCard(Region mountain, Properties prop, PermitCard[] mountainPermitCard) {
		for (int i = 0; i != (Integer.parseInt(prop.getProperty(numberOfPermitCard)) / 3); i++) {
			mountainPermitCard[i] = new PermitCard(createStringCities(prop, i, this.mountain),
					createBonus(prop, i, this.mountain), mountain);
		}
	}

	/**
	 * This method populate the cities of the region: sea
	 * 
	 * @param sea
	 *            is the region of the cities
	 * @param prop
	 *            is the properties of the map
	 */
	private void populateSeaCity(Region sea, Properties prop) {
		for (int i = 0; i != (Integer.parseInt(prop.getProperty(numberOfCities)) / 3); i++) {
			this.city[i] = new City(getNumberOfConnections(prop, i), getCharOfTheCities(i), getCityColor(prop, i), sea,
					createBonus(prop, i, "city"));
			setColor(this.city[i]);
			setRegion(this.city[i]);
		}
	}

	/**
	 * This method populate the cities of the region: hill
	 * 
	 * @param hill
	 *            is the region of the cities
	 * @param prop
	 *            is the properties of the map
	 */
	private void populateHillCity(Region hill, Properties prop) {
		for (int i = Integer.parseInt(prop.getProperty(numberOfCities))
				/ 3; i != (Integer.parseInt(prop.getProperty(numberOfCities)) / 3) * 2; i++) {
			this.city[i] = new City(getNumberOfConnections(prop, i), getCharOfTheCities(i), getCityColor(prop, i), hill,
					createBonus(prop, i, "city"));
			setColor(this.city[i]);
			setRegion(this.city[i]);
		}
	}

	/**
	 * This method populate the cities of the region: mountain
	 * 
	 * @param mountain
	 *            is the region of the cities
	 * @param prop
	 *            is the properties of the map
	 */
	private void populateMountainCity(Region mountain, Properties prop) {
		for (int i = (Integer.parseInt(prop.getProperty(numberOfCities)) / 3)
				* 2; i != (Integer.parseInt(prop.getProperty(numberOfCities)) / 3) * 3; i++) {
			this.city[i] = new City(getNumberOfConnections(prop, i), getCharOfTheCities(i), getCityColor(prop, i),
					mountain, createBonus(prop, i, "city"));
			setColor(this.city[i]);
			setRegion(this.city[i]);
		}
	}

	/**
	 * This method get the number of connection by searching in the file
	 * properties
	 * 
	 * @param prop
	 *            is the properties of the map
	 * @param number
	 *            of the city to search in the properties
	 * @return the number of connections that have that city
	 */
	private int getNumberOfConnections(Properties prop, int number) {
		char x = getCharOfTheCities(number);
		String name = "city" + x + "Connection";
		return prop.getProperty(name).length();
	}

	/**
	 * This method return the char of the cities by an initial number
	 * 
	 * @param number
	 *            of the city
	 * @return the char of the cities
	 */
	private char getCharOfTheCities(int number) {
		return (char) (number + 65);
	}

	/**
	 * This method get the color of the city by searching it in the properties
	 * file
	 * 
	 * @param prop
	 *            is the properties of the map
	 * @param number
	 *            of the city
	 * @return the color associated of that city
	 */
	private Color getCityColor(Properties prop, int number) {
		char x = getCharOfTheCities(number);
		String name = "city" + x + "Color";
		for (int i = 0; i != this.color.length; i++)
			if (this.color[i].getName().equals(prop.getProperty(name)))
				return this.color[i];
		return null;
	}

	/**
	 * This method create the bonus of the city or of the permitCard by
	 * searching it in the properties file
	 * 
	 * @param prop
	 *            is the properties of the map
	 * @param number
	 *            of the city or of the permitCard
	 * @param tipe
	 *            is a String, "city" if you want a city, "permitCard" if you
	 *            want a permitCard
	 * @return the bonus of the city
	 */
	private Bonus createBonus(Properties prop, int number, String tipe) {
		String name = generateKey(number, tipe);
		int moneyBonus = 0;
		int helpersBonus = 0;
		int victoryBonus = 0;
		int politicalCardBonus = 0;
		int nobilityBonus = 0;
		int actionsBonus = 0;
		int emporiumNobilityBonus = 0;
		int pickPermitCardBonus = 0;
		int permitCardBonus = 0;
		for (int i = 0; i != prop.getProperty(name).length(); i++) {
			char lettera = prop.getProperty(name).charAt(i);
			switch (lettera) {
			case 'a':
				actionsBonus = Character.getNumericValue(prop.getProperty(name).charAt(i - 2));
				break;
			case 'h':
				helpersBonus = Character.getNumericValue(prop.getProperty(name).charAt(i - 2));
				break;
			case 'm':
				moneyBonus = Character.getNumericValue(prop.getProperty(name).charAt(i - 2));
				break;
			case 'v':
				victoryBonus = Character.getNumericValue(prop.getProperty(name).charAt(i - 2));
				break;
			case 'n':
				nobilityBonus = Character.getNumericValue(prop.getProperty(name).charAt(i - 2));
				break;
			case 'p':
				politicalCardBonus = Character.getNumericValue(prop.getProperty(name).charAt(i - 2));
				break;
			case 's':
				emporiumNobilityBonus = Character.getNumericValue(prop.getProperty(name).charAt(i - 2));
				break;
			case 'g':
				pickPermitCardBonus = Character.getNumericValue(prop.getProperty(name).charAt(i - 2));
				break;
			case 'c':
				permitCardBonus = Character.getNumericValue(prop.getProperty(name).charAt(i - 2));
				break;
			default:
			}
		}
		int[] bonus = { moneyBonus, helpersBonus, victoryBonus, politicalCardBonus, nobilityBonus, actionsBonus,
				emporiumNobilityBonus, pickPermitCardBonus, permitCardBonus };
		return new Bonus(bonus);
	}

	/**
	 * Set the color of the city
	 * 
	 * @param city
	 *            that you want to set
	 */
	private void setColor(City city) {
		for (int j = 0; j != 4; j++)
			if (city.getColor() == color[j])
				color[j].setCity(city);
	}

	/**
	 * Set the region of the city
	 * 
	 * @param city
	 *            that you want to set
	 */
	private void setRegion(City city) {
		for (int j = 0; j != 3; j++)
			if (city.getRegion() == region[j])
				region[j].setCity(city);
	}

	/**
	 * 
	 * @param charOfTheCity
	 *            is the char of the city that you want to search
	 * @return the city associated with its char, null if there isnt
	 */
	public City searchCityByChar(char charOfTheCity) {
		for (City x : this.city)
			if (x.getCharOfTheCity() == charOfTheCity)
				return x;
		return null;
	}

	/**
	 * 
	 * @param name
	 *            is a String with the name of the region
	 * @return the council associated with its name of the region
	 */
	public Council getCouncilByName(String name) {
		if (name.equals(king.getCouncil().getName()))
			return king.getCouncil();
		for (Council x : this.council)
			if (x.getRegion().getName().equals(name))
				return x;
		return null;
	}

	/**
	 * Set all connected cities to them
	 * 
	 * @param prop
	 *            is the properties of the map
	 */
	private void setConnectedCity(Properties prop) {
		String key;
		char charOfTheConnectedCities;
		for (int j = 0; j != this.city.length; j++) {
			key = "city" + getCharOfTheCities(j) + "Connection";
			for (int i = 0; i != prop.getProperty(key).length(); i++) {
				charOfTheConnectedCities = prop.getProperty(key).charAt(i);
				for (int z = 0; z != this.city.length; z++)
					if (charOfTheConnectedCities == this.city[z].getCharOfTheCity())
						this.city[j].setConnectedCity(this.city[z]);
			}
		}
	}

	/**
	 * This method return the city "capital"
	 * 
	 * @param prop
	 *            is the properties of the map
	 * @return city that is the capital
	 */
	private City getCapital(Properties prop) {
		char x;
		String name;
		for (int i = 0; i != this.city.length; i++) {
			x = getCharOfTheCities(i);
			name = "city" + x + "Color";
			if ("capital".equals(prop.getProperty(name)))
				return searchCityByChar(x);
		}
		return null;
	}

	/**
	 * This method generate an array of bonus for the nobility
	 * 
	 * @param prop
	 *            is the properties of the map
	 * @return bonus that is an array of bonus for the nobility
	 */
	private Bonus[] generateNobilityBonus(Properties prop) {
		Bonus[] bonus = new Bonus[21];
		for (int i = 0; i != Integer.parseInt(prop.getProperty("numberOfNobilityLevel")); i++)
			bonus[i] = createBonus(prop, i, "nobility");
		return bonus;
	}

	/**
	 * 
	 * @param number
	 *            of the city or the permitCard
	 * @param tipe
	 *            is a String, "city" if you want a city, "permitCard" if you
	 *            want a permitCard
	 * @return name is the key for the properties
	 */
	private String generateKey(int number, String tipe) {
		char x;
		String name;
		if (tipe == "city") {
			x = getCharOfTheCities(number);
			name = tipe + x + "Bonus";
		} else
			name = tipe + number + "Bonus";
		return name;
	}

	/**
	 * 
	 * @return city
	 */
	public City[] getCity() {
		return this.city;
	}

	/**
	 * 
	 * @return region
	 */
	public Region[] getRegion() {
		return this.region;
	}

	/**
	 * 
	 * @return color
	 */
	public Color[] getColor() {
		return this.color;
	}

	/**
	 * 
	 * @return council
	 */
	public Council[] getCouncil() {
		return this.council;
	}

	/**
	 * 
	 * @return king
	 */
	public King getKing() {
		return this.king;
	}

	/**
	 * 
	 * @return nobility
	 */
	public Nobility getNobility() {
		return this.nobility;
	}

	/**
	 * Generate a graph of the cities
	 * 
	 * @return graph like: graph[3][3]= (0 0 1) (0 1 1) (1 0 0)
	 */
	public int[][] generateGraphOfCities() {
		int[][] graph = new int[this.city.length][this.city.length];
		for (int i = 0; i != this.city.length; i++)
			for (int j = 0; j != this.city.length; j++)
				for (int k = 0; k != this.city[i].getConnectedCities().length; k++)
					if ((int) (this.city[i].getConnectedCities()[k].getCharOfTheCity()) - 65 == j)
						graph[i][j] = 1;

		return graph;
	}

	/**
	 * 
	 * @return buff an int[][] that is a graph with the distance of the cities
	 */
	public int[][] generateDistanziatedGraphOfCities() {
		int[][] graph = generateGraphOfCities();
		int[][] buff = new int[graph.length][graph.length];
		int[] dist;
		for (int i = 0; i != buff.length; i++) {
			dist = dijkstra(graph, i);
			for (int j = 0; j != buff.length; j++)
				buff[i][j] = dist[j];
		}
		return buff;
	}

	/**
	 * 
	 * @return prize
	 */
	public Prize getPrize() {
		return this.prize;
	}

	/**
	 * This method use the dijkstra algorithm for the minimum path for calculate
	 * how far is the city from the location of the king
	 * 
	 * @param graph
	 *            is the graph of the map
	 * @param position
	 *            of the city that you want to calculate
	 * @return dist is an int[] with the distance of all the other city
	 */
	private int[] dijkstra(int[][] graph, int position) {
		int[] dist = new int[graph.length];
		Boolean[] sptSet = new Boolean[graph.length];
		for (int i = 0; i != graph.length; i++) {
			dist[i] = Integer.MAX_VALUE;
			sptSet[i] = false;
		}
		dist[position] = 0;
		for (int count = 0; count != graph.length - 1; count++) {
			int u = minDistance(dist, sptSet);
			sptSet[u] = true;
			for (int v = 0; v != graph.length; v++)
				if (!sptSet[v] && graph[u][v] != 0 && dist[u] != Integer.MAX_VALUE && dist[u] + graph[u][v] < dist[v])
					dist[v] = dist[u] + graph[u][v];
		}
		return dist;
	}

	/**
	 * @param dist
	 *            is an array needed for the algorithm
	 * @param sptSet
	 *            is an array of boolean to know if you have already searched in
	 *            that position
	 * @return the minumum distance
	 */
	private int minDistance(int[] dist, Boolean[] sptSet) {
		int min = Integer.MAX_VALUE;
		int minIndex = -1;
		for (int v = 0; v < dist.length; v++)
			if (!sptSet[v] && dist[v] <= min) {
				min = dist[v];
				minIndex = v;
			}
		return minIndex;
	}

	/**
	 * This method generate the map with the connections for the command line
	 * 
	 * @param prop
	 *            is the properties of the map
	 * @return commandLineMap that is a string of the map for the command line
	 */
	public String generateCommandLineMap(Properties prop) {
		char[][] map = generateMap(prop);
		String commandLineMap = new String();
		for (int i = 0; i != 3; i++) {
			for (int j = 0; j != 6; j++) {
				commandLineMap += generateTopPart(map, i, j);
			}
			commandLineMap += ("\n");
			for (int k = 0, c = i + 1; k != 6 && c != 3; k++) {
				commandLineMap += generateBottomPart(map, c, k, i);
			}
			commandLineMap += "\n";
		}
		return commandLineMap;
	}

	/**
	 * This method generate the top part of the command line map
	 * 
	 * @param map
	 *            the map with all the cities
	 * @param i
	 *            is the row
	 * @param j
	 *            is the line
	 * @return a String that is the top part of the command line map
	 */
	private String generateTopPart(char[][] map, int i, int j) {
		String commandLineMap = new String();
		if (map[i][j] != 'z') {
			commandLineMap += Character.toString(map[i][j]);

			if (j != 5 && searchCityByChar(map[i][j]) != null && searchCityByChar(map[i][j + 1]) != null
					&& searchCityByChar(map[i][j]).isConnectedTo(searchCityByChar(map[i][j + 1])))
				commandLineMap += "----";
			else if (j != 5 && searchCityByChar(map[i][j + 1]) != null)
				commandLineMap += "    ";
		} else {
			if (j != 5 && searchCityByChar(map[i][j - 1]) != null && searchCityByChar(map[i][j + 1]) != null
					&& searchCityByChar(map[i][j - 1]).isConnectedTo(searchCityByChar(map[i][j + 1])))
				commandLineMap += "---------";
			else if (j != 5 && searchCityByChar(map[i][j - 1]) != null && searchCityByChar(map[i][j + 2]) != null
					&& searchCityByChar(map[i][j - 1]).isConnectedTo(searchCityByChar(map[i][j + 2])))
				commandLineMap += "--------------";
		}
		return commandLineMap;
	}

	/**
	 * This method generate the bottom part of the command line map
	 * 
	 * @param map
	 *            the map with all the cities
	 * @param c
	 *            is the row
	 * @param k
	 *            is the line
	 * @param i
	 *            is the precedent row
	 * @return a String that is the bottom part of the command line map
	 */
	private String generateBottomPart(char[][] map, int c, int k, int i) {
		String commandLineMap = new String();
		if (k != 0 && searchCityByChar(map[c][k]) != null && searchCityByChar(map[i][k - 1]) != null
				&& searchCityByChar(map[c][k]).isConnectedTo(searchCityByChar(map[i][k - 1]))) {
			commandLineMap += "\\ ";
		} else if (k != 0)
			commandLineMap += "  ";
		if (searchCityByChar(map[c][k]) != null && searchCityByChar(map[i][k]) != null
				&& searchCityByChar(map[c][k]).isConnectedTo(searchCityByChar(map[i][k]))) {
			commandLineMap += "|";
		} else
			commandLineMap += " ";
		if (k != 5 && searchCityByChar(map[c][k]) != null && searchCityByChar(map[i][k + 1]) != null
				&& searchCityByChar(map[c][k]).isConnectedTo(searchCityByChar(map[i][k + 1]))) {
			commandLineMap += " /";
		} else
			commandLineMap += "  ";
		return commandLineMap;
	}

	/**
	 * This method generate a map of char[3][6]
	 * 
	 * @param prop
	 *            is the properties of the map
	 * @return map
	 */
	private char[][] generateMap(Properties prop) {
		char[][] map = new char[3][6];
		for (int i = 0; i != 3; i++)
			for (int j = 0; j != 6; j++) {
				if (prop.getProperty("map" + i + j).charAt(0) != 0)
					map[i][j] = prop.getProperty("map" + i + j).charAt(0);
				else
					map[i][j] = 'z';
			}
		return map;
	}

}
