package core;

public class Offer {

	private PlayerImpl playerImpl;
	private PermitCard permitCard;
	private int helpers = 0;
	private char[] politicalCard;
	private int price;

	/**
	 * Constructor for Offer
	 * 
	 * @param playerImpl
	 *            is the player that do the offer
	 * @param politicalCard
	 *            are the politicalCard that the player want to add
	 * @param permitCard
	 *            are the permitCard that the player want to add
	 * @param helpers
	 *            are the helpers that the player want to add
	 * @param price
	 *            are the price that the player want to add
	 */
	public Offer(PlayerImpl playerImpl, char[] politicalCard, PermitCard permitCard, int helpers, int price) {
		this.playerImpl = playerImpl;
		this.politicalCard = politicalCard;
		this.permitCard = permitCard;
		this.price = price;
		this.helpers = helpers;
	}

	/**
	 * 
	 * @return player
	 */
	public PlayerImpl getSeller() {
		return playerImpl;
	}

	/**
	 * 
	 * @return permitCard
	 */
	public PermitCard getPermitCard() {
		return permitCard;
	}

	/**
	 * 
	 * @return politicalCard
	 */
	public char[] getPoliticalCards() {
		return politicalCard;
	}

	/**
	 * 
	 * @return helpers
	 */
	public int getHelpers() {
		return helpers;
	}

	/**
	 * 
	 * @return price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * 
	 * @return the price in a bonus
	 */
	public Bonus getPaymentBonus() {

		return new Bonus(new int[] { price, 0, 0, 0, 0, 0, 0, 0, 0 });
	}
}
