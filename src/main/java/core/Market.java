package core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Market {

	private ArrayList<Offer> offers;
	private boolean offersCollected = false;
	private String offer = "offer";
	private String permitCard = "PermitCard";

	/**
	 * Constructor for Market
	 */
	public Market() {
		this.offers = new ArrayList<>();
	}

	/**
	 * @param offer
	 *            that is added
	 */
	public void addOffer(Offer offer) {

		this.offers.add(offer);

	}

	/**
	 * 
	 * @param offer
	 *            that is removed
	 */
	public void removeOffer(Offer offer) {

		this.offers.remove(offer);

	}

	/**
	 * This method is for buy an offer
	 * 
	 * @param offerNumber
	 *            is the number of the offer
	 * @return the offer that is removed
	 * @throws InvalidMoveException
	 *             is called if the number of the offers does not exist
	 */
	public Offer buyOffer(int offerNumber) throws InvalidMoveException {

		try {

			return offers.remove(offerNumber);

		} catch (ArrayIndexOutOfBoundsException e) {
			throw new InvalidMoveException("The offer chosen does not exist!", e);

		}

	}

	/**
	 * 
	 * @param offerNumber
	 *            is the number of the offer
	 * @return the price of an offer
	 * @throws InvalidMoveException
	 *             is called if the number of the offers does not exist
	 */
	public int getOfferPrice(int offerNumber) throws InvalidMoveException {
		if (offerNumber < offers.size())
			return offers.get(offerNumber).getPrice();
		else
			throw new InvalidMoveException("Offer does not exist");

	}

	/**
	 * say that the offers are all collected
	 */
	public void done() {
		this.offersCollected = true;
	}

	/**
	 * 
	 * @return offersCollected
	 */
	public boolean getOfferCollected() {
		return offersCollected;
	}

	/**
	 * 
	 * @return offers
	 */
	public List<Offer> getOffers() {
		return this.offers;
	}

	/**
	 * This method generate an HashMap of the Market
	 * 
	 * @return the HashMap
	 */
	public HashMap<String, String> getMarketState() {
		HashMap<String, String> hashMap = new HashMap<>();

		for (int i = 0; i != this.offers.size(); i++) {
			String buff = new String();
			hashMap.put(offer + i + "Player", this.offers.get(i).getSeller().getUsername());
			if (this.offers.get(i).getPoliticalCards() != null) {
				for (char c : this.offers.get(i).getPoliticalCards())
					buff += Character.toString(c);
				hashMap.put(offer + i + "PoliticalCard", buff);
			}
			if (this.offers.get(i).getPermitCard() != null) {
				hashMap.put(offer + i + permitCard + "Cities", this.offers.get(i).getPermitCard().getCities());
				hashMap.put(offer + i + permitCard + "Bonus", this.offers.get(i).getPermitCard().getBonus().toString());

				hashMap.put(offer + i + permitCard + "Used",
						Boolean.toString(this.offers.get(i).getPermitCard().isUsed()));
			}
			hashMap.put(offer + i + "Price", Integer.toString(this.offers.get(i).getPrice()));

			hashMap.put(offer + i + "Helpers", Integer.toString(offers.get(i).getHelpers()));

		}
		hashMap.put("numberOfOffer", Integer.toString(this.offers.size()));
		return hashMap;
	}

	/**
	 * This method remove all the offers in the market
	 */
	public void clear() {
		offers.clear();
	}

}
