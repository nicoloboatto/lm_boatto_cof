package core;

import java.util.List;

public class Color {

	private String name;
	private City[] cities;
	private boolean occupied = false;
	private Bonus bonus;

	/**
	 * Constructor for Color
	 * 
	 * @param name
	 *            of the color
	 * @param numberOfTheCities
	 *            that have this color
	 */
	public Color(String name, int numberOfTheCities) {
		setName(name);
		this.cities = new City[numberOfTheCities];
		setBonus(numberOfTheCities);
	}

	/**
	 * Constructor of Color for the king
	 * 
	 * @param city
	 *            is the capital where is the king
	 */
	public Color(City city) {
		this.name = "capital";
		this.cities = new City[1];
		this.cities[0] = city;
		int[] bonusOfTheKing = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		this.bonus = new Bonus(bonusOfTheKing);
	}

	/**
	 * This method check if the player has all the city of the same color
	 * 
	 * @param playerCities
	 *            the list of cities the player owns
	 * @return true if the player has all the cities of the same color, false
	 *         otherwise
	 */
	public boolean checkColor(List<City> playerCities) {
		int count = 0;
		for (int i = 0; i != this.cities.length; i++)
			for (int j = 0; j != playerCities.size(); j++)
				if (!occupied && this.cities[i] == playerCities.get(j))
					count++;
		if (count == this.cities.length) {
			occupied = true;
			return true;
		}
		return false;
	}

	/**
	 * Put the city in the first free space
	 * 
	 * @param city
	 *            that you want to add
	 */
	public void setCity(City city) {
		for (int i = 0; i != this.cities.length; i++)
			if (this.cities[i] == null)
				this.cities[i] = city;
	}

	/**
	 * Set the bonus of the color based by the number of the cities that have
	 * that color
	 * 
	 * @param numberOfTheCities
	 *            that have that color
	 */
	private void setBonus(int numberOfTheCities) {
		int victory;
		switch (numberOfTheCities) {
		case 1:
			victory = 0;
			break;
		case 2:
			victory = 5;
			break;
		case 3:
			victory = 8;
			break;
		case 4:
			victory = 12;
			break;
		case 5:
			victory = 20;
			break;
		default:
			victory = 0;
			break;
		}
		int[] bonusArray = { 0, 0, victory, 0, 0, 0, 0, 0, 0 };
		this.bonus = new Bonus(bonusArray);
	}

	/**
	 * 
	 * @param name
	 *            set the name of the color
	 */
	private void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @param cities
	 *            set the cities of the color
	 */
	public void setCities(City[] cities) {
		this.cities = cities;
	}

	/**
	 * 
	 * @return city
	 */
	public City[] getCities() {
		return this.cities;
	}

	/**
	 * 
	 * @return name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * 
	 * @return occupied
	 */
	public boolean getOccupied() {
		return this.occupied;
	}

	/**
	 * 
	 * @return bonus
	 */
	public Bonus getBonus() {
		return this.bonus;
	}

}
