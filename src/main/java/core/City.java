package core;

import java.util.Arrays;

public class City {

	private char charOfTheCity;
	private Color color;
	private Region region;
	private Bonus bonus;
	private City[] connectedCities;

	/**
	 * Constructor for City
	 * 
	 * @param numberOfConnection
	 *            the number of connection that this city has
	 * @param charOfTheCity
	 *            is the char of the city, between "A" and "O"
	 * @param color
	 *            is the color of the city
	 * @param region
	 *            is the region of the city
	 * @param bonus
	 *            is the bonus of the city
	 */
	public City(int numberOfConnection, char charOfTheCity, Color color, Region region, Bonus bonus) {
		this.connectedCities = new City[numberOfConnection];
		this.charOfTheCity = charOfTheCity;
		this.color = color;
		this.region = region;
		this.bonus = bonus;
	}

	/**
	 * This method put a city that you want to connect in the first free space
	 * of a city
	 * 
	 * @param city
	 *            is the city that you want to connect to an another city
	 */
	public void setConnectedCity(City city) {
		for (int i = 0; i != this.connectedCities.length; i++) {
			if (this.connectedCities[i] == null) {
				this.connectedCities[i] = city;
				break;
			}
		}
	}

	/**
	 * 
	 * @return charOfTheCity
	 */
	public char getCharOfTheCity() {
		return this.charOfTheCity;
	}

	/**
	 * 
	 * @param cityToCheck
	 *            the city to check
	 * @return true if cityToCheck is connected to another city, else return
	 *         false
	 */
	public boolean isConnectedTo(City cityToCheck) {
		return Arrays.asList(this.connectedCities).contains(cityToCheck);
	}

	/**
	 * 
	 * @return color
	 */
	public Color getColor() {
		return this.color;
	}

	/**
	 * Set the color of the city
	 * 
	 * @param color
	 *            that you want to set
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * 
	 * @return region
	 */
	public Region getRegion() {
		return this.region;
	}

	/**
	 * 
	 * @return bonus
	 */
	public Bonus getBonus() {
		return this.bonus;
	}

	/**
	 * 
	 * @return connectedCities
	 */
	public City[] getConnectedCities() {
		return this.connectedCities;
	}

}