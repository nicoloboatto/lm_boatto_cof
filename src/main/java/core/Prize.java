package core;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author Giorgio
 *
 */
public class Prize {

	private ArrayList<Bonus> bonus;

	/**
	 * Constructor of Prize
	 * 
	 * @param prop
	 *            is the properties of the game
	 */
	public Prize(Properties prop) {
		this.bonus = new ArrayList<>();
		for (int i = 0; i != Integer.parseInt(prop.getProperty("numberOfPrize")); i++)
			this.bonus.add(createBonus(prop, i));
	}

	/**
	 * This method create a bonus by the file properties
	 * 
	 * @param prop
	 *            is the properties of the game
	 * @param number
	 *            of the prize
	 * @return a new bonus
	 */
	private Bonus createBonus(Properties prop, int number) {
		String name = "prize" + number + "Bonus";
		int[] bonusPrize = { 0, 0, Integer.parseInt(prop.getProperty(name)), 0, 0, 0, 0, 0, 0 };
		return new Bonus(bonusPrize);
	}

	/**
	 * 
	 * @return a list of bonus
	 */
	public List<Bonus> getBonus() {
		return bonus;
	}

}
