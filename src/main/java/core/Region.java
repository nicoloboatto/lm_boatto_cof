package core;

import java.util.List;

public class Region {

	private City[] cities;
	private Bonus bonus;
	private boolean occupied = false;
	private Council council;
	private String name;

	/**
	 * Constructor of Region
	 * 
	 * @param name
	 *            of the region
	 */
	public Region(String name) {
		int[] regionBonus = { 0, 0, 5, 0, 0, 0, 0, 0, 0 };
		this.bonus = new Bonus(regionBonus);
		this.cities = new City[5];
		this.name = name;
	}

	/**
	 * This method check if the player has all the city of the same region
	 * 
	 * @param playerCities
	 *            the list of cities the player owns
	 * @return true if the player has all the cities in the region, false
	 *         otherwise
	 */
	public boolean checkCities(List<City> playerCities) {
		int count = 0;
		for (int i = 0; i != this.cities.length; i++)
			for (int j = 0; j != playerCities.size(); j++)
				if (!occupied && this.cities[i] == playerCities.get(j))
					count++;
		if (count == this.cities.length) {
			occupied = true;
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @return the Bonus associated with completing the region
	 */
	public Bonus getBonus() {
		return this.bonus;
	}

	/**
	 * 
	 * @return the council
	 */
	public Council getCouncil() {
		return this.council;
	}

	/**
	 * 
	 * @param council
	 *            is the council of the region
	 */
	public void setCouncil(Council council) {
		this.council = council;
	}

	/**
	 * 
	 * @param city
	 *            is an array of cities that you want to add in the region
	 */
	public void setCities(City[] city) {
		this.cities = city;
	}

	/**
	 * Put the city in the first free space
	 * 
	 * @param city
	 *            is the city that you want to add in the region
	 */
	public void setCity(City city) {
		for (int i = 0; i != this.cities.length; i++)
			if (this.cities[i] == null)
				this.cities[i] = city;
	}

	/**
	 * 
	 * @return name
	 */
	public String getName() {
		return this.name;
	}

}