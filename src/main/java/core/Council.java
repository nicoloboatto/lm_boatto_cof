package core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class Council {

	private char[] colors;
	private Region region;
	private ArrayList<PermitCard> permitCards;
	private String name;

	/**
	 * Constructor of Council
	 * 
	 * @param region
	 *            of the council
	 * @param permitCards
	 *            of the council
	 */
	public Council(Region region, PermitCard[] permitCards) {
		this.region = region;
		this.colors = new char[4];
		for (int i = 0; i != 4; i++)
			colors[i] = getRandomColor();
		this.permitCards = new ArrayList<>();
		for (int i = 0; i != permitCards.length; i++)
			this.permitCards.add(permitCards[i]);
		this.name = region.getName();
		shufflePermits();
	}

	/**
	 * Constructor of Council for the King
	 */
	public Council() {
		this.colors = new char[4];
		for (int i = 0; i != 4; i++) {
			colors[i] = getRandomColor();
		}
		this.name = new String("king");
		this.permitCards = new ArrayList<>();
	}

	/**
	 * pop the last color, slide all the array and insert in the position [0]
	 * the new color
	 * 
	 * @param color
	 *            that you want to add
	 */
	public void slideOn(char color) {
		for (int i = 2; i != -1; i--) {
			colors[i + 1] = colors[i];
		}
		colors[0] = color;
		return;
	}

	/**
	 * 
	 * @return a random color
	 */
	private static char getRandomColor() {
		int x = (int) (Math.random() * 5);
		char color;
		switch (x) {

		case 1:
			color = 'w';
			break;
		case 2:
			color = 'o';
			break;
		case 3:
			color = 'p';
			break;
		case 4:
			color = 'b';
			break;
		default:
			color = 'k';
		}
		return color;
	}

	/**
	 * 
	 * @return colors
	 */
	public char[] getColors() {
		return this.colors;
	}

	/**
	 * 
	 * @return region
	 */
	public Region getRegion() {
		return this.region;
	}

	/**
	 * 
	 * @return permitCards
	 */
	public List<PermitCard> getPermitCards() {
		return this.permitCards;
	}

	/**
	 * This method take the first two permitCard and it puts them in the last
	 * two position of the list
	 */
	public void refreshPermits() {
		PermitCard one = this.permitCards.get(0);
		PermitCard two = this.permitCards.get(1);
		this.permitCards.remove(0);
		this.permitCards.remove(0);
		this.permitCards.add(one);
		this.permitCards.add(two);
	}

	/**
	 * This method shuffle all the list of the permitCards
	 */
	private void shufflePermits() {
		Collections.shuffle(this.permitCards);
	}

	/**
	 * This method generate an HashMap of the Council
	 * 
	 * @return buff
	 */
	public HashMap<String, String> getState() {
		HashMap<String, String> buff = new HashMap<>();
		String value;
		value = String.copyValueOf(this.colors);
		buff.put(this.name + "Council", value);
		if (!this.permitCards.isEmpty())
			for (int j = 0; j != 2; j++) {
				buff.put(name + j, this.permitCards.get(j).getCities());
				buff.put(name + j + "Bonus", this.permitCards.get(j).getBonus().toString());
			}
		return buff;
	}

	/**
	 * Set the region
	 * 
	 * @param region
	 *            that you want to set
	 */
	public void setRegion(Region region) {
		this.region = region;
	}

	/**
	 * Remove a permitCard in that position
	 * 
	 * @param position
	 *            of the permitCard
	 * @return the permitCard removed
	 */
	public PermitCard removePermitCard(int position) {
		return permitCards.remove(position);
	}

	/**
	 * 
	 * @return name
	 */
	public String getName() {
		return this.name;
	}
}
