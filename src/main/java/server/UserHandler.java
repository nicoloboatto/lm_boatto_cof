package server;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;

import client.NetworkException;
import core.PlayerImpl;

public interface UserHandler {

	/**
	 * 
	 * @param lobbyImpl
	 *            the lobby to be set
	 * @throws IOException
	 *             in case there was an error in the underlying network
	 *             implementation
	 */
	public void setLobby(LobbyImpl lobbyImpl) throws IOException;

	/**
	 * 
	 * @return the lobby that contains this user
	 */
	public LobbyImpl getLobby();

	/**
	 * Sets a player to be used by the client for the duration of a game
	 * 
	 * @param player
	 *            the player to be set for the game
	 * @throws IOException
	 *             in case there was an error in the underlying network
	 *             implementation
	 */
	public void setPlayer(PlayerImpl player) throws IOException;


	/**
	 * 
	 * @return true if the player is playing, false otherwise
	 */
	public boolean isPlaying();

	/**
	 * Delivers the user any information that has changed since the last move
	 * 
	 * 
	 * @param stateUpdate
	 *            the update to be sent to the user
	 * @throws IOException
	 *             in case there was an error in the underlying network
	 *             implementation
	 */
	public void update(HashMap<String, String> stateUpdate) throws IOException;

	/**
	 * 
	 * 
	 * @return the username
	 */
	public String getUsername();

	/**
	 * Tries to contact the user, used to check if the user is still online
	 * 
	 * @throws NetworkException
	 *             in case there was an error in the underlying network
	 *             implementation
	 */
	public void keepAlive() throws NetworkException;

	/**
	 * Adds the time passed since the user connected to the user's total game
	 * time, Updates the user data on the database
	 * 
	 *
	 */
	public void cleanUp();

}
