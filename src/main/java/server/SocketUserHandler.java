package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import client.Commands;
import client.NetworkException;
import core.PlayerImpl;

public class SocketUserHandler implements UserHandler, Runnable {

	// Logging functionality
	static final Logger logger = Logger.getLogger(SocketUserHandler.class.getName());

	private User user;
	private long connectionTime = new Date().getTime();
	private boolean playing = true;
	private LobbyImpl lobbyImpl;
	private PlayerImpl player;

	private ObjectInputStream objectInputStream;
	private ObjectOutputStream objectOutputStream;

	/**
	 * 
	 * @param socket
	 *            this is used to get the input and output stream to the user
	 * @throws IOException
	 *             In case the initialization of the socket does not work
	 */
	public SocketUserHandler(Socket socket) throws IOException {
		logger.severe("New User Connected");
		objectInputStream = new ObjectInputStream(socket.getInputStream());
		objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
		objectOutputStream.flush();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {

		try {

			while (!authenticate())
				;
			GameServer.addUser(this);
			while (isPlaying()) {
				waitForAction();
			}

		} catch (IOException e) {
			logger.log(Level.WARNING, "User disconnected!", e);
			cleanUp();
		}
	}

	/**
	 * Waits for the client to output a command to the socket and proceeds to
	 * set it up for interpretation
	 * 
	 * @throws IOException
	 *             in case there is and error reading from the socket
	 */
	private void waitForAction() throws IOException {
		try {
			String command = (String) objectInputStream.readObject();
			if (lobbyImpl != null) {
				interpretLobbyAction(command);
			} else {
				interpretMove(command);
			}
		} catch (ClassNotFoundException e) {
			logger.log(Level.WARNING, "Unknown object received from Client", e);
		}

	}

	/**
	 * Interprets the lobby action performed by the client
	 * 
	 * @param command
	 *            the command sent by the player
	 * @throws ClassNotFoundException
	 *             in case an unknown object is received from the client
	 * @throws IOException
	 *             in case there is an error reading from the socket
	 */
	private void interpretLobbyAction(String command) throws ClassNotFoundException, IOException {
		switch (command) {
		case Commands.START_GAME:
			lobbyImpl.startGame();
			lobbyImpl = null;
			break;
		case Commands.SET_CONFIGURATION:
			lobbyImpl.setConfiguration((String) objectInputStream.readObject());
			break;
		default:
			break;
		}

	}

	/**
	 * Interprets the game action performed by the client
	 * 
	 * @param command
	 *            the command sent by the player
	 * @throws ClassNotFoundException
	 *             in case an unknown object is received from the client
	 * @throws IOException
	 *             in case there is an error reading from the socket
	 */
	private void interpretMove(String command) throws ClassNotFoundException, IOException {
		switch (command) {

		case Commands.BUY_PERMIT:
			player.buyPermitCard((String) objectInputStream.readObject(), (int) objectInputStream.readObject(),
					(char[]) objectInputStream.readObject());
			break;
		case Commands.SLIDE_COUNCIL:
			player.slideCouncil((char) objectInputStream.readObject(), (String) objectInputStream.readObject());
			break;
		case Commands.BUY_HELPER:
			player.buyHelper();
			break;
		case Commands.BUY_KING:
			player.buyKing((char[]) objectInputStream.readObject(), (char) objectInputStream.readObject());
			break;
		case Commands.BUY_MAIN_ACTION:
			player.buyMainAction();
			break;
		case Commands.ADD_COUNCIL_MEMBER:
			player.addAMemberOfTheCouncil((String) objectInputStream.readObject(),
					(char) objectInputStream.readObject());
			break;
		case Commands.SKIP_ACTION:
			player.skipAction();
			break;
		case Commands.PUT_OFFER:
			player.putOffer((char[]) objectInputStream.readObject(), (int) objectInputStream.readObject(),
					(int) objectInputStream.readObject(), (int) objectInputStream.readObject());
			break;
		case Commands.PICK_PERMIT:
			player.pickPermitCard((String) objectInputStream.readObject(), (int) objectInputStream.readObject());
			break;

		case Commands.GET_PERMIT_BONUS:
			player.getPermitCardBonus((int) objectInputStream.readObject());
			break;

		case Commands.GET_EMPORIUM_BONUS:
			player.getBonusOfAnEmporium((char) objectInputStream.readObject());
			break;

		case Commands.PUT_EMPORIUM:
			player.putEmporiumInACity((int) objectInputStream.readObject(), (char) objectInputStream.readObject());
			break;

		case Commands.REFRESH_PERMITS:
			player.refreshPermitCards((String) objectInputStream.readObject());
			break;

		case Commands.BUY_OFFER:
			player.buyOffer((int) objectInputStream.readObject());
			break;

		default:
			break;
		}

	}

	/**
	 * Reads username and password from socket and writes true if login is
	 * successful, false otherwise
	 * 
	 * @throws IOException
	 *             if there are errors in the underlying socket
	 */
	private boolean authenticate() throws IOException {

		String command;
		boolean succesful = false;

		while (!succesful) {
			try {
				command = (String) objectInputStream.readObject();
				switch (command) {

				case "login":

					succesful = login((String) objectInputStream.readObject(), (String) objectInputStream.readObject());
					objectOutputStream.writeBoolean(succesful);
					objectOutputStream.flush();
					break;

				case "register":

					succesful = register((String) objectInputStream.readObject(),
							(String) objectInputStream.readObject());
					objectOutputStream.writeBoolean(succesful);
					objectOutputStream.flush();
					break;

				default:
					objectOutputStream.writeBoolean(false);
					objectOutputStream.flush();
					return false;
				}

			} catch (ClassNotFoundException e) {

				logger.log(Level.WARNING, "Illegal command received from user", e);
				return false;
			}
		}
		objectOutputStream.flush();
		return succesful;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.UserHandler#cleanUp()
	 */
	@Override
	public void cleanUp() {

		GameServer.removeUser(this);
		if (this.lobbyImpl != null)
			lobbyImpl.removeUser(this);
		try {
			DatabaseManager.updateUser(user);
		} catch (SQLException e) {
			logger.log(Level.INFO, "Could not update user upon disconnection", e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.UserHandler#isPlaying()
	 */
	@Override
	public boolean isPlaying() {
		return playing;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.UserHandler#register(java.lang.String, java.lang.String)
	 */
	private boolean register(String username, String password) {
		try {
			User loadedUser = DatabaseManager.addUser(username, password);
			if (loadedUser != null) {
				this.user = loadedUser;
				return true;
			}
		} catch (SQLException e) {
			logger.log(Level.WARNING, "User tried to register with an existing username", e);

		}
		return false;
	}

	/**
	 * 
	 * @param username
	 * @param password
	 * @return true if the login is successful, false otherwise
	 */
	private boolean login(String username, String password) {
		try {
			User loadedUser = DatabaseManager.login(username, password);
			if (loadedUser != null) {
				this.user = loadedUser;
				return true;
			}

		} catch (SQLException e) {
			logger.log(Level.WARNING, "User " + username + " login attempt failed", e);

		}
		return false;
	}

	/**
	 * @see server.UserHandler#getLobby()
	 */
	@Override
	public LobbyImpl getLobby() {
		return lobbyImpl;
	}

	/**
	 * @see server.UserHandler#setLobby(LobbyImpl)
	 */
	@Override
	public void setLobby(LobbyImpl lobbyImpl) {
		this.lobbyImpl = lobbyImpl;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.UserHandler#getUsername()
	 */
	@Override
	public String getUsername() {
		return user.getUsername();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.UserHandler#gameStarted(core.Game, client.GameState,
	 * core.PlayerImpl)
	 */
	@Override
	public void setPlayer(PlayerImpl player) throws IOException {
		lobbyImpl = null;
		this.player = player;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.UserHandler#update(client.GameState)
	 */
	@Override
	public void update(HashMap<String, String> stateUpdate) throws IOException {
		objectOutputStream.reset();
		objectOutputStream.writeObject(stateUpdate);
		objectOutputStream.flush();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.UserHandler#keepAlive()
	 */
	@Override
	public void keepAlive() throws NetworkException {
		try {
			objectOutputStream.reset();
			objectOutputStream.writeObject(true);
			objectOutputStream.flush();
		} catch (IOException e) {
			throw new NetworkException(e);
		}

	}

}
