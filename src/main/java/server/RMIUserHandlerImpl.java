package server;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.server.Unreferenced;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import client.NetworkException;
import client.RemotePlayer;
import core.PlayerImpl;

/**
 * @author B
 *
 */
public class RMIUserHandlerImpl extends UnicastRemoteObject
		implements client.RMIUserHandler, UserHandler, Unreferenced {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5562660410655760962L;

	// Logging functionality
	static final Logger logger = Logger.getLogger(RMIUserHandlerImpl.class.getName());

	private final User user;
	private transient State state;
	private final long connectionTime = new Date().getTime();
	private boolean playing;
	private transient LobbyImpl lobbyImpl;

	/**
	 * 
	 * 
	 * @param user
	 *            the user object that contains all the information
	 * @throws RemoteException
	 *             in case the remote object can't be created
	 */
	public RMIUserHandlerImpl(User user) throws RemoteException {
		this.user = user;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.UserHandler#cleanUp()
	 */
	@Override
	public void cleanUp() {
		GameServer.removeUser(this);
		if (this.lobbyImpl != null)
			lobbyImpl.removeUser(this);
		try {
			DatabaseManager.updateUser(user);
		} catch (SQLException e) {
			logger.log(Level.INFO, "Could not update user upon disconnection", e);
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see server.UserHandler#isPlaying()
	 */
	@Override
	public boolean isPlaying() {
		return playing;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.UserHandler#setLobby(server.Lobby)
	 */
	@Override
	public void setLobby(LobbyImpl lobbyImpl) throws IOException {
		this.lobbyImpl = lobbyImpl;
		state.setLobby(lobbyImpl);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.UserHandler#getLobby()
	 */
	@Override
	public LobbyImpl getLobby() {
		return lobbyImpl;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.UserHandler#getUsername()
	 */
	@Override
	public String getUsername() {
		return user.getUsername();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.RMIUserHandler#setInitialState(server.State)
	 */
	@Override
	public void setInitialState(State state) throws RemoteException {
		this.state = state;
		GameServer.addUser(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.rmi.server.Unreferenced#unreferenced()
	 */
	@Override
	public void unreferenced() {
		logger.severe("UNREFERENCED CALLED!!");
		this.cleanUp();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.UserHandler#gameStarted(core.Game, core.Player)
	 */
	@Override
	public void setPlayer(PlayerImpl player) throws IOException {
		logger.info("userHandler.gameStarted entered");
		state.setPlayer((RemotePlayer) player);
		logger.info("state.gameStarted exited");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.UserHandler#update(client.GameState)
	 */
	@Override
	public void update(HashMap<String, String> stateUpdate) throws IOException {
		state.update(stateUpdate);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.RMIUserHandler#quit()
	 */
	@Override
	public void quit() throws RemoteException {
		cleanUp();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.UserHandler#keepAlive()
	 */
	@Override
	public void keepAlive() throws NetworkException {

		try {
			state.alive();
		} catch (RemoteException e) {
			throw new NetworkException(e);
		}

	}

}
