package server;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseManager {

	// URL format is
	// jdbc:derby:<local directory to save data> to save to disk ----->
	// server/db/users
	// jdbc:derby:memory:name to save to memory
	// append ;create=true if you want Derby to create the db in case it does
	// not exist
	private static final String DB_URL = "jdbc:derby:server/db/game";

	/**
	 * Logging
	 */
	private static final Logger logger = Logger.getLogger(DatabaseManager.class.getName());

	/**
	 * Empty private constructor to hide default public one
	 */
	private DatabaseManager() {
		// Empty private constructor to hide default public one
	}

	/**
	 * Creates and returns a connection to the local user database
	 * 
	 * 
	 * @return Connection a new connection to the local Derby Database
	 * @throws SQLException
	 *             in case the method fails to connect to the db
	 */
	private static Connection getConnection() throws SQLException {

		try {
			return DriverManager.getConnection(DatabaseManager.DB_URL);

		} catch (SQLException e) {
			logger.log(Level.INFO, "Database connection attempt returned ", e);
			logger.severe("Could not connect to local database \nAttempting to build a new one...");
			Connection connection = DriverManager.getConnection(DB_URL + ";create=true");
			Statement statement = connection.createStatement();
			// Creates a new users table to manage login and User information
			statement.executeUpdate(
					"CREATE TABLE USERS(username varchar(20) not null primary key, password varchar(20), savedUser blob)");
			logger.severe("New database created!");
			statement.close();
			return connection;
		}

	}

	/**
	 * Creates a new User, adds it to the database and returns it
	 * 
	 * @return User a copy of the created user
	 * @param username
	 *            new user's username
	 * @param password
	 *            new user's password
	 * @throws SQLException
	 *             in case the database connection fails
	 */
	public static User addUser(String username, String password) throws SQLException {

		// TODO Hash password
		User user = new User(username);
		Connection connection = getConnection();
		PreparedStatement preparedStatement = connection
				.prepareStatement("INSERT into USERS (username, password, savedUser) values(?, ?, ?)");
		preparedStatement.setString(1, username);
		preparedStatement.setString(2, password);

		byte[] serializedUser = serialize(user);

		preparedStatement.setBytes(3, serializedUser);
		preparedStatement.executeUpdate();
		preparedStatement.close();
		connection.close();
		return user;
	}

	/**
	 * 
	 * @param username
	 *            the user's username for logging in
	 * @param password
	 *            the user's password for logging in
	 * @return true if the user exists, else otherwise
	 * @throws SQLException
	 *             in case the connection to the db fails
	 */
	public static User login(String username, String password) throws SQLException {

		// TODO Hash password
		Connection connection = getConnection();
		PreparedStatement preparedStatement = connection
				.prepareStatement("SELECT savedUser FROM USERS WHERE username = ? and password = ?");
		preparedStatement.setString(1, username);
		preparedStatement.setString(2, password);
		ResultSet resultSet = preparedStatement.executeQuery();
		if (resultSet.next()) {
			User deSerializedUser = (User) deSerialize(resultSet.getBytes("savedUser"));

			resultSet.close();
			preparedStatement.close();
			connection.close();
			return deSerializedUser;
		}
		preparedStatement.close();
		connection.close();
		return null;

	}

	/**
	 * Updates the user object that's already present on the database
	 * 
	 * @param user
	 *            the user that needs its object backup updated
	 * @throws SQLException
	 *             in case the db can't find the user to update
	 */
	public static void updateUser(User user) throws SQLException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = connection
				.prepareStatement("UPDATE USERS SET savedUser = ? WHERE username = ?");
		preparedStatement.setBytes(1, serialize(user));
		preparedStatement.setString(2, user.getUsername());

		preparedStatement.close();
		connection.close();
	}

	/**
	 * removes the user with the specified username from the database
	 * 
	 * 
	 * @param username
	 *            the name of the user to be removed from the database
	 * @throws SQLException
	 *             in case the db can't find the user to delete
	 */
	public static void removeUser(String username) throws SQLException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM users WHERE username = ?");
		preparedStatement.setString(1, username);
		preparedStatement.executeUpdate();

		preparedStatement.close();
		connection.close();
	}

	/**
	 * Allows new configurations to be added to the database
	 * 
	 * @param configuration
	 *            the properties object of the configuration
	 * 
	 * @param name
	 *            with which it will be identified on the databse
	 * @throws SQLException in case the database can't be reached
	 */
	public static void saveConfiguration(Properties configuration, String name) throws SQLException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = connection
				.prepareStatement("INSERT into CONFIGS (name, configuration) values (?, ?)");
		preparedStatement.setString(1, name);
		preparedStatement.setBytes(2, serialize(configuration));
		preparedStatement.executeUpdate();

		preparedStatement.close();
		connection.close();
	}

	/**
	 * Returns the configuration with the specified name from the databse
	 * 
	 * @param name
	 *            the name of the configuration to load
	 * @return the configuration loaded from the database
	 * @throws SQLException in case the database can't be reached
	 */
	public static Properties loadConfiguration(String name) throws SQLException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = connection
				.prepareStatement("SELECT configuration FROM CONFIGS where name = ?");
		preparedStatement.setString(1, name);
		ResultSet resultSet = preparedStatement.executeQuery();
		Properties loadedConfiguration = (Properties) resultSet.getObject("configuration");

		preparedStatement.close();
		connection.close();
		return loadedConfiguration;

	}

	/**
	 * Serializes the specified Serializable object
	 * 
	 * 
	 * @param object
	 *            the object to be serialized
	 * @return the serialized object, empty in case of errors
	 */
	private static byte[] serialize(Serializable object) {
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(bo);
			oos.writeObject(object);
			oos.flush();
			oos.close();
			bo.close();
		} catch (IOException e) {
			logger.log(Level.WARNING, "User serialization failed!", e);
			throw new IllegalStateException(e);
		}

		return bo.toByteArray();
	}

	/**
	 * Deserializes an object, used for User retrieval from the database
	 * 
	 * 
	 * @param serializedObject
	 *            the serialized object as byte array
	 * 
	 * @return the deSerialized object
	 */
	private static Object deSerialize(byte[] serializedObject) {
		Object deSerializedObject;

		ByteArrayInputStream bi = new ByteArrayInputStream(serializedObject);
		ObjectInputStream ois;
		try {
			ois = new ObjectInputStream(bi);
			deSerializedObject = ois.readObject();
		} catch (IOException | ClassNotFoundException e) {
			logger.log(Level.WARNING, "Object deserialization failed!", e);
			throw new IllegalStateException(e);
		}

		return deSerializedObject;
	}

}
