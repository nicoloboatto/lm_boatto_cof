package server;

import java.io.FileInputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import client.RemoteLobby;
import core.Game;
import core.PlayerImpl;

public class LobbyImpl extends UnicastRemoteObject implements RemoteLobby {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3087985030923543338L;

	// Logging functionality
	private static final Logger logger = Logger.getLogger(LobbyImpl.class.getName());

	private transient UserHandler admin;
	private transient List<UserHandler> userHandlers = new ArrayList<UserHandler>(4);
	private Properties configuration;

	/**
	 * 
	 * 
	 * @param admin
	 *            the user to be set as admin
	 * @throws RemoteException
	 *             in case creating the lobby is not succesful
	 */
	public LobbyImpl(UserHandler admin) throws RemoteException {
		this.userHandlers.add(admin);
		this.admin = admin;
	}

	/**
	 * 
	 * @throws RemoteException
	 */
	public LobbyImpl() throws RemoteException {
		// Empty constructor, useful for both the public lobby and testing
	}

	/**
	 * 
	 * Adds the user to the lobby, cheching if it already is in the lobby and
	 * removing it in that case
	 * 
	 * @param userHandler
	 *            the userHandler to be added to the lobby
	 */
	public synchronized void addUser(UserHandler userHandler) {

		// if it's the first user it will be set as admin
		if (userHandlers.isEmpty())
			admin = userHandler;
		try {
			for (Iterator iterator = userHandlers.iterator(); iterator.hasNext();) {
				UserHandler user = (UserHandler) iterator.next();
				if (user.getUsername() == userHandler.getUsername()) {
					userHandlers.remove(user);
					user.cleanUp();
				}
			}

			userHandler.setLobby(this);

		} catch (IOException e) {
			logger.log(Level.WARNING, "Could not set lobby", e);
			userHandler.cleanUp();
		}

		userHandlers.add(userHandler);
		// Update users that a new user has joined
		for (UserHandler updatee : userHandlers) {
			try {
				updatee.update(this.getState());
			} catch (IOException e) {
				logger.log(Level.WARNING, "Could not update user", e);
				updatee.cleanUp();
			}
		}

	}

	/**
	 * Removes user from the lobby, if the is the admin, it elects a new admin
	 * 
	 * @param userHandler
	 *            the user to be removed
	 */
	public synchronized void removeUser(UserHandler userHandler) {
		userHandlers.remove(userHandler);

		if (admin == userHandler) {
			if (!userHandlers.isEmpty()) {
				admin = userHandlers.get(0);
			} else {
				admin = null;
			}

		}

		for (UserHandler updatee : userHandlers) {
			try {
				updatee.update(this.getState());
			} catch (IOException e) {
				logger.log(Level.INFO, "Could not update user", e);
				updatee.cleanUp();
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.Lobby#setConfiguration(java.lang.String)
	 */
	@Override
	public boolean setConfiguration(String configurationName) {
		try (FileInputStream configurationFile = new FileInputStream(
				"server/GameConfigurations/" + configurationName + ".properties")) {
			this.configuration = new Properties();
			configuration.load(configurationFile);
			return true;
		} catch (IOException e) {
			logger.log(Level.INFO, "Configuration does not exist", e);
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.Lobby#startGame()
	 */
	@Override
	public synchronized void startGame() {
		if (userHandlers.size() < 2)
			return;
		if (configuration == null)
			setConfiguration("default1");
		logger.info("Configuration Set");
		Game game = new Game(userHandlers.toArray(new UserHandler[userHandlers.size()]), configuration);
		new Thread(game).start();
		logger.info("Game created");
		clear();
	}

	/**
	 * 
	 * 
	 * @return the number of users in the lobby
	 */
	public int size() {
		return userHandlers.size();
	}

	/**
	 * @see client.RemoteLobby#getUsernames()
	 */
	@Override
	public List<String> getUsernames() {

		ArrayList<String> usernames = new ArrayList<>();
		for (int i = 0; i < userHandlers.size(); i++) {
			usernames.add(userHandlers.get(i).getUsername());
		}
		return usernames;
	}

	/**
	 * Clears the lobby from all the users, removes the admin and sets the
	 * configuration back to null
	 */
	private void clear() {
		userHandlers.clear();
		admin = null;
		configuration = null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.Lobby#isAdmin(java.lang.String)
	 */
	@Override
	public boolean isAdmin(String username) throws RemoteException {
		if (admin.getUsername().equals(username))
			return true;
		return false;
	}

	/**
	 * Returns a hashmap that represents the state of the lobby, with
	 * informations such as the number of users, their usernames and the name of
	 * the admin
	 * 
	 * @return the state of the lobby
	 */
	public HashMap<String, String> getState() {
		HashMap<String, String> state = new HashMap<>();
		state.put("state", "LOBBY");
		state.put("admin", admin.getUsername());
		state.put("numberOfPlayers", "" + userHandlers.size());
		state.put("timeLimit", Integer.toString(GameServer.getTimeLimit() / 1000));
		for (int i = 0; i < userHandlers.size(); i++) {
			state.put("player" + i + "Username", userHandlers.get(i).getUsername());
		}
		return state;
	}

}
