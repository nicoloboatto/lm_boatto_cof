package server;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

public class User implements Serializable {
	// Class is serializable in order to be able to save users to the database

	// Logging functionality
	private static final Logger logger = Logger.getLogger(User.class.getName());

	// This is used to recognize different versions of the object.
	private static final long serialVersionUID = 2377746223877038615L;

	private String username;
	private long playTime = 0;
	private int playedMatches = 0;
	private int wonMatches = 0;
	private ArrayList<Properties> configurations;

	/**
	 * 
	 * Name of the user
	 * 
	 * @param username
	 *            this will be set as the player username
	 */
	public User(String username) {
		this.username = username;
	}

	/**
	 * Adds time to the total play time
	 * 
	 * @param sessionTime
	 *            the time passed since login in this session
	 */
	public void addPlayTime(long sessionTime) {
		playTime += sessionTime;
	}

	public long getPlaytime() {
		return playTime;
	}

	/**
	 *
	 * 
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * 
	 * @return the number of played matches
	 */
	public int getPlayedMatches() {
		return playedMatches;
	}

	/**
	 * 
	 * @return the number of won matches
	 */
	public int getWonMatches() {
		return wonMatches;
	}

	/**
	 * 
	 * @return the user's custom game configurations
	 */
	public List<Properties> getConfigurations() {
		return this.configurations;
	}
}
