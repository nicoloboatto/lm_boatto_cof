package server;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class CustomConsoleFormatter extends Formatter {

	/**
	 * 
	 * @param record the log that needs to be formatted
	 * 
	 * @return the formatted string 
	 */
	@Override
	public String format(LogRecord record) {
		return record.getMessage() + "\n";
	}

}
