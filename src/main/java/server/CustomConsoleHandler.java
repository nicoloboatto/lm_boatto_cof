package server;

import java.util.logging.ConsoleHandler;

/**
 * @author B
 *
 */
public class CustomConsoleHandler extends ConsoleHandler {

	/**
	 * Changes the console output from System.err to System.out so that some
	 * logs can also serve as CLI output
	 */
	public CustomConsoleHandler() {
		super();
		this.setOutputStream(System.out);
	}

}
