package server;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Properties;

import client.RemoteLobby;
import client.RemotePlayer;
import core.PlayerImpl;

public interface State extends Remote {


	/**
	 * Adds a username to the client state
	 * 
	 * @param username
	 *            the name of the user who joined
	 * @throws RemoteException
	 *             in case there are errors in the remote invocation
	 */
	public void addUser(String username) throws RemoteException;

	/**
	 * Removes a username from the client state
	 * 
	 * @param username
	 *            the name of the user who left
	 * @throws RemoteException
	 *             in case there are errors in the remote invocation
	 */
	public void removeUser(String username) throws RemoteException;

	/**
	 * Sets the lobby so that rmi client can access it
	 * 
	 * 
	 * @param remoteLobby
	 *            the lobbby to be set
	 * @throws RemoteException
	 *             in case there are errors in the remote invocation
	 */
	public void setLobby(RemoteLobby remoteLobby) throws RemoteException;

	/**
	 * Sets the player so that the remote client can access its methods and
	 * informations
	 * 
	 * @param player
	 *            the player to be set
	 * @throws RemoteException
	 *             in case there are errors in the remote invocation
	 */
	public void setPlayer(RemotePlayer player) throws RemoteException;

	/**
	 * @param stateUpdate
	 *            the update to be put in the client gamestate
	 * 
	 * @throws RemoteException
	 *             if there are errors in the remote invocation
	 * 
	 */
	public void update(HashMap<String, String> stateUpdate) throws RemoteException;

	/**
	 * Method that checks if the client is still connected
	 * 
	 * @throws RemoteException
	 *             in case the user has disconnected
	 */
	public void alive() throws RemoteException;

}
