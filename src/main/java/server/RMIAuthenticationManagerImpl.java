package server;

import java.net.MalformedURLException;
import java.rmi.AccessException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import client.RMIUserHandler;

/**
 * @author B
 *
 */
public class RMIAuthenticationManagerImpl extends UnicastRemoteObject implements client.RMIAuthenticationManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3033393160725089742L;
	private static final Logger logger = Logger.getLogger(RMIAuthenticationManagerImpl.class.getName());

	/**
	 * 
	 * 
	 * @param port
	 *            the number of the port on which to start the authentication
	 *            manager
	 * @throws RemoteException
	 *             in case the object can't be registered on the registry
	 */
	public RMIAuthenticationManagerImpl(int port) throws RemoteException {
		super();

		System.setProperty("java.rmi.server.codebase", "file:target/classes/server");
		System.setProperty("java.security.policy", "file:server/security.policy");

		try {
			LocateRegistry.createRegistry(port);
			Naming.rebind("AuthenticationManager", this);
		} catch (MalformedURLException | RemoteException e) {
			logger.log(Level.SEVERE, "Could not bind Authentication Manager", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.RMIAuthenticationManager#login(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public RMIUserHandler login(String username, String password) throws RemoteException {
		try {
			User user = DatabaseManager.login(username, password);
			if (user == null)
				return null;
			UserHandler userHandler = new server.RMIUserHandlerImpl(user);
			return (RMIUserHandler) userHandler;

		} catch (SQLException | RemoteException e) {
			logger.log(Level.INFO, "User login failed", e);
			return null;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.RMIAuthenticationManager#register(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public RMIUserHandler register(String username, String password) throws RemoteException {
		try {
			UserHandler userHandler = new server.RMIUserHandlerImpl(DatabaseManager.addUser(username, password));
			return (RMIUserHandler) userHandler;

		} catch (SQLException | RemoteException e) {
			logger.log(Level.INFO, "User registration failed", e);
			return null;
		}
	}

}
