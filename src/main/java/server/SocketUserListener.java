package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SocketUserListener implements Runnable {

	private static final Logger logger = Logger.getLogger(SocketUserListener.class.getName());
	private boolean running = true;
	private ExecutorService userHandlerExecutor = Executors.newCachedThreadPool();
	private ServerSocket serverSocket;

	/**
	 * 
	 * @param port
	 *            the port number on which the server will be listening
	 * @throws IOException
	 *             In case starting the listener on the port fails (port is
	 *             probably occupied)
	 */
	public SocketUserListener(int port) throws IOException {
		serverSocket = new ServerSocket(port);
		logger.severe("Socket Listener Started on port " + port + "!");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {

		while (running) {

			try {

				Socket socket = serverSocket.accept();
				userHandlerExecutor.submit(new SocketUserHandler(socket));

			} catch (IOException e) {
				logger.log(Level.WARNING, "User connection failed", e);
			}
		}
		try {
			serverSocket.close();
		} catch (IOException e) {
			logger.log(Level.SEVERE, "SocketListener stop returned an error", e);
		}
		userHandlerExecutor.shutdown();
		return;

	}

	/**
	 * Stops the listener's execution
	 */
	public void stop() {
		logger.severe("Stopping Socket Listener");
		running = false;
	}

}
