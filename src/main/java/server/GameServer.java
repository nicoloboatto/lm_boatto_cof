package server;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import client.NetworkException;
import client.RMIAuthenticationManager;
import core.Game;

public class GameServer {

	/*
	 * Logging functionality for exceptions and some user prints
	 */
	private static final Logger logger = Logger.getLogger(GameServer.class.getName());

	private static final int DEFAULT_SOCKET_PORT = 6666;
	private static final int DEFAULT_RMI_PORT = 1099;
	private static final int DEFAULT_TIME_LIMIT = 200000;
	/**
	 * User can specify custom time limit or it will be set to the default one
	 */
	private static int timeLimit;

	/**
	 * Listens for new user connections
	 */
	private static SocketUserListener socketUserListener;

	private static RMIAuthenticationManager authenticationManager;

	/**
	 * Condition that keeps server running, changed to false when user quits
	 */
	private static boolean running = true;

	/**
	 * Public lobby where users are put after connection
	 */
	private static LobbyImpl publicLobby;

	/*
	 * List of users on the server, used mainly by keepAlive function
	 */
	private static ArrayList<UserHandler> users = new ArrayList<>(5);

	/**
	 * List of games running on the server
	 */
	private static List<Game> games = new ArrayList<>();

	private static Scanner scanner = new Scanner(System.in);

	/**
	 * Private constructor to hide the default public one
	 * 
	 * 
	 */
	private GameServer() {

	}

	public static void main(String[] args) {

		if (args.length > 0) {
			// if arguments are present, the first should be the port number.
			try {

				GameServer.setup(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]));

			} catch (NumberFormatException e) {
				// In case the user enters an invalid parameter
				logger.log(Level.SEVERE, "Port number must be an integer 1-63737", e);

				return;
			} catch (IOException e) {
				// In case the SocketUserListener fails to start
				logger.log(Level.SEVERE,
						"Socket listener failed to start, make sure port " + args[0] + "is not already in use");
				logger.log(Level.WARNING, "", e);

				return;
			}

		} else {
			try {
				GameServer.setup(DEFAULT_SOCKET_PORT, DEFAULT_RMI_PORT, DEFAULT_TIME_LIMIT);
			} catch (IOException e) {
				logger.log(Level.SEVERE,
						"Socket listener start failed due to a connection error, make sure the port is not already occupied");
				logger.log(Level.WARNING, "", e);
			}
		}
		logger.severe("Type quit and press enter to quit.");
		while (!"quit".equals(scanner.nextLine()))
			;

		shutdown();

		return;
	}

	/**
	 * Starts the gameserver on the specified ports and with the specified turn
	 * time limit
	 *
	 * @param socketPort
	 *            the port on which to listen for socket connections
	 * @param rmiPort
	 *            the port on which to listen for rmi connections
	 * @param timeLimit
	 *            the maximum time a player has to take a turn (in milliseconds)
	 * @throws IOException
	 *             in case starting the server causes errors
	 */
	public static void setup(int socketPort, int rmiPort, int timeLimit) throws IOException {

		running = true;
		// Sets global logging level to WARNING, change for finer information
		logger.setLevel(Level.WARNING);
		logger.info("Setting up logging to file and console");

		GameServer.timeLimit = timeLimit;

		try {
			// Loads the logger configuration file from the system
			FileInputStream serverConfig = new FileInputStream("server/server.properties");
			LogManager.getLogManager().readConfiguration(serverConfig);
			publicLobby = new LobbyImpl();

		} catch (IOException e) {

			logger.log(Level.WARNING,
					"Could not load configuration file\n Logging not configured, will output to console", e);

		}
		logger.severe("Welcome to the Council of Four Server!");

		// Creates a new SocketUserListener on a separate Thread
		socketUserListener = new SocketUserListener(socketPort);
		new Thread(socketUserListener).start();

		// Creates a KeepAlive Thread to check if players are online and run
		// cleanup if they are not
		new Thread(new KeepAlive()).start();

		// Starts the RMIAuthenticationManager
		authenticationManager = new RMIAuthenticationManagerImpl(rmiPort);

	}

	/**
	 * Sets up GameServer on specified Socket Port, default RMI port(1099) and
	 * with a default turn time limit which is 200 seconds
	 * 
	 * @param socketPort
	 *            the port on which to listen for socket connections
	 * 
	 * @throws IOException
	 *             in case starting the server causes errors
	 */
	public static void setup(int socketPort) throws IOException {
		setup(socketPort, DEFAULT_RMI_PORT, DEFAULT_TIME_LIMIT);

	}

	/**
	 * Shuts down the socket listener and quits
	 */
	public static void shutdown() {
		running = false;
		authenticationManager = null;
		socketUserListener.stop();
	}

	/**
	 * 
	 * Adds the specified user to the public lobby and to the list of users, if
	 * there are two or more players in the lobby a 20 second timer is started,
	 * after which the game is started
	 * 
	 * @param userHandler
	 *            the userHandler to add
	 */
	public static synchronized void addUser(UserHandler userHandler) {
		users.add(userHandler);
		publicLobby.addUser(userHandler);
		if (publicLobby.size() == 2) {
			new Timer().schedule(new TimedStart(publicLobby), 20000);
		} else if (publicLobby.size() == 4) {
			publicLobby.startGame();
		}

	}

	/**
	 * Removes the speficied user from the list
	 * 
	 * @param userHandler
	 *            the userHandler to be removed
	 */
	public static synchronized void removeUser(UserHandler userHandler) {
		users.remove(userHandler);
	}

	/**
	 * 
	 * @param game
	 *            adds a game to the list, synchronized to guarantee sequential
	 *            access
	 */
	public static synchronized void addGame(Game game) {
		games.add(game);
	}

	/**
	 * 
	 * Removes the speficied game from the lsit of games running on the server
	 * 
	 * @param game
	 *            the game to be removed
	 */
	public static void removeGame(Game game) {
		games.remove(game);
	}

	/**
	 * @return the timeLimit set during server setup
	 */
	public static int getTimeLimit() {
		return timeLimit;
	}

	/**
	 * @return true if the server is running, false otherwise
	 */
	public static boolean isRunning() {
		return running;
	}

	/**
	 * 
	 * 
	 * @return the list of UserHandlers on the server
	 */
	public static List<UserHandler> getUsers() {

		return new ArrayList<>(users);
	}

}

class KeepAlive implements Runnable {

	ArrayList<UserHandler> users = new ArrayList<>();
	private static final Logger logger = Logger.getLogger(KeepAlive.class.getName());

	/**
	 * Runs through all users on the server and check if they are still
	 * connected, used to clean up faster after disconnection
	 */
	@Override
	public void run() {
		while (GameServer.isRunning()) {
			Iterator<UserHandler> userHandlers = GameServer.getUsers().iterator();

			while (userHandlers.hasNext()) {
				UserHandler user = userHandlers.next();
				try {

					user.keepAlive();
				} catch (NetworkException e) {
					user.cleanUp();
					logger.log(Level.WARNING, "User disconnected", e);
				}
			}

			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				logger.log(Level.WARNING, "Interrupted", e);
				Thread.currentThread().interrupt();
			}

		}

	}

}

class TimedStart extends TimerTask {

	private LobbyImpl lobbyImpl;

	/**
	 * 
	 * @param lobbyImpl
	 *            the lobby on which the game should be started
	 */
	public TimedStart(LobbyImpl lobbyImpl) {
		this.lobbyImpl = lobbyImpl;
	}

	/**
	 * Checks if the number of players in the lobby is greater than 2 and if it
	 * is starts the game
	 */
	@Override
	public void run() {
		if (lobbyImpl.size() >= 2)
			lobbyImpl.startGame();

	}

}
