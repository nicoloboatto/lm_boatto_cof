package client;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * @author B
 *
 */
public interface RemoteLobby extends Remote {

	/**
	 * Sets the game configuration for the game that is about to start
	 * 
	 * @param configurationName
	 *            name of the configuration to load
	 * @return true if the configuration was found, false otherwise
	 * @throws RemoteException
	 *             if there were errors in the underlying network implementation
	 */
	boolean setConfiguration(String configurationName) throws RemoteException;

	/**
	 * Starts the game, but only if there is more than one player in the lobby
	 * 
	 * @throws RemoteException
	 *             if there were errors in the underlying network implementation
	 */
	void startGame() throws RemoteException;

	/**
	 * Checks if the player is the admin of the lobby
	 * 
	 * @param username
	 *            the username of the player on which to check
	 * @return true if the player with the specified username is the admin
	 * @throws RemoteException
	 *             if there were errors in the underlying network implementation
	 */
	boolean isAdmin(String username) throws RemoteException;

	/**
	 * Gets the array of the usernamef of other players in the lobby
	 * 
	 * @return the array of the usernames in the lobby
	 * @throws RemoteException
	 *             if there were errors in the underlying network implementation
	 */
	List<String> getUsernames() throws RemoteException;

}