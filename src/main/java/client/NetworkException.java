package client;

public class NetworkException extends Exception {

	/**
	 * @param cause
	 *            the underlying cause of the network exception
	 */
	public NetworkException(Throwable cause) {
		super(cause);
	}

}
