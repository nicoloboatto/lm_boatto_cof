package client;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author B
 *
 */
public interface RemotePlayer extends Remote {

	/**
	 * First main action from the game
	 * 
	 * @param nameOfTheRegion
	 *            name of the region which contains the permit card
	 * @param position
	 *            position of the permit card in the region, can be 0 or 1
	 * @param politicalCard
	 *            character array of the political cards to be used for
	 *            acquisition
	 * @throws RemoteException
	 *             in case there are errors in the underlying remote
	 *             implementation
	 */
	void buyPermitCard(String nameOfTheRegion, int position, char[] politicalCard) throws RemoteException;

	/**
	 * Second main action from the game
	 * 
	 * @param politicalCard
	 *            the character array of the colors of political cards to be
	 *            used
	 * @param city
	 *            the initial of the name of the city in which to move the king
	 * 
	 * @throws RemoteException
	 *             in case there are errors in the underlying remote
	 *             implementation
	 */
	void buyKing(char[] politicalCard, char city) throws RemoteException;

	/**
	 * Third main action from the game
	 * 
	 * @param color
	 *            the counselor to be slid onto the council
	 * @param nameOfTheRegion
	 *            name of the region that contains the council
	 * @throws RemoteException
	 *             in case there are errors in the underlying remote
	 *             implementation
	 */
	void slideCouncil(char color, String nameOfTheRegion) throws RemoteException;

	/**
	 * Fourth main action
	 * 
	 * @param numberOfThePermitCard
	 *            the number of the permit card from the player's list
	 * @param charOfTheCity
	 *            the initial of the city in which to build the emporium
	 * @throws RemoteException
	 *             in case there are errors in the underlying remote
	 *             implementation
	 */
	void putEmporiumInACity(int numberOfThePermitCard, char charOfTheCity) throws RemoteException;

	/**
	 * 
	 * Buys a helper in the game
	 * 
	 * @throws RemoteException
	 *             in case there are errors in the underlying remote
	 *             implementation
	 * 
	 */
	void buyHelper() throws RemoteException;

	/**
	 * 
	 * @param nameOfTheRegion
	 *            name of the region which will have its permit cards refreshed
	 * @throws RemoteException
	 *             in case there are errors in the underlying remote
	 *             implementation
	 */
	void refreshPermitCards(String nameOfTheRegion) throws RemoteException;

	/**
	 * 
	 * @param nameOfTheRegionOfTheCouncil
	 *            name of the region which contains the council to which the
	 *            member will be added
	 * @param color
	 *            the color of the counselor to be added to the council
	 * @throws RemoteException
	 *             in case there are errors in the underlying remote
	 *             implementation
	 */
	void addAMemberOfTheCouncil(String nameOfTheRegionOfTheCouncil, char color) throws RemoteException;

	/**
	 * 
	 * @throws RemoteException
	 *             in case there are errors in the underlying remote
	 *             implementation
	 */
	public void buyMainAction() throws RemoteException;

	/**
	 * 
	 * @throws RemoteException
	 *             in case there are errors in the underlying remote
	 *             implementation
	 */
	public void skipAction() throws RemoteException;

	/**
	 * 
	 * @param city
	 *            the initial of the city from which to get a bonus
	 * @throws RemoteException
	 *             in case there are errors in the underlying remote
	 *             implementation
	 */
	void getBonusOfAnEmporium(char city) throws RemoteException;

	/**
	 * 
	 * @param position
	 *            the position of the permit card of which the bonus will be
	 *            reactivated
	 * @throws RemoteException
	 *             in case there are errors in the underlying remote
	 *             implementation
	 */
	void getPermitCardBonus(int position) throws RemoteException;

	/**
	 * 
	 * @param region
	 *            name of the region that contains the permit card
	 * @param permitCard
	 *            position of the permit card in the region, can be either 0 or
	 *            1
	 * @throws RemoteException
	 *             in case there are errors in the underlying remote
	 *             implementation
	 */
	void pickPermitCard(String region, int permitCard) throws RemoteException;

	/**
	 * 
	 * @param politicalCard
	 *            character array of the political cards to be sold
	 * @param permitCard
	 *            position of the permit cards to be sold
	 * @param assistants
	 *            number of assistants to be sold
	 * @param price
	 *            the price to be paid to buy this offer
	 * @throws RemoteException
	 *             in case there are errors in the underlying remote
	 *             implementation
	 */
	void putOffer(char[] politicalCard, int permitCard, int assistants, int price) throws RemoteException;

	/**
	 * 
	 * @param offer
	 *            the number of the offer from the market
	 * @throws RemoteException
	 *             in case there are errors in the underlying remote
	 *             implementation
	 */
	void buyOffer(int offer) throws RemoteException;

}
