package client;

import java.net.Socket;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.IOException;

public class SocketClient implements Client {

	private Socket socket;
	private ObjectInputStream objectInputStream;
	private ObjectOutputStream objectOutputStream;

	private SocketUpdateListener updateListener;

	private String username;

	private Lock lock = new ReentrantLock();
	private Condition updated = lock.newCondition();

	private HashMap<String, String> gameState = new HashMap<>();
	private boolean hasUpdates = false;
	private boolean playing = false;

	/**
	 * 
	 * @param ip
	 *            the ip address where the server is listening
	 * @param port
	 *            the port on which the server is listening
	 * @throws IOException
	 *             in case there are errors in the socket
	 */
	public SocketClient(String ip, int port) throws IOException {
		socket = new Socket(ip, port);
		objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
		objectOutputStream.flush();
		objectInputStream = new ObjectInputStream(socket.getInputStream());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#quit()
	 */
	@Override
	public void quit() {
		updateListener.stop();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#login(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean login(String username, String password) throws NetworkException {

		try {
			objectOutputStream.reset();
			objectOutputStream.writeObject("login");
			objectOutputStream.writeObject(username);
			objectOutputStream.writeObject(password);
			objectOutputStream.flush();
			if (objectInputStream.readBoolean()) {
				this.username = username;
				new Thread(new SocketUpdateListener(this, objectInputStream)).start();
				return true;
			}
			return false;
		} catch (IOException e) {
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#register(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean register(String username, String password) throws NetworkException {

		try {
			objectOutputStream.reset();
			objectOutputStream.writeObject("register");
			objectOutputStream.writeObject(username);
			objectOutputStream.writeObject(password);
			objectOutputStream.flush();
			if (objectInputStream.readBoolean()) {
				this.username = username;
				new Thread(new SocketUpdateListener(this, objectInputStream)).start();
				return true;
			}
			return false;
		} catch (IOException e) {
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#isPlaying()
	 */
	@Override
	public boolean isPlaying() {
		return playing;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#getUsername()
	 */
	@Override
	public String getUsername() {
		return username;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#getUsers()
	 */
	@Override
	public List<String> getUserNames() {

		List<String> users = new ArrayList<>();

		if (gameState.containsKey("numberOfPlayers")) {
			int size = Integer.parseInt(gameState.get("numberOfPlayers"));
			for (int i = 0; i < size; i++) {
				if (!username.equals(gameState.get("player" + i + "Username")))
					users.add(gameState.get("player" + i + "Username"));
			}
			hasUpdates = false;
		}

		return users;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#setConfiguration(java.lang.String)
	 */
	@Override
	public boolean setConfiguration(String configurationName) throws NetworkException {
		try {
			objectOutputStream.reset();
			objectOutputStream.writeObject(Commands.SET_CONFIGURATION);
			objectOutputStream.writeObject(configurationName);
			objectOutputStream.flush();
			return true;
		} catch (IOException e) {
			throw new NetworkException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#isAdmin()
	 */
	@Override
	public boolean isAdmin() throws NetworkException {
		return username.equals(gameState.get("admin"));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#startGame()
	 */
	@Override
	public void startGame() throws NetworkException {
		try {
			objectOutputStream.reset();
			objectOutputStream.writeObject(Commands.START_GAME);
			objectOutputStream.flush();
		} catch (IOException e) {
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#getState()
	 */
	@Override
	public HashMap<String, String> getGameState() {
		hasUpdates = false;
		return gameState;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#getUpdated()
	 */
	@Override
	public Condition getUpdated() {
		return updated;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#getLock()
	 */
	@Override
	public Lock getLock() {
		return lock;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#isMoving()
	 */
	@Override
	public boolean isMoving() {
		if (gameState.containsKey("moving")) {
			return gameState.get("moving").equals(gameState.get("number"));
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#buyPermitCard(java.lang.String, int, char[])
	 */
	@Override
	public void buyPermitCard(String nameOfTheRegion, int position, char[] politicalCard) throws NetworkException {
		try {
			objectOutputStream.reset();
			objectOutputStream.writeObject(Commands.BUY_PERMIT);
			objectOutputStream.writeObject(nameOfTheRegion);
			objectOutputStream.writeObject(position);
			objectOutputStream.writeObject(politicalCard);
			objectOutputStream.flush();
		} catch (IOException e) {
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#buyKing(char[], char)
	 */
	@Override
	public void buyKing(char[] politicalCard, char city) throws NetworkException {
		try {
			objectOutputStream.reset();
			objectOutputStream.writeObject(Commands.BUY_KING);
			objectOutputStream.writeObject(politicalCard);
			objectOutputStream.writeObject(city);
			objectOutputStream.flush();
		} catch (IOException e) {
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#slideCouncil(char, java.lang.String)
	 */
	@Override
	public void slideCouncil(char color, String nameOfTheRegionOfTheCouncil) throws NetworkException {
		try {
			objectOutputStream.reset();
			objectOutputStream.writeObject(Commands.SLIDE_COUNCIL);
			objectOutputStream.writeObject(color);
			objectOutputStream.writeObject(nameOfTheRegionOfTheCouncil);
			objectOutputStream.flush();
		} catch (IOException e) {
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#putEmporiumInACity(int, char)
	 */
	@Override
	public void putEmporiumInACity(int numberOfThePermitCard, char charOfTheCity) throws NetworkException {
		try {
			objectOutputStream.reset();
			objectOutputStream.writeObject(Commands.PUT_EMPORIUM);
			objectOutputStream.writeObject(numberOfThePermitCard);
			objectOutputStream.writeObject(charOfTheCity);
			objectOutputStream.flush();
		} catch (IOException e) {
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#buyHelper()
	 */
	@Override
	public void buyHelper() throws NetworkException {
		try {
			objectOutputStream.reset();
			objectOutputStream.writeObject(Commands.BUY_HELPER);
			objectOutputStream.flush();
		} catch (IOException e) {
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#refreshPermitsCard(java.lang.String)
	 */
	@Override
	public void refreshPermitsCard(String nameOfTheRegionOfTheCouncil) throws NetworkException {
		try {
			objectOutputStream.reset();
			objectOutputStream.writeObject(Commands.REFRESH_PERMITS);
			objectOutputStream.writeObject(nameOfTheRegionOfTheCouncil);
			objectOutputStream.flush();
		} catch (IOException e) {
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#addAMemberOfTheCouncil(java.lang.String, char)
	 */
	@Override
	public void addAMemberOfTheCouncil(String nameOfTheRegionOfTheCouncil, char color) throws NetworkException {
		try {
			objectOutputStream.reset();
			objectOutputStream.writeObject(Commands.ADD_COUNCIL_MEMBER);
			objectOutputStream.writeObject(nameOfTheRegionOfTheCouncil);
			objectOutputStream.writeObject(color);
			objectOutputStream.flush();
		} catch (IOException e) {
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#buyOffer(core.Offer)
	 */
	@Override
	public void buyOffer(int offer) throws NetworkException {
		try {
			objectOutputStream.reset();
			objectOutputStream.writeObject(Commands.BUY_OFFER);
			objectOutputStream.writeObject(offer);
			objectOutputStream.flush();
		} catch (IOException e) {
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#buyMainAction()
	 */
	@Override
	public void buyMainAction() throws NetworkException {
		try {
			objectOutputStream.reset();
			objectOutputStream.writeObject(Commands.BUY_MAIN_ACTION);
			objectOutputStream.flush();
		} catch (IOException e) {
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#getBonusOfAnEmporium(char)
	 */
	@Override
	public void getBonusOfAnEmporium(char city) throws NetworkException {
		try {
			objectOutputStream.reset();
			objectOutputStream.writeObject(Commands.GET_EMPORIUM_BONUS);
			objectOutputStream.writeObject(city);
			objectOutputStream.flush();
		} catch (IOException e) {
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#getPermitCardBonus(int)
	 */
	@Override
	public void getPermitCardBonus(int position) throws NetworkException {
		try {
			objectOutputStream.reset();
			objectOutputStream.writeObject(Commands.GET_PERMIT_BONUS);
			objectOutputStream.writeObject(position);
			objectOutputStream.flush();
		} catch (IOException e) {
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#pickPermitCard(java.lang.String, int)
	 */
	@Override
	public void pickPermitCard(String region, int permitCard) throws NetworkException {
		try {
			objectOutputStream.reset();
			objectOutputStream.writeObject(Commands.PICK_PERMIT);
			objectOutputStream.writeObject(region);
			objectOutputStream.writeObject(permitCard);
			objectOutputStream.flush();
		} catch (IOException e) {
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#skipQuickAction()
	 */
	@Override
	public void skipAction() throws NetworkException {
		try {
			objectOutputStream.reset();
			objectOutputStream.writeObject(Commands.SKIP_ACTION);
			objectOutputStream.flush();
		} catch (IOException e) {
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#putOffer(char[], int, int, int)
	 */
	@Override
	public void putOffer(char[] politicalCard, int permitCard, int assistants, int price) throws NetworkException {
		try {
			objectOutputStream.reset();
			objectOutputStream.writeObject(Commands.PUT_OFFER);
			objectOutputStream.writeObject(politicalCard);
			objectOutputStream.writeObject(permitCard);
			objectOutputStream.writeObject(assistants);
			objectOutputStream.writeObject(price);
			objectOutputStream.flush();
		} catch (IOException e) {
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#hasUpdates()
	 */
	@Override
	public boolean hasUpdates() {
		return hasUpdates;
	}

	/**
	 * Puts update in the client state and updates the interface that an update
	 * has occured.
	 * 
	 * It also sets the hasUpdates flag to true
	 * 
	 * If the update contains "PLAYING" then the corresponding playing flag is
	 * set to true
	 * 
	 * @param update
	 *            the update to be put in in the client
	 */
	public void update(HashMap<String, String> update) {
		lock.lock();
		gameState.putAll(update);
		if ("PLAYING".equals(gameState.get("state"))) {
			playing = true;
		} else if ("LOBBY".equals(gameState.get("state"))) {
			playing = false;
		}
		hasUpdates = true;
		updated.signalAll();
		lock.unlock();

	}

}

class SocketUpdateListener implements Runnable {

	private static final Logger logger = Logger.getLogger(SocketUpdateListener.class.getName());

	private SocketClient client;
	private ObjectInputStream inputStream;
	private boolean running = true;

	public SocketUpdateListener(SocketClient client, ObjectInputStream inputStream) {
		this.client = client;
		this.inputStream = inputStream;
	}

	/**
	 * Stops the listener's run.
	 */
	public void stop() {
		try {
			inputStream.close();
			running = false;
		} catch (Exception e) {
			logger.log(Level.WARNING, "There was an error closing the inputStream", e);
		}
	}

	/**
	 * Listens for updates from the server and sends them to the client
	 */
	@Override
	public void run() {

		while (running) {

			Object latestUpdate;
			try {
				latestUpdate = inputStream.readObject();
				if (latestUpdate instanceof Map<?, ?>) {
					client.update((HashMap<String, String>) latestUpdate);
				}

			} catch (ClassNotFoundException | IOException e) {
				logger.log(Level.WARNING, "There was an error reading from the socket", e);
				running = false;
				client.quit();
			}

		}

	}
}