package client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author B
 *
 */
public class RMIClient implements Client {

	public static final String CONNECTION_ERROR = "Connection error!";

	// Logging functionality
	private static final Logger logger = Logger.getLogger(RMIClient.class.getName());

	private String username;
	private StateImpl remoteState;
	private RemoteLobby remoteLobby;
	private boolean playing;
	private Lock lock = new ReentrantLock();
	private Condition updated = lock.newCondition();

	private RemotePlayer player;

	private Registry remoteRegistry;
	private RMIUserHandler rmiUserHandler;

	/**
	 * @throws RemoteException
	 *             in case there are errors with the remote implementation
	 * @param address
	 *            the address of the server
	 * @param port
	 *            the port on which the registry is listening
	 * 
	 */
	public RMIClient(String address, int port) throws RemoteException {
		System.setProperty("java.rmi.server.codebase", "file:target/classes/client");
		System.setProperty("java.security.policy", "file:client/security.policy");
		remoteState = new StateImpl(this);

		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}
		remoteRegistry = LocateRegistry.getRegistry(address, port);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#login(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean login(String username, String password) throws NetworkException {

		RMIAuthenticationManager authenticationManager;
		try {
			authenticationManager = (RMIAuthenticationManager) remoteRegistry.lookup("AuthenticationManager");
			rmiUserHandler = authenticationManager.login(username, password);
			if (rmiUserHandler != null) {
				rmiUserHandler.setInitialState(remoteState);
				this.username = username;
				return true;
			}

		} catch (RemoteException | NotBoundException e) {
			throw new NetworkException(e);
		}

		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#register(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean register(String username, String password) throws NetworkException {
		RMIAuthenticationManager authenticationManager;
		try {
			authenticationManager = (RMIAuthenticationManager) remoteRegistry.lookup("AuthenticationManager");
			rmiUserHandler = authenticationManager.register(username, password);
			if (rmiUserHandler != null) {
				rmiUserHandler.setInitialState(remoteState);
				this.username = username;
				return true;
			}

		} catch (RemoteException | NotBoundException e) {
			throw new NetworkException(e);
		}

		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#quit()
	 */
	@Override
	public void quit() throws NetworkException {
		try {
			rmiUserHandler.quit();
		} catch (RemoteException e) {
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#isPlaying()
	 */
	@Override
	public boolean isPlaying() {
		return playing;
	}

	/**
	 * 
	 */
	@Override
	public String getUsername() {
		return username;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#getUsers()
	 */
	@Override
	public List<String> getUserNames() throws NetworkException {
		try {
			remoteState.hasUpdates(false);
			List<String> usernames = remoteLobby.getUsernames();
			usernames.remove(username);
			return usernames;
		} catch (RemoteException e) {
			throw new NetworkException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#isAdmin()
	 */
	@Override
	public boolean isAdmin() throws NetworkException {
		try {
			return remoteLobby.isAdmin(this.username);
		} catch (RemoteException e) {
			throw new NetworkException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#setConfiguration()
	 */
	@Override
	public boolean setConfiguration(String configurationName) throws NetworkException {
		try {
			return remoteLobby.setConfiguration(configurationName);
		} catch (RemoteException e) {
			throw new NetworkException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#startGame()
	 */
	@Override
	public void startGame() throws NetworkException {
		try {
			remoteLobby.startGame();
		} catch (RemoteException e) {
			throw new NetworkException(e);
		}
	}

	/**
	 * @param remoteLobby
	 *            the lobby to be set
	 */
	public void setLobby(RemoteLobby remoteLobby) {
		playing = false;
		this.remoteLobby = remoteLobby;
	}

	/**
	 * 
	 * 
	 * @param player
	 *            the remote player to be set in the RMIClient
	 */
	public void setPlayer(RemotePlayer player) {
		this.player = player;
		playing = true;
		logger.info("player set");
	}

	/**
	 * 
	 */
	@Override
	public Lock getLock() {
		return lock;
	}

	/**
	 * 
	 */
	@Override
	public Condition getUpdated() {
		return updated;
	}

	@Override
	public boolean isMoving() {
		return remoteState.isMoving();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#getState()
	 */
	@Override
	public HashMap<String, String> getGameState() {
		return (HashMap<String, String>) remoteState.getGameState();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#buyPermitCard(java.lang.String, int, char[])
	 */
	@Override
	public void buyPermitCard(String nameOfTheRegion, int position, char[] politicalCard) throws NetworkException {
		try {
			player.buyPermitCard(nameOfTheRegion, position, politicalCard);
		} catch (RemoteException e) {
			logger.log(Level.WARNING, CONNECTION_ERROR, e);
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#buyKing(char[], char)
	 */
	@Override
	public void buyKing(char[] politicalCard, char city) throws NetworkException {
		try {
			player.buyKing(politicalCard, city);
		} catch (RemoteException e) {
			logger.log(Level.WARNING, CONNECTION_ERROR, e);
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#slideCouncil(char, java.lang.String)
	 */
	@Override
	public void slideCouncil(char color, String nameOfTheRegionOfTheCouncil) throws NetworkException {
		try {
			player.slideCouncil(color, nameOfTheRegionOfTheCouncil);
		} catch (RemoteException e) {
			logger.log(Level.WARNING, CONNECTION_ERROR, e);
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#putEmporiumInACity(int, char)
	 */
	@Override
	public void putEmporiumInACity(int numberOfThePermitCard, char charOfTheCity) throws NetworkException {
		try {
			player.putEmporiumInACity(numberOfThePermitCard, charOfTheCity);
		} catch (RemoteException e) {
			logger.log(Level.WARNING, CONNECTION_ERROR, e);
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#buyHelper()
	 */
	@Override
	public void buyHelper() throws NetworkException {
		try {
			player.buyHelper();
		} catch (RemoteException e) {
			logger.log(Level.WARNING, CONNECTION_ERROR, e);
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#refreshPermitsCard(java.lang.String)
	 */
	@Override
	public void refreshPermitsCard(String nameOfTheRegionOfTheCouncil) throws NetworkException {
		try {
			player.refreshPermitCards(nameOfTheRegionOfTheCouncil);
		} catch (RemoteException e) {
			logger.log(Level.WARNING, CONNECTION_ERROR, e);
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#addAMemberOfTheCouncil(java.lang.String, char)
	 */
	@Override
	public void addAMemberOfTheCouncil(String nameOfTheRegionOfTheCouncil, char color) throws NetworkException {
		try {
			player.addAMemberOfTheCouncil(nameOfTheRegionOfTheCouncil, color);
		} catch (RemoteException e) {
			logger.log(Level.WARNING, CONNECTION_ERROR, e);
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#buyOffer(core.Offer)
	 */
	@Override
	public void buyOffer(int offer) throws NetworkException {
		try {
			player.buyOffer(offer);
		} catch (RemoteException e) {
			logger.log(Level.WARNING, CONNECTION_ERROR, e);
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#buyMainAction()
	 */
	@Override
	public void buyMainAction() throws NetworkException {
		try {
			player.buyMainAction();
		} catch (RemoteException e) {
			logger.log(Level.WARNING, CONNECTION_ERROR, e);
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#getBonusOfAnEmporium(char)
	 */
	@Override
	public void getBonusOfAnEmporium(char city) throws NetworkException {
		try {
			player.getBonusOfAnEmporium(city);
		} catch (RemoteException e) {
			logger.log(Level.WARNING, CONNECTION_ERROR, e);
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#getPermitCardBonus(int)
	 */
	@Override
	public void getPermitCardBonus(int position) throws NetworkException {
		try {
			player.getPermitCardBonus(position);
		} catch (RemoteException e) {
			logger.log(Level.WARNING, CONNECTION_ERROR, e);
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#pickPermitCard(java.lang.String, int)
	 */
	@Override
	public void pickPermitCard(String region, int permitCard) throws NetworkException {
		try {
			player.pickPermitCard(region, permitCard);
		} catch (RemoteException e) {
			logger.log(Level.WARNING, CONNECTION_ERROR, e);
			throw new NetworkException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#putOffer(char[], int, int)
	 */
	@Override
	public void putOffer(char[] politicalCard, int permitCard, int assistants, int price) throws NetworkException {
		try {
			player.putOffer(politicalCard, permitCard, assistants, price);
		} catch (RemoteException e) {
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#skipQuickAction()
	 */
	@Override
	public void skipAction() throws NetworkException {
		try {
			player.skipAction();
		} catch (RemoteException e) {
			throw new NetworkException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.Client#hasUpdates()
	 */
	@Override
	public boolean hasUpdates() {
		return remoteState.hasUpdates();
	}

}
