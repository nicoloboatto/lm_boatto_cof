
package client;

import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public interface Client {

	/**
	 * Performs registration on the server
	 * 
	 * @param username
	 *            the username with which to register
	 * @param password
	 *            the password with which to register
	 * @return true if registration was successful, false otherwise
	 * @throws NetworkException
	 *             if there were errors in the underlying network implementation
	 */
	public boolean register(String username, String password) throws NetworkException;

	/**
	 * Performs login on the server
	 * 
	 * @param username
	 *            the username with which to login
	 * @param password
	 *            the password with which to login
	 * @return true if login was successful, false otherwise
	 * @throws NetworkException
	 *             if there were errors in the underlying network implementation
	 */
	public boolean login(String username, String password) throws NetworkException;

	/**
	 * 
	 * @return the username of the client
	 */
	public String getUsername();

	/**
	 * 
	 * @return the list of the usernames of the OTHER players in the lobby
	 * @throws NetworkException
	 *             in case there are errors in the underlying network
	 *             implementation
	 */
	public List<String> getUserNames() throws NetworkException;

	/**
	 * While in the lobby, sets the game configuration of the game
	 * 
	 * @param configurationName
	 *            name of the configuration to load
	 * @return true if the configuration was found, false otherwise
	 * @throws NetworkException
	 *             if there were errors in the underlying network implementation
	 */
	public boolean setConfiguration(String configurationName) throws NetworkException;

	/**
	 * @throws NetworkException
	 *             if there were errors in the underlying network implementation
	 */
	public void startGame() throws NetworkException;

	/**
	 * Returns the current game state and resets hasUpdates as false
	 * 
	 * @return the current game state
	 */
	public Map<String, String> getGameState();

	/**
	 * 
	 * @return true if a new game state is already available
	 */
	public boolean hasUpdates();

	/**
	 * First main action from the game
	 * 
	 * Buys a permitcard from the specified region, using the specified
	 * politicalCards
	 * 
	 * @param nameOfTheRegion
	 *            the name of the region of the council
	 * @param position
	 *            the position of the card can be 0 or 1
	 * @param politicalCards
	 *            the cards to be used to buy the card
	 * @throws NetworkException
	 *             if there were errors in the underlying network implementation
	 */
	void buyPermitCard(String nameOfTheRegion, int position, char[] politicalCards) throws NetworkException;

	/**
	 * Second main action from the game
	 * 
	 * @param politicalCard
	 *            the character array of the colors of political cards to be
	 *            used
	 * @param city
	 *            the initial of the name of the city in which to move the king
	 * @throws NetworkException
	 *             if there were errors in the underlying network implementation
	 */
	void buyKing(char[] politicalCard, char city) throws NetworkException;

	/**
	 * Third main action from the Game
	 * 
	 * adds a counselor to the council
	 * 
	 * @param color
	 *            the color of to be inserted in the council
	 * @param nameOfTheRegion
	 *            the name of the region that contains the council
	 * @throws NetworkException
	 *             if there were errors in the underlying network implementation
	 */
	void slideCouncil(char color, String nameOfTheRegion) throws NetworkException;

	/**
	 * Fourth main action from the Game
	 * 
	 * Puts an emporium in the specified city using the specified card
	 * 
	 * @param numberOfThePermitCard
	 *            the number of the permitCard to be used
	 * @param charOfTheCity
	 *            the first character of name of the city
	 * @throws NetworkException
	 *             if there were errors in the underlying network implementation
	 */
	void putEmporiumInACity(int numberOfThePermitCard, char charOfTheCity) throws NetworkException;

	/**
	 * quick action number 1
	 * 
	 * @throws NetworkException
	 *             if there were errors in the underlying network implementation
	 */
	void buyHelper() throws NetworkException;

	/**
	 * Quick action number 2 from the game
	 * 
	 * Refreshes permit cards
	 * 
	 * @param nameOfTheRegionOfTheCouncil
	 *            the name of the region which contains the council
	 * 
	 * @throws NetworkException
	 *             if there were errors in the underlying network implementation
	 */
	void refreshPermitsCard(String nameOfTheRegionOfTheCouncil) throws NetworkException;

	/**
	 * 
	 * @param nameOfTheRegion
	 *            the name of the region which contains the council
	 * @param color
	 *            quick action number 3
	 * @throws NetworkException
	 *             if there were errors in the underlying network implementation
	 */
	void addAMemberOfTheCouncil(String nameOfTheRegion, char color) throws NetworkException;

	/**
	 * Quick action from the game, buys main action
	 * 
	 * @throws NetworkException
	 *             if there were errors in the underlying network implementation
	 */
	public void buyMainAction() throws NetworkException;

	/**
	 * Used for skipping quick action
	 * 
	 * 
	 * @throws NetworkException
	 *             if there were errors in the underlying network implementation
	 */
	public void skipAction() throws NetworkException;

	/**
	 * Special bonus move from the game
	 * 
	 * Activates the bonus from a city with an emporium
	 * 
	 * @param city
	 *            initial of the city from which to get a bonus
	 * @throws NetworkException
	 *             if there were errors in the underlying network implementation
	 */
	void getBonusOfAnEmporium(char city) throws NetworkException;

	/**
	 * 
	 * 
	 * @param position
	 *            position of the card from which to get the bonus, from the
	 *            list of permit cards in the player
	 * @throws NetworkException
	 *             if there were errors in the underlying network implementation
	 */
	void getPermitCardBonus(int position) throws NetworkException;

	/**
	 * 
	 * @param region
	 *            name of the region from which to get the permit card
	 * @param permitCard
	 *            position of the permit card, can be 0 or 1
	 * @throws NetworkException
	 *             if there were errors in the underlying network implementation
	 */
	void pickPermitCard(String region, int permitCard) throws NetworkException;

	/**
	 * 
	 * 
	 * @return the client lock to make update management thread safe
	 */
	public Lock getLock();

	/**
	 * Market action, buys the specified offer
	 * 
	 * @param offer
	 *            the number of the offer from the market
	 * @throws NetworkException
	 *             if there were errors in the underlying network implementation
	 */
	void buyOffer(int offer) throws NetworkException;

	/**
	 * Puts an offer in the market
	 * 
	 * @param politicalCard
	 *            the character array of cards the player wants to sell
	 * @param permitCard
	 *            the number of the permit card from the player's list
	 * @param assistants
	 *            the number of assistants the player wants to sell
	 * @param price
	 *            the price to be paid for th offer
	 * @throws NetworkException
	 *             if there were errors in the underlying network implementation
	 */
	void putOffer(char[] politicalCard, int permitCard, int assistants, int price) throws NetworkException;

	/**
	 * 
	 * @return true if the client is playing, false otherwise
	 */
	public boolean isPlaying();

	/**
	 * 
	 * @return true if the player is admin, false otherwise
	 * @throws NetworkException
	 *             if there were errors in the underlying network implementation
	 */
	public boolean isAdmin() throws NetworkException;

	/**
	 * 
	 * @return true if the player is movin, false otherwise
	 */
	boolean isMoving();

	/**
	 * Returns the condition on which the client calls the signal() method
	 * 
	 * @return the updated condition
	 */
	Condition getUpdated();

	/**
	 * Stops client execution
	 * 
	 * @throws NetworkException
	 *             in case the disconnection returns errors
	 */
	public void quit() throws NetworkException;

}
