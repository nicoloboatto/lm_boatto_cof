package client;

import java.rmi.Remote;
import java.rmi.RemoteException;

import server.State;

public interface RMIUserHandler extends Remote {

	/**
	 * Sets the remote state object so that the server can update the clients
	 * 
	 * @param state
	 *            the state to be set in the client
	 * @throws RemoteException
	 *             in case there are errors in the remote implementation
	 */
	void setInitialState(State state) throws RemoteException;

	/**
	 * Quits from the server so that the user is removed from its game or lobby
	 * 
	 * @throws RemoteException
	 *             in case there are errors in the remote implementation
	 */
	void quit() throws RemoteException;

}
