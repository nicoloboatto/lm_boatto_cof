package client;

import java.util.logging.ConsoleHandler;

public class CustomConsoleHandler extends ConsoleHandler {

	/**
	 * Changes the console output from System.err to System.out so that some
	 * logs can also serve as CLI
	 */
	public CustomConsoleHandler() {
		super();
		this.setOutputStream(System.out);
	}

}
