package client;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.concurrent.locks.Lock;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class CommandLineHandler {

	/**
	 * 
	 */
	private static final String MESSAGE = "message";

	// Logging functionality
	private static final Logger logger = Logger.getLogger(CommandLineHandler.class.getName());

	private static Scanner scanner = new Scanner(System.in);
	private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

	private static Client client;
	private static boolean running = true;

	/**
	 * Useful strings for the command line
	 * 
	 * 
	 */
	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_PURPLE = "\u001B[35m";
	public static final String ANSI_CYAN = "\u001B[36m";
	public static final String ANSI_WHITE = "\u001B[37m";
	public static final String LOGO = "   ______                       _ __         ____   ______                "
			+ "\n  / ____/___  __  ______  _____(_) /  ____  / __/  / ____/___  __  _______"
			+ "\n / /   / __ \\/ / / / __ \\/ ___/ / /  / __ \\/ /_   / /_  / __ \\/ / / / ___/"
			+ "\n/ /___/ /_/ / /_/ / / / / /__/ / /  / /_/ / __/  / __/ / /_/ / /_/ / /    "
			+ "\n\\____/\\____/\\__,_/_/ /_/\\___/_/_/   \\____/_/    /_/    \\____/\\__,_/_/     "
			+ "\n                                                                          ";
	public static final String POSSIBLE_MOVES = "possibleMoves";
	public static final String SOME_NEW_LINES = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
	public static final String HUGE_SPACE = "----------------------------------------------------------------------------------------------------";
	public static final String NUMBER_OF_OFFER = "numberOfOffer";
	public static final String BONUS = "Bonus";
	public static final String PLAYER_NAME_LOWERCASE = "player";
	public static final String PLAYER_NAME_UPPERCASE = "Player";
	public static final String OFFER = "offer";
	public static final String PERMITCARDS = "PermitCards";
	public static final String PERMITCARD = "PermitCard";
	public static final String HELPERS = "Helpers";
	public static final String MONEY = "Money";
	public static final String VICTORY = "Victory";
	public static final String NOBILITY = "Nobility";
	public static final String EMPORIUMS = "Emporiums";

	private CommandLineHandler() {
		// Empty method to override default public one
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		CommandLineHandler.setup();
		handleCommands();
		System.exit(0);
	}

	private static void handleCommands() {
		try {
			while (!authenticate())
				;

			while (running) {
				lobby();
				game();
				finalMenu();
			}

		} catch (NetworkException e) {
			logger.severe("Connection to the server was lost, please try connecting later");
			logger.log(Level.WARNING, "Connection error: ", e);

			return;
		}
		try {
			logger.severe("Quitting...");
			client.quit();
		} catch (NetworkException e) {
			logger.log(Level.WARNING, "There was an error quitting", e);
		}
	}

	/**
	 * Sets up logging to file and console reading from a configuration file
	 * 
	 * 
	 */
	private static void setup() {

		logger.setLevel(Level.WARNING);

		try {
			FileInputStream clientConfig = new FileInputStream("client/client.properties");
			LogManager.getLogManager().readConfiguration(clientConfig);

		} catch (IOException e) {
			logger.log(Level.SEVERE, "Could not load configuration file", e);
			logger.warning("Logging not configured, will output to console");
		}
		CommandLineHandler.clientSetup();
	}

	/**
	 * This method is the setup for the client, the client must to decide the
	 * type of the connection
	 */
	private static void clientSetup() {

		boolean setupComplete = false;
		logger.severe(LOGO);
		logger.severe("\nWelcome!");

		while (!setupComplete) {

			switch (selectConnection()) {
			case "1":
				setupSocketClient();
				setupComplete = true;
				break;
			case "2":
				setupRMIClient();
				setupComplete = true;
				break;
			default:
			}
		}

	}

	/**
	 * This method is for print in command line which selection you want to make
	 * 
	 * @return
	 */
	private static String selectConnection() {

		String selection;
		logger.severe("What type of connection would you like to use?");
		logger.severe("(Just type the corresponding number)");
		logger.severe("1: Socket");
		logger.severe("2: RMI");
		selection = readMenuString();

		while (selection != "1" ^ selection != "2") {
			logger.severe("NOPE! That is not a valid option, try again");
			selection = readMenuString();

		}
		return selection;

	}

	/**
	 * This metod setup the socket client
	 */
	private static void setupSocketClient() {
		while (client == null) {
			logger.severe("What's the server's address?");
			String ip = readMenuString();
			logger.severe("What's the server's port? (default is 6666)");
			int port = readMenuInt();

			try {
				client = new SocketClient(ip, port);
			} catch (IOException e) {
				logger.severe(
						"There was a connection error, please make sure the information you entered was correct.");
				logger.log(Level.WARNING, "", e);
			}
		}

	}

	/**
	 * This metod setup the RMI client
	 */
	private static void setupRMIClient() {
		while (client == null) {
			logger.severe("What's the server's IP address?");
			String ip = readMenuString();
			logger.severe("What's the server's port? (default is 1099)");
			int port = readMenuInt();
			try {
				client = new RMIClient(ip, port);
			} catch (RemoteException e) {
				logger.severe(
						"There was a connection error, please make sure the information you entered was correct.");
				logger.log(Level.WARNING, "", e);
			}
		}
	}

	/**
	 * This metod is for choosing to register or to login
	 * 
	 * @return
	 * @throws NetworkException
	 */
	private static boolean authenticate() throws NetworkException {

		logger.severe("Please choose whether you want to Login or Register");
		logger.severe("1: Login");
		logger.severe("2: Register");
		int selection;
		try {
			selection = Integer.parseInt(scanner.nextLine());
		} catch (NumberFormatException e) {
			logger.log(Level.INFO, "invalid user input", e);
			return false;
		}

		switch (selection) {

		case 1:
			return login();
		case 2:
			return register();
		default:
			logger.severe("Please enter a valid input!");
		}

		return false;

	}

	/**
	 *
	 * @return
	 * @throws NetworkException
	 */
	private static boolean login() throws NetworkException {
		String username;
		String password;

		logger.severe("Please enter your username");
		username = scanner.nextLine();
		logger.severe("Please enter your password");
		password = scanner.nextLine();
		try {
			if (client.login(username, password)) {
				return true;
			}
		} catch (Exception e) {
			throw new NetworkException(e);
		}
		logger.severe("Invalid login!");
		return false;
	}

	/**
	 * 
	 * @return
	 * @throws NetworkException
	 */
	private static boolean register() throws NetworkException {
		String username;
		String password;

		logger.severe("Please enter desired username");
		username = scanner.nextLine();
		logger.severe("Please enter desired password");
		password = scanner.nextLine();
		logger.severe("Please enter password confirmation");
		String passwordConf = scanner.nextLine();
		if (passwordConf.equals(password)) {
			try {
				if (client.register(username, password)) {
					return true;
				}
			} catch (Exception e) {
				throw new NetworkException(e);
			}
			logger.severe("Username already exists!");
			return false;
		}

		logger.severe("Password does not match confirmation!");
		return false;

	}

	private static void waitForUpdates() {
		Lock lock = client.getLock();
		logger.info("Acquiring lock from CLI");
		lock.lock();
		logger.info("CLI lobby lock acquired, now awaiting...");
		while (!client.hasUpdates()) {
			try {
				client.getUpdated().await();
			} catch (InterruptedException e) {
				logger.log(Level.INFO, "Interrupted!", e);
				Thread.currentThread().interrupt();
			}
		}

		logger.info("CLI now awake!");
		lock.unlock();
	}

	private static void lobby() throws NetworkException {

		logger.severe("Authentication complete!");
		logger.severe("Welcome to the lobby!\n");
		waitForUpdates();
		while (!client.isPlaying()) {
			listUsers();
			if (client.isAdmin()) {
				try {
					adminMenu();
					if (client.isPlaying())
						break;
				} catch (TurnOverException e) {
					logger.log(Level.WARNING, "Time for input has passed", e);
				}
			}

			waitForUpdates();
		}

	}

	/**
	 * This is the menu of the admin. An admin can set the game configuration or
	 * start a game
	 * 
	 * @throws TurnOverException
	 * 
	 */
	private static void adminMenu() throws NetworkException, TurnOverException {

		boolean decision = false;

		while (!decision) {
			logger.severe("You're the admin, what would you like to do?");
			logger.severe("1: Set game configuration");
			if (!client.getUserNames().isEmpty()) {
				logger.severe("2: Start game");
			}
			logger.severe("If you don't select a game configuration a default one will be set for you");
			int selection = readGameMenuInt();
			switch (selection) {

			case 1:
				setConfiguration();
				break;

			case 2:
				if (!client.getUserNames().isEmpty()) {
					logger.severe("Starting Game!");
					decision = true;
					client.startGame();
				}

				break;
			default:
				break;
			}
		}

	}

	/**
	 * This menu is printed at the end of the game. It gives to you the
	 * possibility to quit the game or return to the lobby
	 */
	private static void finalMenu() {
		logger.severe("Do you want to return to the Lobby or quit the Game?");
		logger.severe("1- Return to the Lobby");
		logger.severe("2- Quit the Game");
		int decision = readMenuInt();
		if (decision == 2)
			running = false;
	}

	/**
	 * This method check if the player is the one that can move, if he can move
	 * this method print all the game, from the state of the game to the
	 * possible moves
	 * 
	 * @throws NetworkException
	 */
	private static void game() throws NetworkException {

		waitForUpdates();
		HashMap<String, String> gameState;
		while (client.isPlaying()) {
			gameState = (HashMap<String, String>) client.getGameState();
			printGame(gameState);
			if ("OVER".equals(gameState.get("state"))) {
				logger.severe("END OF THE GAME!");
				logger.severe("THE WINNER IS: " + gameState.get("winner"));
			}

			if (client.isMoving() && !"OVER".equals(gameState.get("state"))) {
				logger.severe("Time limit is " + gameState.get("timeLimit") + " seconds.");
				try {
					if ("C".equals(gameState.get(POSSIBLE_MOVES))) {
						printCollectingOfferMenu();
					} else if ("M".equals(gameState.get(POSSIBLE_MOVES))) {
						printMarketMenu(gameState);
					} else if (checkSpecialMoves(gameState)) {
						printSpecialMoves(gameState);
					} else {
						printGameMenu(gameState);
					}

				} catch (TurnOverException e) {
					logger.log(Level.WARNING, "Not the player's turn", e);
					logger.severe("It's not your turn anymore!");
				}
			}
			waitForUpdates();
		}

	}

	/**
	 * This method print in command line the list of the users
	 * 
	 * @throws NetworkException
	 *             in case there are errors
	 */
	private static void listUsers() throws NetworkException {

		ArrayList<String> userlist = (ArrayList<String>) client.getUserNames();
		userlist.remove(client.getUsername());
		if (userlist.isEmpty()) {
			logger.severe("You're the only one in the lobby at the moment");
			return;
		}
		logger.severe("Other online players are: ");
		printList(userlist);
	}

	/**
	 * This method set the configuration of the game like the map, the bonuses
	 * etc..
	 * 
	 * @throws NetworkException
	 */
	private static void setConfiguration() throws NetworkException {
		logger.severe("What configuration would you like to set?");
		logger.severe("1: Default 1");
		logger.severe("2: Default 2");
		logger.severe("3: Default 3");
		logger.severe("Or just type the name of the desired configuration");
		if (scanner.hasNextInt()) {
			int defaultSeletion = Integer.parseInt(scanner.nextLine());
			client.setConfiguration("default" + defaultSeletion);

			logger.severe("Default " + defaultSeletion + " configuration selected");
		} else {
			client.setConfiguration(scanner.nextLine());
		}

	}

	private static void printList(ArrayList<String> list) {
		for (String item : list) {
			logger.severe(item);
		}
	}

	/**
	 * This method check if the player can do a special moves. The special moves
	 * are gained by the nobility level
	 * 
	 * @param gameState
	 * @return true if the player has a special move, false otherwise
	 */
	private static Boolean checkSpecialMoves(HashMap<String, String> gameState) {
		return (int) (gameState.get(POSSIBLE_MOVES).charAt(9)) - 48 > 0
				|| gameState.get(POSSIBLE_MOVES).charAt(10) == '1' || gameState.get(POSSIBLE_MOVES).charAt(11) == '1';
	}

	/**
	 * This method print the special moves that the player can do
	 * 
	 * @param gameState
	 * @throws TurnOverException
	 * @throws NetworkException
	 */
	private static void printSpecialMoves(HashMap<String, String> gameState)
			throws TurnOverException, NetworkException {
		logger.severe("You have a Special Bonus: ");
		if ((int) (gameState.get(POSSIBLE_MOVES).charAt(9)) - 48 > 0) {
			String buff = new String();
			for (int i = 0; i != (int) (gameState.get(POSSIBLE_MOVES).charAt(9)) - 48; i++) {
				logger.severe("You can get the bonus of a City where you have an Emporium!");
				logger.severe("Chose the City: ");
				String city = readGameMenuString();
				if (buff != city)
					client.getBonusOfAnEmporium(city.charAt(0));
				buff = city;
			}
		}
		if (gameState.get(POSSIBLE_MOVES).charAt(10) == '1') {
			logger.severe("You can get the Bonus of a PermitCard that you own!");
			logger.severe("Type the position of the PermitCard: ");
			int position = readGameMenuInt();
			client.getPermitCardBonus(position - 1);
		}
		if (gameState.get(POSSIBLE_MOVES).charAt(11) == '1') {
			logger.severe("You can pick a PermitCard from a Council!");
			printChoseWhichCouncil();
			int council = readGameMenuInt();
			while (council > 3) {
				logger.severe("Invalid input, try again");
				council = readGameMenuInt();
			}
			logger.severe("PermitCard 1 or 2?");
			int permitCard = checkOneOrTwo();
			client.pickPermitCard(intToRegionName(council), permitCard - 1);
		}
	}

	/**
	 * This method print the game menu and all the possible moves
	 * 
	 * @throws TurnOverException
	 * @throws NetworkException
	 */
	private static void printGameMenu(HashMap<String, String> gameState) throws TurnOverException, NetworkException {
		boolean moveMade = false;
		printCalculatePossibleMove(gameState);
		while (!moveMade) {
			int selection = readGameMenuInt();
			if (checkSelection(selection, gameState)) {
				switch (selection) {
				case 1:
					menuBuyAPermitCard();
					break;
				case 2:
					menuMoveTheKing();
					break;
				case 3:
					menuSlideACouncil();
					break;
				case 4:
					menuPutAnEmporiumInACity();
					break;
				case 5:
					menuBuyAnHelper();
					break;
				case 6:
					menuRefreshThePermitsCardsInACouncil();
					break;
				case 7:
					menuSlideACouncilWithHelpers();
					break;
				case 8:
					menuBuyAMainAction();
					break;
				case 9:
					client.skipAction();
					break;
				default:
				}
				moveMade = true;
			}
		}
	}

	private static void printCalculatePossibleMove(HashMap<String, String> gameState) {
		logger.severe("Chose which action you would like to do:");
		String buff = gameState.get(POSSIBLE_MOVES);

		if (buff.charAt(0) == '1')
			logger.severe("1- Main Action N°1: Buy a PermitCard");
		if (buff.charAt(1) == '1')
			logger.severe("2- Main Action N°2: Move the King");
		if (buff.charAt(2) == '1')
			logger.severe("3- Main Action N°3: Slide a Council");
		if (buff.charAt(3) == '1')
			logger.severe("4- Main Action N°4: Put an Emporium in a City");
		if (buff.charAt(4) == '1')
			logger.severe("5- Quick Action N°1: Buy a Helper");
		if (buff.charAt(5) == '1')
			logger.severe("6- Quick Action N°2: Refresh the Permit Cards in a Council");
		if (buff.charAt(6) == '1')
			logger.severe("7- Quick Action N°3: Slide a Council with Helpers");
		if (buff.charAt(7) == '1')
			logger.severe("8- Quick Action N°4: Buy a Main Action");
		if (buff.charAt(8) == '1')
			logger.severe("9- Skip quick action");

	}

	private static Boolean checkSelection(int selection, HashMap<String, String> gameState) {
		String buff = gameState.get(POSSIBLE_MOVES);
		return buff.charAt(selection - 1) == '1';
	}

	/**
	 * This is the method of the menu BuyAPermitCard
	 * 
	 * @throws TurnOverException
	 * @throws NetworkException
	 */
	private static void menuBuyAPermitCard() throws TurnOverException, NetworkException {
		printChoseWhichCouncil();
		int council = readGameMenuInt();
		while (council > 3) {
			logger.severe("Invalid input, try again");
			council = readGameMenuInt();
		}
		logger.severe("Chose which PermitCard:");
		logger.severe("Number 1 or number 2?");
		int permitCard = checkOneOrTwo();
		logger.severe("Chose which PoliticalCard:");
		String politicalCard = readGameMenuString();
		client.buyPermitCard(intToRegionName(council), permitCard - 1, politicalCard.toCharArray());
		logger.severe("Added PermitCard and its bonuses");

	}

	/**
	 * This is the method of the menu MoveTheKing
	 * 
	 * @throws TurnOverException
	 * @throws NetworkException
	 */
	private static void menuMoveTheKing() throws TurnOverException, NetworkException {
		logger.severe("Chose which Political Cards to use:");
		String politicalCard = readGameMenuString();
		logger.severe("Where you whant to move the King?");
		String city = readGameMenuString();
		client.buyKing(politicalCard.toCharArray(), city.toCharArray()[0]);
		logger.severe("King moved to " + city);
	}

	/**
	 * This is the method of the menu SlideACouncil
	 * 
	 * @throws TurnOverException
	 * @throws NetworkException
	 */
	private static void menuSlideACouncil() throws TurnOverException, NetworkException {
		printChoseWhichCouncil();
		logger.severe("4- King council");
		int council = readGameMenuInt();
		while (council > 4) {
			logger.severe("Invalid input, try again");
			council = readGameMenuInt();
		}
		logger.severe("Chose which Color:");
		String color = checkPoliticalCard();
		client.slideCouncil(color.toCharArray()[0], intToRegionName(council));
		logger.severe("Color " + color + " added in Council " + Integer.toString(council));
	}

	/**
	 * This is the method of the menu PutAnEmporiumInACity
	 * 
	 * @throws TurnOverException
	 * @throws NetworkException
	 */
	private static void menuPutAnEmporiumInACity() throws TurnOverException, NetworkException {
		logger.severe("Chose which PermitCard, type the position of the PermitCard:");
		int permitCard = readGameMenuInt();
		logger.severe("Chose witch City:");
		String cityOfThePermitCard = readGameMenuString();
		client.putEmporiumInACity(permitCard - 1, cityOfThePermitCard.toCharArray()[0]);
		logger.severe("Builded Emporium in " + cityOfThePermitCard);
	}

	/**
	 * This is the method of the menu BuyAnHelper
	 * 
	 * @throws NetworkException
	 */
	private static void menuBuyAnHelper() throws NetworkException {
		client.buyHelper();
		logger.severe("Removed 3 Money, added 1 Helper");
	}

	/**
	 * This is the method of the menu RefreshThePermitsCardsInACounci
	 * 
	 * @throws TurnOverException
	 * @throws NetworkException
	 */
	private static void menuRefreshThePermitsCardsInACouncil() throws TurnOverException, NetworkException {
		printChoseWhichCouncil();
		int council = readGameMenuInt();
		while (council > 3) {
			logger.severe("Invalid input, tru again");
			council = readGameMenuInt();
		}
		client.refreshPermitsCard(intToRegionName(council));
		logger.severe("Removed 1 Helper and refreshed the Permit Cards in Council " + Integer.toString(council));
	}

	/**
	 * This is the method of the menu SlideACouncilWithHelpers
	 * 
	 * @throws TurnOverException
	 * @throws NetworkException
	 */
	private static void menuSlideACouncilWithHelpers() throws TurnOverException, NetworkException {
		printChoseWhichCouncil();
		logger.severe("4- King Council");
		int council = readGameMenuInt();
		while (council > 4) {
			logger.severe("Invalid input, tru again");
			council = readGameMenuInt();
		}
		logger.severe("Chose which Color:");
		String color = checkPoliticalCard();
		client.addAMemberOfTheCouncil(intToRegionName(council), color.toCharArray()[0]);
		logger.severe("Color " + color + " added in Council " + Integer.toString(council));
	}

	/**
	 * This is the method of the menu BuyAMainAction
	 * 
	 * @throws NetworkException
	 */
	private static void menuBuyAMainAction() throws NetworkException {
		client.buyMainAction();
		logger.severe("Removed 3 Helpers, added 1 Main Action");
	}

	/**
	 * This method print the properties of the game
	 * 
	 * @param gameState
	 */
	private static void printGame(HashMap<String, String> gameState) {
		if ("NOPE".equals(gameState.get("lastMove"))) {
			if (client.isMoving())
				logger.severe("You made an invalid move:");

			else
				logger.severe("The player made an invalid move:");

			logger.severe(gameState.get(MESSAGE));
			return;
		}
		logger.severe(SOME_NEW_LINES + HUGE_SPACE);
		logger.severe("");
		printMap(gameState);
		logger.severe("");
		logger.severe("The location of the King is: " + gameState.get("locationOfTheKing"));
		logger.severe("");
		printBonusOfTheNobility(gameState);
		logger.severe("");
		printBonusOfTheCities(gameState);
		logger.severe("");
		printPlayers(gameState);
		logger.severe("");
		printPermitCards(gameState);
		logger.severe("");
		printCouncil(gameState);
		logger.severe("");
		printPermitCardsOfThePlayer(gameState);
		logger.severe("");
		printPoliticalCards(gameState);
		logger.severe("");
		if ("LAST_TURN".equals(gameState.get("state"))) {
			logger.severe("WARNING: LAST TURN!");
		}
		logger.severe("Now playing: " + gameState.get("player" + gameState.get("moving") + "Username"));
		logger.severe("");

	}

	/**
	 * This method prints the collecting offer menu, where the player can do an
	 * offer
	 * 
	 * @throws TurnOverException
	 * @throws NetworkException
	 */
	private static void printCollectingOfferMenu() throws TurnOverException, NetworkException {
		String politicalCard = null;
		int permitCard = -1;
		int helpers = 0;
		int price = 0;
		boolean offerCollected = false;
		logger.severe(HUGE_SPACE);
		logger.severe("");
		logger.severe("It's your turn to put something up for sale");
		logger.severe("");
		while (!offerCollected) {
			logger.severe("Do you want to add an Offer?");
			printYesOrNo();
			int decision1 = checkOneOrTwo();
			if (decision1 == 1) {
				logger.severe("Do you want to Offer Political Cards?");
				printYesOrNo();
				int decision2 = checkOneOrTwo();
				if (decision2 == 1) {
					logger.severe("Type the PoliticalCards that you want to Offer");
					politicalCard = readGameMenuString();
				}
				logger.severe("Do you want to Offer a PermitCard?");
				printYesOrNo();
				int decision3 = checkOneOrTwo();
				if (decision3 == 1) {
					logger.severe("type the position of the PermitCard that you want to Offer");
					permitCard = readGameMenuInt() - 1;
				}
				logger.severe("Do you want to Offer Helpers?");
				printYesOrNo();
				int decision4 = checkOneOrTwo();
				if (decision4 == 1) {
					logger.severe("How many Helpers do you want to Offer?");
					helpers = readGameMenuInt();
				}
				logger.severe("How many Money?");
				price = readGameMenuInt();
				if (price > 0)
					if (politicalCard != null) {
						offerCollected = true;
						client.putOffer(politicalCard.toCharArray(), permitCard, helpers, price);
						logger.severe("Offer added!");
					} else {
						offerCollected = true;
						client.putOffer(null, permitCard, helpers, price);
						logger.severe("Offer added!");
					}
			}
			if (decision1 == 2) {
				offerCollected = true;
				logger.severe("No Offer added!");
			}
		}
		client.skipAction();
	}

	/**
	 * This method prints the market menu where the player can buy an offer of
	 * an another player
	 * 
	 * @param gameState
	 * @throws TurnOverException
	 * @throws NetworkException
	 */
	private static void printMarketMenu(HashMap<String, String> gameState) throws TurnOverException, NetworkException {
		logger.severe(HUGE_SPACE);
		logger.severe("");
		logger.severe("It's your turn to buy an offer!");
		logger.severe("");
		if (!"0".equals(gameState.get(NUMBER_OF_OFFER)) && gameState.get(NUMBER_OF_OFFER) != null
				&& !client.getUsername().equals(gameState.get("offer0" + client.getUsername()))) {
			printMarket(gameState);
			logger.severe("Do you want to buy an Offer?");
			printYesOrNo();
			int decision = checkOneOrTwo();
			if (decision == 1) {
				logger.severe("Type the position of the Offer that you want to buy");
				int offer = readGameMenuInt();
				client.buyOffer(offer - 1);
			} else {
				client.skipAction();
				logger.severe("Exit from the MarketMenu");
			}
		} else {
			logger.severe("There are no Offers in the Market!");
			logger.severe("Press Enter to exit!");
			readGameMenuString();
			client.skipAction();
		}

	}

	/**
	 * This method prints the properties of the map
	 * 
	 * @param gameState
	 */
	private static void printMap(HashMap<String, String> gameState) {
		String buff = new String();
		char c;
		for (int i = 0; i != gameState.get("commandLineMap").length(); i++) {
			c = gameState.get("commandLineMap").charAt(i);
			if (isACity(c))
				buff += whichColorForCity(gameState, c);
			else
				buff += Character.toString(c);
		}
		logger.severe(buff);
	}

	/**
	 * This method prints the bonuses of the nobility
	 * 
	 * @param gameState
	 */
	private static void printBonusOfTheNobility(HashMap<String, String> gameState) {
		String buff = new String("Nobility: ");
		for (int i = 0; i != Integer.parseInt(gameState.get("numberOfNobilityLevel")); i++) {
			if (gameState.get("nobility" + i + BONUS).charAt(0) != '0')
				buff += " Level " + i + " :" + gameState.get("nobility" + i + BONUS);
			if ((i + 1) % 10 == 0) {
				logger.severe(buff);
				buff = "	";
			}

		}

	}

	/**
	 * This method prints the properties of the players
	 * 
	 * @param gameState
	 */
	private static void printPlayers(HashMap<String, String> gameState) {
		for (int i = 0; i != Integer.parseInt(gameState.get("numberOfPlayers")); i++) {
			if ("OFFLINE".equals(gameState.get(PLAYER_NAME_LOWERCASE + i + "State")))
				logger.severe("Player " + Integer.toString(i + 1) + " is offline, his status:");
			else
				logger.severe("Player " + Integer.toString(i + 1) + " is online, his status:");
			logger.severe("	Username: " + gameState.get(PLAYER_NAME_LOWERCASE + i + "Username") + "	Money: "
					+ gameState.get(PLAYER_NAME_LOWERCASE + i + MONEY) + "	Helpers: "
					+ gameState.get(PLAYER_NAME_LOWERCASE + i + HELPERS) + "	Victory: "
					+ gameState.get(PLAYER_NAME_LOWERCASE + i + VICTORY) + "	Nobility: "
					+ gameState.get(PLAYER_NAME_LOWERCASE + i + NOBILITY) + "	Emporiums: "
					+ gameState.get(PLAYER_NAME_LOWERCASE + i + EMPORIUMS));
		}
	}

	/**
	 * This method prints the properties of the permitCard
	 * 
	 * @param gameState
	 */
	private static void printPermitCards(HashMap<String, String> gameState) {
		logger.severe("Sea permitcard:      N°1- Cities: " + gameState.get("sea0") + " " + BONUS + " : "
				+ gameState.get("sea0Bonus") + "	N°2- Cities: " + gameState.get("sea1") + " " + BONUS + " : "
				+ gameState.get("sea1Bonus"));
		logger.severe("Hill permitcard:     N°1- Cities: " + gameState.get("hill0") + " " + BONUS + " : "
				+ gameState.get("hill0Bonus") + "	N°2- Cities: " + gameState.get("hill1") + " " + BONUS + " : "
				+ gameState.get("hill1Bonus"));
		logger.severe("Mountain permitcard: N°1- Cities: " + gameState.get("mountain0") + " " + BONUS + " : "
				+ gameState.get("mountain0Bonus") + "	N°2- Cities: " + gameState.get("mountain1") + " " + BONUS
				+ " : " + gameState.get("mountain1Bonus"));
	}

	/**
	 * This method prints the properties of the councils
	 * 
	 * @param gameState
	 */
	private static void printCouncil(HashMap<String, String> gameState) {
		String buff = new String();
		char c;
		for (int i = 0; i != gameState.get("seaCouncil").length(); i++) {
			c = gameState.get("seaCouncil").charAt(i);
			buff += whichColorForHandAndCouncil(c);
		}
		logger.severe("Sea council: 	  " + buff);
		buff = new String();
		for (int i = 0; i != gameState.get("hillCouncil").length(); i++) {
			c = gameState.get("hillCouncil").charAt(i);
			buff += whichColorForHandAndCouncil(c);
		}
		logger.severe("Hill council:     " + buff);
		buff = new String();
		for (int i = 0; i != gameState.get("mountainCouncil").length(); i++) {
			c = gameState.get("mountainCouncil").charAt(i);
			buff += whichColorForHandAndCouncil(c);
		}
		logger.severe("Mountain council: " + buff);
		buff = new String();
		for (int i = 0; i != gameState.get("kingCouncil").length(); i++) {
			c = gameState.get("kingCouncil").charAt(i);
			buff += whichColorForHandAndCouncil(c);
		}
		logger.severe("King council: 	  " + buff);
	}

	/**
	 * This method prints the properties of the hand of the player
	 * 
	 * @param gameState
	 */
	private static void printPoliticalCards(HashMap<String, String> gameState) {
		String buff = new String();
		char c;
		for (int i = 0; i != gameState.get("hand").length(); i++) {
			c = gameState.get("hand").charAt(i);
			buff += whichColorForHandAndCouncil(c);
		}
		logger.severe("Your hand: " + buff);
	}

	/**
	 * This method prints the properties of the cities
	 * 
	 * @param gameState
	 */
	private static void printBonusOfTheCities(HashMap<String, String> gameState) {
		String key;
		String row = "";
		for (int i = 0; i != Integer.parseInt(gameState.get("numberOfCities")); i++) {
			key = "city" + (char) (i + 65) + BONUS;

			row += "City " + whichColorForCity(gameState, (char) (i + 65)) + " Bonus: " + gameState.get(key) + "	";
			if ((i + 1) % 5 == 0) {
				logger.severe(row);
				row = "";
			}

		}
	}

	/**
	 * This method prints the permitCard of the player
	 * 
	 * @param gameState
	 */
	private static void printPermitCardsOfThePlayer(HashMap<String, String> gameState) {
		for (int i = 0; i != Integer.parseInt(gameState.get("numberOfPlayers")); i++)
			if (gameState.get(PLAYER_NAME_LOWERCASE + Integer.toString(i) + PERMITCARDS) != null
					&& !"0".equals(gameState.get(PLAYER_NAME_LOWERCASE + Integer.toString(i) + PERMITCARDS))) {
				logger.severe("Player " + Integer.toString(i + 1) + " has "
						+ gameState.get(PLAYER_NAME_LOWERCASE + Integer.toString(i) + PERMITCARDS) + " PermitCard: ");
				for (int j = 0; j != Integer
						.parseInt(gameState.get(PLAYER_NAME_LOWERCASE + Integer.toString(i) + PERMITCARDS)); j++)
					logger.severe("PermitCard " + Integer.toString(j + 1) + " Cities: "
							+ gameState.get(PLAYER_NAME_LOWERCASE + Integer.toString(i) + PERMITCARD
									+ Integer.toString(j) + "Cities")
							+ " Bonus: "
							+ gameState.get(PLAYER_NAME_LOWERCASE + Integer.toString(i) + PERMITCARD
									+ Integer.toString(j) + BONUS)
							+ " Used: " + gameState.get(PLAYER_NAME_LOWERCASE + Integer.toString(i) + PERMITCARD
									+ Integer.toString(j) + "Used"));
			} else
				logger.severe("Player " + Integer.toString(i + 1) + " has 0 PermitCard");
	}

	/**
	 * This method prints all the offer in the market
	 * 
	 * @param gameState
	 */
	private static void printMarket(HashMap<String, String> gameState) {
		logger.severe("There are " + gameState.get(NUMBER_OF_OFFER) + " Offer in the Market:");
		for (int i = 0; i != Integer.parseInt(gameState.get(NUMBER_OF_OFFER)); i++) {
			if (!client.getUsername().equals(gameState.get(OFFER + Integer.toString(i) + PLAYER_NAME_UPPERCASE))) {
				logger.severe("Offer N°" + Integer.toString(i + 1) + " of the Player: "
						+ gameState.get(OFFER + Integer.toString(i) + PLAYER_NAME_UPPERCASE));
				if (gameState.get(OFFER + Integer.toString(i) + "PoliticalCard") != null) {
					logger.severe("PoliticalCard " + gameState.get(OFFER + Integer.toString(i) + "PoliticalCard"));
				}
				if (gameState.get(OFFER + Integer.toString(i) + PERMITCARD) != null) {
					logger.severe(
							"PermitCard  Cities " + gameState.get(OFFER + Integer.toString(i) + PERMITCARD + "Cities")
									+ " Bonus " + gameState.get(OFFER + Integer.toString(i) + PERMITCARD + BONUS)
									+ " Used " + gameState.get(OFFER + Integer.toString(i) + PERMITCARD + "Used"));
				}
				if (gameState.get(OFFER + Integer.toString(i) + HELPERS) != null)
					logger.severe("Helpers :" + gameState.get(OFFER + Integer.toString(i) + HELPERS));
				logger.severe("Price :" + gameState.get(OFFER + Integer.toString(i) + "Price") + " Money");
			}
		}
	}

	/**
	 * This method transform a char into a colored string
	 * 
	 * @param c
	 * @return a colored string of the char
	 */
	private static String whichColorForHandAndCouncil(char c) {
		String buff;
		switch (c) {

		case 'w':
			buff = ANSI_WHITE + "w" + ANSI_RESET;
			break;
		case 'o':
			buff = ANSI_RED + "o" + ANSI_RESET;
			break;
		case 'p':
			buff = ANSI_PURPLE + "p" + ANSI_RESET;
			break;
		case 'b':
			buff = ANSI_BLUE + "b" + ANSI_RESET;
			break;
		case 'k':
			buff = ANSI_BLACK + "k" + ANSI_RESET;
			break;
		default:
			buff = "x";
		}
		return buff;
	}

	/**
	 * This method transforms a character into a colored string with the right
	 * color for the city
	 * 
	 * @param gameState
	 * @param city
	 * @return
	 */
	private static String whichColorForCity(HashMap<String, String> gameState, char city) {
		String buff;
		String color = gameState.get("city" + city + "Color");

		switch (color) {

		case "yellow":
			buff = ANSI_YELLOW + city + ANSI_RESET;
			break;
		case "orange":
			buff = ANSI_RED + city + ANSI_RESET;
			break;
		case "grey":
			buff = ANSI_CYAN + city + ANSI_RESET;
			break;
		case "blue":
			buff = ANSI_BLUE + city + ANSI_RESET;
			break;
		default:
			buff = ANSI_PURPLE + city + ANSI_RESET;
		}
		return buff;
	}

	/**
	 * This method tells if the character is between A and Z
	 * 
	 * @param c
	 * @return true if it is, false otherwise
	 */
	private static boolean isACity(char c) {
		return (int) (c) < 80 && (int) (c) > 64;
	}

	/**
	 * This method poll the input of a player
	 * 
	 * @throws TurnOverException
	 */
	private static void pollInput() throws TurnOverException {

		try {
			while (!bufferedReader.ready()) {
				if (client.hasUpdates())
					throw new TurnOverException();
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					logger.log(Level.WARNING, "Interrupted!", e);
					Thread.currentThread().interrupt();
				}

			}
		} catch (IOException e) {
			logger.log(Level.WARNING, "IOException", e);
		}
	}

	/**
	 * This method reads a line for the game menu, checks that it's not empty
	 * and that the turn is not over and returns the line as a String
	 * 
	 * @return returns a new non-empty string to the gaming menu
	 * @throws TurnOverException
	 *             in case the user tries to enter an input after his turn has
	 *             ended
	 */
	private static String readGameMenuString() throws TurnOverException {
		boolean isValid = false;
		String string = new String();
		while (!isValid) {
			pollInput();
			string = scanner.nextLine();
			if (string != "\n") {
				isValid = true;
			} else {
				logger.severe("NOPE!");
			}

		}

		return string;

	}

	/**
	 * This method reads a line for the game menu, checks that it's not empty
	 * and that the turn is not over and returns an integer parsed from the line
	 * 
	 * 
	 * @return returns the number read from console
	 * @throws TurnOverException
	 *             in case the user tries to enter an input after his turn has
	 *             ended
	 */
	private static int readGameMenuInt() throws TurnOverException {
		boolean isValid = false;
		int number = 0;
		while (!isValid) {
			try {
				pollInput();
				number = Integer.parseInt(scanner.nextLine());
				isValid = true;
			} catch (NumberFormatException e) {
				logger.log(Level.INFO, "Invalid input", e);
			}
		}
		return number;
	}

	/**
	 * Its like readGameMenuInt but in this method dont poll the player
	 * 
	 * @return
	 */
	private static String readMenuString() {
		boolean isValid = false;
		String readString = new String();
		while (!isValid) {

			readString = scanner.nextLine();
			if (!"".equals(readString)) {
				isValid = true;
			}

		}
		return readString;
	}

	/**
	 * Reads the input for a valid integer
	 * 
	 * @return
	 */
	private static int readMenuInt() {
		boolean isValid = false;
		int number = 0;
		while (!isValid) {
			try {
				number = Integer.parseInt(scanner.nextLine());
				isValid = true;
			} catch (NumberFormatException e) {
				logger.log(Level.INFO, "Invalid input", e);
			}
		}
		return number;
	}

	/**
	 * Simple method for printing Chose which Council etc...
	 */
	private static void printChoseWhichCouncil() {
		logger.severe("Chose which Council:");
		logger.severe("1- Sea council");
		logger.severe("2- Hill council");
		logger.severe("3- Mountain council");
	}

	/**
	 * Convert int into String of the name of the region
	 * 
	 * @param number
	 *            the region
	 * @return the name of the region
	 */
	private static String intToRegionName(int number) {
		String regionString = new String();
		switch (number) {
		case 1:
			regionString = "sea";
			break;
		case 2:
			regionString = "hill";
			break;
		case 3:
			regionString = "mountain";
			break;
		case 4:
			regionString = "king";
			break;
		default:
		}
		return regionString;
	}

	/**
	 * Simple method for printing yes or no
	 */
	private static void printYesOrNo() {
		logger.severe("1- Yes");
		logger.severe("2- No");
	}

	/**
	 * Simply check if the selection is one or two
	 * 
	 * is the number to check
	 * 
	 * @throws TurnOverException
	 *             if the timeout of the player is called
	 */
	private static int checkOneOrTwo() throws TurnOverException {
		int decision = readGameMenuInt();
		while (decision != 1 && decision != 2) {
			logger.severe("Invalid input, try again");
			decision = readGameMenuInt();
		}
		return decision;
	}

	/**
	 * This method check when you slide a council that the input is valid
	 * 
	 * @return a String with the valid input
	 * @throws TurnOverException
	 *             if the timeout of the player is called
	 */
	private static String checkPoliticalCard() throws TurnOverException {
		String decision = new String();
		boolean correct = false;
		while (!correct) {
			decision = readGameMenuString();
			switch (decision) {
			case "k":
				correct = true;
				break;
			case "o":
				correct = true;
				break;
			case "b":
				correct = true;
				break;
			case "w":
				correct = true;
				break;
			case "p":
				correct = true;
				break;
			default:
				logger.severe("Invalid input, try again");
			}
		}
		return decision;
	}
}
