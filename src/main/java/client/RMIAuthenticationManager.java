package client;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author B
 *
 */
public interface RMIAuthenticationManager extends Remote {

	/**
	 * Logs into the server and returns a new RMIUserHandler object so that the
	 * client has access to its methods
	 * 
	 * @param username
	 * @param password
	 * @return an RMIUserHandler object, or null if the authentication failed
	 * @throws RemoteException
	 *             if there are errors in the remote implementation
	 */
	public RMIUserHandler login(String username, String password) throws RemoteException;

	/**
	 * Registers a new user on the server and returns a new RMIUserHandler
	 * object so that the client has access to its methods
	 * 
	 * @param username
	 * @param password
	 * @return an RMIUserHandler object, or null if the authentication failed
	 * @throws RemoteException
	 *             if there are errors in the remote implementation
	 */
	public RMIUserHandler register(String username, String password) throws RemoteException;

}
