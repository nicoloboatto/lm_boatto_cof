package client;

/**
 * These are all commands used by the socket listener to perform actions on the
 * server
 * 
 * 
 * @author B
 *
 */
public class Commands {

	public static final String PUT_OFFER = "putOffer";
	public static final String SKIP_ACTION = "skipAction";
	public static final String PICK_PERMIT = "pick";
	public static final String GET_PERMIT_BONUS = "getPermitCardBonus";
	public static final String GET_EMPORIUM_BONUS = "getEmporiumBonus";
	public static final String BUY_MAIN_ACTION = "buyMainAction";
	public static final String BUY_PERMIT = "buyPermit";
	public static final String BUY_KING = "buyKing";
	public static final String SLIDE_COUNCIL = "slideCouncil";
	public static final String PUT_EMPORIUM = "putEmporium";
	public static final String BUY_HELPER = "buyHelper";
	public static final String REFRESH_PERMITS = "refreshPermits";
	public static final String ADD_COUNCIL_MEMBER = "addCouncilMember";
	public static final String BUY_OFFER = "buyOffer";
	public static final String START_GAME = "startGame";
	public static final String SET_CONFIGURATION = "setConfiguration";

	/**
	 * Empty private constructor to hide default public one
	 */
	private Commands() {

	}
}
