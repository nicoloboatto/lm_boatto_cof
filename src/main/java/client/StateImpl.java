package client;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.logging.Logger;

import server.State;

/**
 * @author B
 *
 */
public class StateImpl extends UnicastRemoteObject implements State {

	private static final Logger logger = Logger.getLogger(StateImpl.class.getName());
	/**
	 * 
	 */
	private static final long serialVersionUID = -4148587560664409065L;
	private ArrayList<String> usernames;
	private transient RMIClient client;
	private transient HashMap<String, String> gameState = new HashMap<>();
	private boolean hasUpdates = false;

	/**
	 * 
	 * @param client
	 *            the client to which the state belongs
	 * @throws RemoteException
	 *             if there are errors in the remote registration
	 */
	public StateImpl(RMIClient client) throws RemoteException {
		this.client = client;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.State#addUsername(java.lang.String)
	 */
	@Override
	public void addUser(String username) throws RemoteException {

		Lock lock = client.getLock();
		lock.lock();
		this.usernames.add(username);
		hasUpdates = true;
		client.getUpdated().signalAll();
		lock.unlock();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.State#removeUser(java.lang.String)
	 */
	@Override
	public void removeUser(String username) throws RemoteException {

		Lock lock = client.getLock();
		lock.lock();
		this.usernames.remove(username);
		hasUpdates = true;
		client.getUpdated().signalAll();
		lock.unlock();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.State#setLobby(server.LobbyImpl)
	 */
	@Override
	public void setLobby(RemoteLobby remoteLobby) throws RemoteException {
		client.setLobby(remoteLobby);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.LobbyState#gameStarted()
	 */
	@Override
	public void setPlayer(RemotePlayer player) throws RemoteException {
		logger.info("Entered state.setPlayer");
		client.setPlayer(player);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.State#update(client.GameState)
	 */
	@Override
	public void update(HashMap<String, String> stateUpdate) {
		Lock lock = client.getLock();
		lock.lock();
		hasUpdates = true;
		client.getUpdated().signalAll();
		lock.unlock();

	}

	/**
	 * 
	 * @return true if there are new updates, false otherwise
	 */
	public boolean hasUpdates() {
		return hasUpdates;
	}

	/**
	 * Sets the hasUpdates flag in the state
	 * 
	 */
	public void hasUpdates(boolean newValue) {
		hasUpdates = newValue;
	}

	/**
	 * 
	 * @return the current gameState from the remote object
	 */
	public HashMap<String, String> getGameState() {
		hasUpdates = false;
		return this.gameState;
	}

	/**
	 * 
	 * @return true if the player is moving, false otherwise
	 */
	public boolean isMoving() {
		int moving = Integer.parseInt(gameState.get("moving"));
		String movingUsername = gameState.get("player" + moving + "Username");
		return client.getUsername().equals(movingUsername);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see server.State#alive()
	 */
	@Override
	public void alive() throws RemoteException {
		return;

	}

}
