package client;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;


public class CustomConsoleFormatter extends Formatter {

	/**
	 * Formats console to a simple output and adds a newline at the end of each
	 * print
	 * 
	 */
	@Override
	public String format(LogRecord record) {
		return record.getMessage() + "\n";
	}

}
