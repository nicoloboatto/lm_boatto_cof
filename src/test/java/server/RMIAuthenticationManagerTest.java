package server;

import static org.junit.Assert.*;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import client.NetworkException;
import client.RMIClient;

/**
 * @author B
 *
 */
public class RMIAuthenticationManagerTest {

	private static final Logger logger = Logger.getLogger(RMIAuthenticationManagerTest.class.getName());

	/**
	 * Starts a new server on which the authenticationManager runs
	 * 
	 * @throws IOException
	 *             in case there are problems setting up the server
	 */
	@BeforeClass
	public static void setUp() throws IOException {
		GameServer.setup(6400, 1099, 20);

	}

	/**
	 * Shuts down the server and removes the test users from the database
	 * 
	 * 
	 */
	@AfterClass
	public static void cleanUp() {
		try {
			DatabaseManager.removeUser("user");
			DatabaseManager.removeUser("alsoUser");

			GameServer.shutdown();
		} catch (SQLException e) {
			logger.log(Level.INFO, "Test user was already deleted, you never know", e);
		}

	}

	/**
	 * Tries to login onto the server
	 * 
	 * Test method for
	 * {@link server.RMIAuthenticationManagerImpl#login(java.lang.String, java.lang.String)}
	 * .
	 * 
	 * @throws Exception
	 *             in case there are problems with the test
	 */
	@Test
	public void testLogin() throws Exception {
		try {
			DatabaseManager.addUser("user", "test");
		} catch (SQLException e) {
			logger.info("Test user already exists");
		}
		RMIClient client = new RMIClient("127.0.0.1", 1099);
		assertEquals(true, client.login("user", "test"));
		assertEquals(false, client.login("noUser", "test"));
	}

	/**
	 * Tries to register a new user on the server
	 * 
	 * Test method for
	 * {@link server.RMIAuthenticationManagerImpl#register(java.lang.String, java.lang.String)}
	 * 
	 * @throws Exception
	 *             in case there are problems with the test
	 */
	@Test
	public void testRegister() throws Exception {

		RMIClient client = new RMIClient("127.0.0.1", 1099);
		assertEquals(true, client.register("alsoUser", "test"));
		assertEquals(false, client.register("user", "test"));
		DatabaseManager.removeUser("alsoUser");

	}

}
