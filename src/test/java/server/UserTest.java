package server;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class UserTest {
	
	User testUser;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {	
		testUser = new User("user");
	}
	

	/**
	 * Test method for {@link server.User#getPlayedMatches()}.
	 */
	@Test
	public void testGetPlayedMatches() {
		assertEquals(0, testUser.getPlayedMatches());
	}

	/**
	 * Test method for {@link server.User#getWonMatches()}.
	 */
	@Test
	public void testGetWonMatches() {
		assertEquals(0, testUser.getWonMatches());
	}

	/**
	 * Test method for {@link server.User#getConfigurations()}.
	 */
	@Test
	public void testGetConfigurations() {
		assertNull(testUser.getConfigurations());
	}

}
