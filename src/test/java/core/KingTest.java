package core;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Giorgio
 *
 */
public class KingTest {

	/**
	 * Test method for {@link core.King#getLocation()}.
	 * 
	 * This test shows that the getLocation of the King works
	 */
	@Test
	public void testGetLocation() {
		City city = new City(0, 'a', null, null, null);
		King king = new King(city);
		assertEquals(city, king.getLocation());
	}

	/**
	 * Test method for {@link core.King#getCouncil()}.
	 * 
	 * This test shows that the getCouncil of the King works
	 */
	@Test
	public void testGetCouncil() {
		City city = new City(0, 'a', null, null, null);
		King king = new King(city);
		king.getCouncil().setRegion(null);
		assertEquals(null, king.getCouncil().getRegion());
	}

	/**
	 * Test method for {@link core.King#move(core.City)}.
	 * 
	 * This test shows that the method move of the King works
	 */
	@Test
	public void testMove() {
		City first = new City(0, 'a', null, null, null);
		City second = new City(0, 'b', null, null, null);
		King king = new King(first);
		king.move(second);
		assertEquals(second, king.getLocation());
	}

}
