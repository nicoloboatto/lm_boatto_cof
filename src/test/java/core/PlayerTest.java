package core;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Test;

import server.UserHandler;

/**
 * @author Giorgio
 *
 */
public class PlayerTest {

	private static final Logger logger = Logger.getLogger(Map.class.getName());
	private static Properties prop = new Properties();

	@BeforeClass
	public static void setUp() {
		try (FileInputStream input = new FileInputStream("server/GameConfigurations/default1.properties")) {
			// load a properties file
			prop.load(input);
		} catch (IOException ex) {
			logger.log(Level.WARNING, "File not found!", ex);
		}
	}

	/**
	 * Test method for {@link core.PlayerImpl#PlayerImpl(int)}
	 * 
	 * This method shows that the constructor of the player works properly
	 * 
	 * @throws RemoteException
	 */
	@Test
	public void testPlayer() throws RemoteException {
		PlayerImpl playerImpl = new PlayerImpl(1);
		assertEquals(6, playerImpl.getPoliticalCards().size());
	}

	/**
	 * Test method for
	 * {@link core.PlayerImpl#addAMemberOfTheCouncil(String, char)}
	 * 
	 * This test shows that the quick action that add a member of the council
	 * works
	 */
	@Test
	public void testPlayerAddAMemberOfTheCouncil() {
		UserHandler[] players = new UserHandler[1];
		Game game = new Game(players, prop);
		char orange = 'o';
		game.getPlayers()[0].addAMemberOfTheCouncil(game.getMap().getCouncil()[0].getRegion().getName(), orange);
		assertEquals(orange, game.getMap().getCouncil()[0].getColors()[0]);
		assertEquals(0, game.getPlayers()[0].getHelpers());
	}

	/**
	 * Test method for {@link core.PlayerImpl#buyHelper()}
	 * 
	 * This test shows that the quick action that buy an helper works
	 * 
	 * @throws RemoteException
	 */
	@Test
	public void testBuyHelpers() throws RemoteException {
		UserHandler[] players = new UserHandler[1];
		Game game = new Game(players, prop);
		PlayerImpl playerImpl = new PlayerImpl(0);
		playerImpl.setGame(game);
		playerImpl.startTurn();
		playerImpl.buyHelper();
		assertEquals(2, playerImpl.getHelpers());
	}

	/**
	 * Test method for {@link core.PlayerImpl#buyPermitCard(String,int, char[])}
	 * 
	 * This test shows that the main action that add a permitCard from the
	 * council works. The player gain a bonus of 100 politicalCard and he gives
	 * 4 of them for the council to buy the permitCard. Check that the
	 * permitCard is bought properly
	 */
	@Test
	public void testBuyPermitCard() {
		UserHandler[] players = new UserHandler[1];
		Game game = new Game(players, prop);
		int[] bonusTest = { 10, 10, 10, 0, 0, 0, 0, 0, 0 };
		int[] bonus = { 0, 0, 0, 100, 0, 0, 0, 0, 0 };
		Bonus pickACard = new Bonus(bonus);
		Bonus tenOfMoneyHelpersVictory = new Bonus(bonusTest);
		game.getPlayers()[0].activateBonus(pickACard);
		game.getPlayers()[0].activateBonus(tenOfMoneyHelpersVictory);
		game.getPlayers()[0].buyPermitCard(game.getMap().getCouncil()[0].getRegion().getName(), 0,
				game.getMap().getCouncil()[0].getColors());
		assertEquals(102 + game.getPlayers()[0].getPermitCard().get(0).getBonus().getPoliticalBonus(),
				game.getPlayers()[0].getPoliticalCards().size());
	}

	/**
	 * Test method for {@link core.PlayerImpl#buyKing(char[], char)}
	 * 
	 * This test shows that the main action that buy the king works. The player
	 * gain a bonus of 100 politicalCard and he gives 4 of them for the council
	 * of the king to buy him. Check if the emporium is properly gain by the
	 * player
	 */
	@Test
	public void testBuyKing() {
		UserHandler[] players = new UserHandler[1];
		Game game = new Game(players, prop);
		int[] bonusTest = { 10, 10, 10, 0, 0, 0, 0, 0, 0 };
		int[] bonus = { 0, 0, 0, 100, 0, 0, 0, 0, 0 };
		Bonus pickACard = new Bonus(bonus);
		Bonus tenOfMoneyHelpersVictory = new Bonus(bonusTest);
		game.getPlayers()[0].activateBonus(pickACard);
		game.getPlayers()[0].activateBonus(tenOfMoneyHelpersVictory);
		game.getPlayers()[0].setGame(game);
		game.getPlayers()[0].buyKing(game.getMap().getKing().getCouncil().getColors(),
				game.getMap().getCity()[3].getCharOfTheCity());
		assertEquals(102, game.getPlayers()[0].getPoliticalCards().size());
		assertEquals(game.getPlayers()[0].getEmporiums().get(0), game.getMap().getCity()[3]);
		assertEquals(18, game.getPlayers()[0].getMoney());
		game.getPlayers()[0].buyKing(game.getMap().getKing().getCouncil().getColors(),
				game.getMap().getKing().getLocation().getCharOfTheCity());
		assertEquals(18, game.getPlayers()[0].getMoney());
	}

	/**
	 * Test method for {@link core.PlayerImpl#buyKing(char[], char)}
	 * 
	 * This test shows that the main action that buy the king works. The player
	 * gain a bonus of 100 politicalCard and he gives 4 of them for the council
	 * of the king to buy him. Check if the emporium is properly gain by the
	 * player
	 */
	@Test
	public void testBuyKing2() {
		UserHandler[] players = new UserHandler[1];
		Game game = new Game(players, prop);
		int[] bonusTest = { 10, 10, 10, 0, 0, 0, 0, 0, 0 };
		int[] bonus = { 0, 0, 0, 100, 0, 0, 0, 0, 0 };
		Bonus pickACard = new Bonus(bonus);
		Bonus tenOfMoneyHelpersVictory = new Bonus(bonusTest);
		game.getPlayers()[0].activateBonus(pickACard);
		game.getPlayers()[0].activateBonus(tenOfMoneyHelpersVictory);
		game.getPlayers()[0].setGame(game);
		char[] colors = game.getMap().getKing().getCouncil().getColors();
		char x = 'x';
		colors[3] = x;
		game.getPlayers()[0].buyKing(game.getMap().getKing().getCouncil().getColors(),
				game.getMap().getKing().getLocation().getCharOfTheCity());
		assertEquals(102, game.getPlayers()[0].getPoliticalCards().size());
		assertEquals(game.getPlayers()[0].getEmporiums().get(0), game.getMap().getKing().getLocation());
		assertEquals(19, game.getPlayers()[0].getMoney());
	}

	/**
	 * Test method for {@link core.PlayerImpl#slideCouncil(char, String)}
	 * 
	 * This test shows that the main action that slide a council works. The
	 * player slide a council and check if he gain the money
	 */
	@Test
	public void testSlideCouncil() {
		UserHandler[] players = new UserHandler[1];
		Game game = new Game(players, prop);
		game.getPlayers()[0].slideCouncil('w', game.getMap().getCouncil()[0].getRegion().getName());
		assertEquals(14, game.getPlayers()[0].getMoney());
	}

	/**
	 * Test method for
	 * {@link core.PlayerImpl#activateBonusOfTheCitiesNear(City)}
	 * 
	 * This test shows that the bonuses of the cities are gained properly when
	 * the player builds a new emprium close to other emporium that the player
	 * owns
	 */
	@Test
	public void testActivateBonusOfTheCitiesNear() {
		UserHandler[] players = new UserHandler[1];
		Game game = new Game(players, prop);
		int[] bonusTest = { 10, 10, 10, 0, 0, 0, 0, 0, 0 };
		int[] bonus = { 0, 0, 0, 100, 0, 0, 0, 0, 0 };
		Bonus pickACard = new Bonus(bonus);
		Bonus tenOfMoneyHelpersVictory = new Bonus(bonusTest);
		game.getPlayers()[0].activateBonus(pickACard);
		game.getPlayers()[0].activateBonus(tenOfMoneyHelpersVictory);
		game.getPlayers()[0].setEmporium(game.getMap().getCity()[0]);
		game.getPlayers()[0].setEmporium(game.getMap().getCity()[10]);
		game.getPlayers()[0].setEmporium(game.getMap().getCity()[12]);
		game.getPlayers()[0].activateBonusOfTheCitiesNear(game.getMap().getCity()[2]);
		assertEquals(1, game.getPlayers()[0].getNobility());
		assertEquals(12, game.getPlayers()[0].getVictory());
		game.getPlayers()[0].setEmporium(game.getMap().getCity()[3]);
		game.getPlayers()[0].activateBonusOfTheCitiesNear(game.getMap().getCity()[2]);
		assertEquals(3, game.getPlayers()[0].getNobility());
		assertEquals(14, game.getPlayers()[0].getVictory());
	}

	/**
	 * Test method for {@link core.PlayerImpl#slideCouncil(char, String)}
	 * 
	 * This test that also the council of the king works with the main action
	 * slideCouncil
	 */
	@Test
	public void testSlideTheCouncilOfTheKing() {
		char k = 'k';
		String king = "king";
		UserHandler[] players = new UserHandler[1];
		Game game = new Game(players, prop);
		game.getPlayers()[0].slideCouncil(k, king);
		assertEquals('k', game.getMap().getKing().getCouncil().getColors()[0]);
	}

	/**
	 * Test method for {@link core.PlayerImpl#pickPermitCard(String, int)} Test
	 * method for {@link core.PlayerImpl#getPermitCardBonus(int)} Test method
	 * for {@link core.PlayerImpl#getBonusOfAnEmporium(char)}
	 * 
	 * This are tests for the special action gained by the nobility level.
	 * Checks that all the special action works properly
	 */
	@Test
	public void testSpecialAction() {
		String sea = "sea";
		UserHandler[] players = new UserHandler[1];
		Game game = new Game(players, prop);
		game.getPlayers()[0].pickPermitCard(sea, 0);
		game.getPlayers()[0].getPermitCardBonus(0);
		game.getPlayers()[0].putEmporiumInACity(0, game.getPlayers()[0].getPermitCard().get(0).getCities().charAt(0));
		game.getPlayers()[0].getBonusOfAnEmporium(game.getPlayers()[0].getPermitCard().get(0).getCities().charAt(0));
		assertEquals(game.getPlayers()[0].getEmporiums().get(0),
				game.getMap().searchCityByChar(game.getPlayers()[0].getPermitCard().get(0).getCities().charAt(0)));
	}

	/**
	 * Test method for {@link core.PlayerImpl#refreshPermitCards(String)}
	 * 
	 * This test shows that the quick action that refresh the permitCards of a
	 * council works
	 */
	@Test
	public void testRefreshPermitCards() {
		UserHandler[] players = new UserHandler[1];
		Game game = new Game(players, prop);
		game.getPlayers()[0].refreshPermitCards("sea");
		assertEquals(0, game.getPlayers()[0].getHelpers());
	}

	/**
	 * Test method for {@link core.PlayerImpl#buyMainAction()}
	 * 
	 * This test shows that the fourth quick action of the players works
	 * properly
	 */
	@Test
	public void testBuyMainAction() {
		UserHandler[] players = new UserHandler[4];
		Game game = new Game(players, prop);
		game.getPlayers()[3].buyMainAction();
		assertEquals(4, game.getPlayers()[3].getHelpers());
	}

	/**
	 * Test method for {@link core.PlayerImpl#skipAction()}
	 * 
	 * This test shows that the skip quick action of the players works properly
	 */
	@Test
	public void testSkipAction() {
		UserHandler[] players = new UserHandler[4];
		Game game = new Game(players, prop);
		try {
			game.getPlayers()[3].skipAction();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(0, game.getPlayers()[3].getQuickAction());
	}

	/**
	 * Test method for {@link core.PlayerImpl#getGame()}
	 * 
	 * This test shows that the getGame works properly
	 */
	@Test
	public void testGetGame() {
		UserHandler[] players = new UserHandler[4];
		Game game = new Game(players, prop);
		assertEquals(game, game.getPlayers()[0].getGame());
	}

	/**
	 * Test method for {@link core.PlayerImpl#hasAction()}
	 * 
	 * This test shows that the hasAction works properly
	 */
	@Test
	public void testHasAction() {
		UserHandler[] players = new UserHandler[4];
		Game game = new Game(players, prop);
		assertEquals(true, game.getPlayers()[0].hasAction());
	}

	/**
	 * Test method for {@link core.PlayerImpl#calculatePossibleMoves()}
	 * 
	 * This method shows that the method that calculate the possible moves of a
	 * player works properly
	 */
	@Test
	public void testCalculatePossibleMoves() {
		UserHandler[] players = new UserHandler[4];
		Game game = new Game(players, prop);
		assertEquals('1', game.getPlayers()[0].calculatePossibleMoves().charAt(2));
	}

	/**
	 * Test method for {@link core.PlayerImpl#skipTurn()}
	 * 
	 * This method shows that skipTurn works properly
	 */
	@Test
	public void testSkipTurn() {
		UserHandler[] players = new UserHandler[4];
		Game game = new Game(players, prop);
		game.getPlayers()[0].skipTurn();
		assertEquals(0, game.getPlayers()[0].getMainAction());
		assertEquals(0, game.getPlayers()[0].getQuickAction());
	}

	/**
	 * Test method for {@link core.PlayerImpl#getHand()}
	 * 
	 * This method shows that getHand works properly
	 */
	@Test
	public void testGetHand() {
		UserHandler[] players = new UserHandler[4];
		Game game = new Game(players, prop);
		assertEquals(6, game.getPlayers()[0].getHand().length());
	}
}
