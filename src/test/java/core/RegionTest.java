package core;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * @author Giorgio
 *
 */
public class RegionTest {

	/**
	 * Test method for {@link core.Region#getCouncil()}. Test method for
	 * {@link core.Region#getBonus()}.
	 * 
	 * This test shows that checkCities and getCouncil and getBonus works
	 * properly, this test create some cities, a council and a region. Set the
	 * region of all tha cities and check if the region is the one expected
	 */
	@Test
	public void testCheckCities() {
		City A = new City(2, 'A', null, null, null);
		City B = new City(2, 'B', null, null, null);
		City C = new City(2, 'C', null, null, null);
		City D = new City(2, 'D', null, null, null);
		City E = new City(2, 'E', null, null, null);
		City[] check = new City[5];
		check[0] = A;
		check[1] = B;
		check[2] = C;
		check[3] = D;
		check[4] = E;
		ArrayList<City> city = new ArrayList<>();
		city.add(A);
		city.add(B);
		city.add(C);
		city.add(D);
		city.add(E);
		Council council = new Council();
		Region first = new Region("");
		first.setCities(check);
		first.setCouncil(council);
		assertEquals(true, first.checkCities(city));
		assertEquals(council, first.getCouncil());

	}

}
