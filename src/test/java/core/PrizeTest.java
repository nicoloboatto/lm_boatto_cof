package core;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Giorgio
 *
 */
public class PrizeTest {

	private static final Logger logger = Logger.getLogger(Map.class.getName());
	private static Properties prop = new Properties();

	@BeforeClass
	public static void setUp() {
		try (FileInputStream input = new FileInputStream("server/GameConfigurations/default1.properties")) {
			// load a properties file
			prop.load(input);
		} catch (IOException ex) {
			logger.log(Level.WARNING, "File not found!", ex);
		}
	}

	/**
	 * Test method for {@link core.Prize#Prize(java.util.Properties)}.
	 * 
	 * This test shows that the constructor of prize works. This test check that
	 * all the prize from the properties are right
	 */
	@Test
	public void testPrize() {
		Properties buff = new Properties();
		buff = formatProperties(prop);
		Prize prize = new Prize(buff);
		assertEquals(25, Integer.parseInt(buff.getProperty("prize0Bonus")));
		assertEquals(5, Integer.parseInt(buff.getProperty("prize4Bonus")));
		assertEquals(5, prize.getBonus().get(4).getVictoryBonus());
		assertEquals(25, prize.getBonus().get(0).getVictoryBonus());
	}

	public Properties formatProperties(Properties prop) {
		String key;
		String connection = "Connection";
		String bonus = "Bonus";
		String color = "Color";
		Properties buff = new Properties();
		for (int i = 0; i != Integer.parseInt(prop.getProperty("numberOfCities")); i++) {
			key = "city" + (char) (i + 65) + connection;
			buff.setProperty(key, prop.getProperty(key));
			key = "city" + (char) (i + 65) + bonus;
			buff.setProperty(key, prop.getProperty(key));
			key = "city" + (char) (i + 65) + color;
			buff.setProperty(key, prop.getProperty(key));
		}
		for (int i = 0; i != Integer.parseInt(prop.getProperty("numberOfNobilityLevel")); i++) {
			key = "nobility" + i + bonus;
			buff.setProperty(key, prop.getProperty(key));
		}
		for (int i = 0; i != Integer.parseInt(prop.getProperty("numberOfPrize")); i++) {
			key = "prize" + i + bonus;
			buff.setProperty(key, prop.getProperty(key));
		}
		key = "numberOfPrize";
		buff.setProperty(key, prop.getProperty(key));
		key = "numberOfNobilityLevel";
		buff.setProperty(key, prop.getProperty(key));
		key = "numberOfCities";
		buff.setProperty(key, prop.getProperty(key));
		return buff;
	}

}
