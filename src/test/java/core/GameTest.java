package core;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Test;
import server.UserHandler;

/**
 * @author Giorgio
 *
 */
public class GameTest {

	private static final Logger logger = Logger.getLogger(Map.class.getName());
	private static Properties prop = new Properties();

	@BeforeClass
	public static void setUp() {
		try (FileInputStream input = new FileInputStream("server/GameConfigurations/default1.properties")) {
			// load a properties file
			prop.load(input);
		} catch (IOException ex) {
			logger.log(Level.WARNING, "File not found!", ex);
		}
	}

	/**
	 * Test method for {@link core.Game#decideWinner()}.
	 * 
	 * This test shows that decideWinner works, in this particular test there
	 * are 3 players, one of them gain a bonus of 50 victory points and he
	 * should be the winner
	 */
	@Test
	public void testDecideWinner1() {
		UserHandler[] players = new UserHandler[3];
		Game game = new Game(players, prop);
		int[] bonusTest = { 0, 0, 50, 0, 0, 0, 0, 0, 0 };
		Bonus bonus = new Bonus(bonusTest);
		game.getPlayers()[2].activateBonus(bonus);
		game.decideWinner();
		assertEquals(game.getPlayers()[2], game.getWinner());
		assertEquals(false, game.getisOver());
	}

	/**
	 * Test method for {@link core.Game#decideWinner()}.
	 * 
	 * This test shows that decideWinner works, in this particular test there
	 * are 3 players, two of them gain a bonus of 50 victory points and the
	 * player with most helpers should be the winner (the last one because the
	 * last one by the rule should have most helpers)
	 */
	@Test
	public void testDecideWinner2() {
		UserHandler[] players = new UserHandler[3];
		Game game = new Game(players, prop);
		int[] bonusTest = { 0, 0, 50, 0, 0, 0, 0, 0, 0 };
		Bonus bonus = new Bonus(bonusTest);
		game.getPlayers()[2].activateBonus(bonus);
		game.getPlayers()[1].activateBonus(bonus);
		game.decideWinner();
		assertEquals(game.getPlayers()[2], game.getWinner());
	}

	/**
	 * Test method for {@link core.Game#decideWinner()}.
	 * 
	 * This test shows that decideWinner works, in this particular test there
	 * are 3 players, one of them gain a bonus of 5 nobility level and he should
	 * be the winner
	 */
	@Test
	public void testDecideWinner3() {
		UserHandler[] players = new UserHandler[3];
		int[] bonusTest1 = { 0, 0, 0, 0, 5, 0, 0, 0, 0 };
		int[] bonusTest2 = { 0, 0, 0, 0, 2, 0, 0, 0, 0 };
		Bonus bonus1 = new Bonus(bonusTest1);
		Bonus bonus2 = new Bonus(bonusTest2);
		Game game = new Game(players, prop);
		game.getPlayers()[2].activateBonus(bonus1);
		game.getPlayers()[1].activateBonus(bonus1);
		game.getPlayers()[0].activateBonus(bonus2);
		game.decideWinner();
		assertEquals(game.getPlayers()[2], game.getWinner());
	}

	/**
	 * Test method for {@link core.Game#decideWinner()}.
	 * 
	 * This test shows that decideWinner works, in this particular test there
	 * are 3 players, all of them gain a bonus of 5 nobility level and the first
	 * pick up a permitCard, so he should be the winner
	 */
	@Test
	public void testDecideWinner4() {
		UserHandler[] players = new UserHandler[3];
		Game game = new Game(players, prop);
		int[] bonusTest1 = { 0, 0, 0, 0, 5, 0, 0, 0, 0 };
		int[] bonusTest2 = { 0, 0, 0, 10, 0, 0, 0, 0, 0 };
		Bonus bonus1 = new Bonus(bonusTest1);
		Bonus bonus2 = new Bonus(bonusTest2);
		game.getPlayers()[2].activateBonus(bonus1);
		game.getPlayers()[1].activateBonus(bonus1);
		game.getPlayers()[0].activateBonus(bonus1);
		game.getPlayers()[0].activateBonus(bonus2);
		game.getPlayers()[0].pickPermitCard("sea", 0);
		game.decideWinner();
		assertEquals(game.getPlayers()[0], game.getWinner());
	}

	/**
	 * Test method for {@link core.Game#decideWinner()}.
	 * 
	 * This test shows that decideWinner works, in this particular test there
	 * are 3 players, one of them gain a bonus of 5 nobility level and two of
	 * them gain 2 nobility level, so decideWinner give to the one that have 5
	 * level 5 victory points and to the other that are same second 2 victory
	 * points
	 */
	@Test
	public void testDecideWinner5() {
		UserHandler[] players = new UserHandler[3];
		Game game = new Game(players, prop);
		int[] bonusTest1 = { 0, 0, 0, 0, 5, 0, 0, 0, 0 };
		int[] bonusTest2 = { 0, 0, 0, 0, 2, 0, 0, 0, 0 };
		Bonus bonus1 = new Bonus(bonusTest1);
		Bonus bonus2 = new Bonus(bonusTest2);
		game.getPlayers()[2].activateBonus(bonus1);
		game.getPlayers()[1].activateBonus(bonus2);
		game.getPlayers()[0].activateBonus(bonus2);
		game.decideWinner();
		assertEquals(game.getPlayers()[2], game.getWinner());
	}

	/**
	 * Test method for {@link core.Game#decideWinner()}.
	 * 
	 * This test shows that decideWinner works, in this particular test there
	 * are 2 players, both gain a bonus, so they have the same nobility level,
	 * helpers and money but the first have a politicalCard more than the
	 * second, so the first is the winner
	 */
	@Test
	public void testDecideWinner6() {
		UserHandler[] players = new UserHandler[2];
		Game game = new Game(players, prop);
		int[] bonusTest1 = { 0, 0, 0, 0, 5, 0, 0, 0, 0 };
		int[] bonusTest2 = { 1, 1, 0, 1, 0, 0, 0, 0, 0 };
		Bonus bonus1 = new Bonus(bonusTest1);
		Bonus bonus2 = new Bonus(bonusTest2);
		game.getPlayers()[1].activateBonus(bonus1);
		game.getPlayers()[0].activateBonus(bonus1);
		game.getPlayers()[0].activateBonus(bonus2);
		game.decideWinner();
		assertEquals(game.getPlayers()[0], game.getWinner());
	}

	/**
	 * Test method for {@link core.Game#decideWinner()}.
	 * 
	 * This test shows that decideWinner works, in this particular test there
	 * are 2 players, both equals in helpers, politicalCard and money and
	 * permitCard. In this case there is no winner, there is a draw
	 */
	@Test
	public void testDecideWinner7() {
		UserHandler[] players = new UserHandler[2];
		Game game = new Game(players, prop);
		int[] bonusTest2 = { 1, 1, 0, 0, 0, 0, 0, 0, 0 };
		Bonus bonus2 = new Bonus(bonusTest2);
		game.getPlayers()[0].activateBonus(bonus2);
		game.decideWinner();
		assertEquals(null, game.getWinner());
	}

	/**
	 * Test method for {@link core.Game#giveVictoryPointsByNobility()}.
	 * 
	 * This test shows that giveVictoryPointByNobility (with DoubleFirst) works,
	 * in this particular test there are 3 players, two of them gain a bonus of
	 * 10 nobility level and the the other one gain 9 nobility level, so to the
	 * firsts 5 victory points and to the second nothing
	 */
	@Test
	public void testGiveVictoryPointByNobilityDoubleFirst() {
		int[] bonusTest1 = { 0, 0, 0, 0, 10, 0, 0, 0, 0 };
		int[] bonusTest2 = { 0, 0, 0, 0, 9, 0, 0, 0, 0 };
		Bonus bonus1 = new Bonus(bonusTest1);
		Bonus bonus2 = new Bonus(bonusTest2);
		UserHandler[] players = new UserHandler[3];
		Game game = new Game(players, prop);
		game.getPlayers()[0].activateBonus(bonus1);
		game.getPlayers()[1].activateBonus(bonus1);
		game.getPlayers()[2].activateBonus(bonus2);
		game.giveVictoryPointsByNobility();
		assertEquals(5, game.getPlayers()[0].getVictory());
		assertEquals(5, game.getPlayers()[1].getVictory());
		assertEquals(0, game.getPlayers()[2].getVictory());
	}

	/**
	 * Test method for {@link core.Game#giveVictoryPointsByNobility()}.
	 * 
	 * This test shows that giveVictoryPointByNobility (with DoubleSecond)
	 * works, in this particular test there are 3 players, one of them gain a
	 * bonus of 10 nobility level and the the other two gain 9 nobility level,
	 * so to the firsts 5 victory points and to the second 2 victory points
	 */
	@Test
	public void testGiveVictoryPointByNobilityDoubleSecond() {
		int[] bonusTest1 = { 0, 0, 0, 0, 10, 0, 0, 0, 0 };
		int[] bonusTest2 = { 0, 0, 0, 0, 9, 0, 0, 0, 0 };
		Bonus bonus1 = new Bonus(bonusTest1);
		Bonus bonus2 = new Bonus(bonusTest2);
		UserHandler[] players = new UserHandler[3];
		Game game = new Game(players, prop);
		game.getPlayers()[0].activateBonus(bonus1);
		game.getPlayers()[1].activateBonus(bonus2);
		game.getPlayers()[2].activateBonus(bonus2);
		game.giveVictoryPointsByNobility();
		assertEquals(5, game.getPlayers()[0].getVictory());
		assertEquals(2, game.getPlayers()[1].getVictory());
		assertEquals(2, game.getPlayers()[2].getVictory());
	}

	/**
	 * Test method for {@link core.Game#giveVictoryPointsByNobility()}.
	 * 
	 * This test shows that giveVictoryPointByNobility (with 3First) works, in
	 * this particular test there are 3 players, all of them gain a bonus of 10
	 * nobility level, so all of them will gain 5 victory points
	 */
	@Test
	public void testGiveVictoryPointByNobility3First() {
		int[] bonusTest1 = { 0, 0, 0, 0, 10, 0, 0, 0, 0 };
		Bonus bonus = new Bonus(bonusTest1);
		UserHandler[] players = new UserHandler[3];
		Game game = new Game(players, prop);
		game.getPlayers()[0].activateBonus(bonus);
		game.getPlayers()[1].activateBonus(bonus);
		game.getPlayers()[2].activateBonus(bonus);
		game.giveVictoryPointsByNobility();
		assertEquals(5, game.getPlayers()[0].getVictory());
		assertEquals(5, game.getPlayers()[1].getVictory());
		assertEquals(5, game.getPlayers()[2].getVictory());
	}

	/**
	 * Test method for {@link core.Game#generateInitialGameState(Properties)}.
	 * 
	 * This test shows that there are all the council in the initial file
	 * properties generate by the game
	 */
	@Test
	public void testGenerateInitialGameState() {
		UserHandler[] players = new UserHandler[3];
		Game game = new Game(players, prop);
		HashMap<String, String> buff = game.generateInitialGameState(prop);
		assertEquals(String.copyValueOf(game.getMap().getKing().getCouncil().getColors()), buff.get("kingCouncil"));
		assertEquals(String.copyValueOf(game.getMap().getCouncil()[0].getColors()), buff.get("seaCouncil"));
		assertEquals(String.copyValueOf(game.getMap().getCouncil()[2].getColors()), buff.get("mountainCouncil"));
	}

}
