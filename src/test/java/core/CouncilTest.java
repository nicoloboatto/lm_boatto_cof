package core;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Test;

import server.UserHandler;

/**
 * @author Giorgio
 *
 */
public class CouncilTest {

	private static final Logger logger = Logger.getLogger(Map.class.getName());
	private static Properties prop = new Properties();

	@BeforeClass
	public static void setUp() {
		try (FileInputStream input = new FileInputStream("server/GameConfigurations/default1.properties")) {
			// load a properties file
			prop.load(input);
		} catch (IOException ex) {
			logger.log(Level.WARNING, "File not found!", ex);
		}
	}

	/**
	 * Test method for {@link core.Council#slideOn(char)}.
	 * 
	 * This important test shows that the third main action works
	 */
	@Test
	public void testSlideOn() {
		char b = 'b';
		Council first = new Council();
		first.slideOn(b);
		assertEquals('b', first.getColors()[0]);
	}

	/**
	 * Test method for {@link core.Council#refreshPermits()}.
	 * 
	 * This important test shows that the quick action that refresh the
	 * permitCard in a council works
	 */
	@Test
	public void testRefreshPermits() {
		UserHandler[] players = new UserHandler[3];
		Game game = new Game(players, prop);
		game.getMap().getCouncil()[0].refreshPermits();
		assertEquals("sea", game.getMap().getCouncil()[0].getRegion().getName());
	}

}
