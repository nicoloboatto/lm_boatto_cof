package core;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Test;

import server.UserHandler;

/**
 * @author Giorgio
 *
 */
public class NobilityTest {

	private static final Logger logger = Logger.getLogger(Map.class.getName());
	private static Properties prop = new Properties();

	@BeforeClass
	public static void setUp() {
		try (FileInputStream input = new FileInputStream("server/GameConfigurations/default1.properties")) {
			// load a properties file
			prop.load(input);
		} catch (IOException ex) {
			logger.log(Level.WARNING, "File not found!", ex);
		}
	}

	/**
	 * Test method for {@link core.Nobility#Nobility(core.Bonus[])}.
	 * 
	 * This test shows that the game generate properly the bonus of the nobility
	 */
	@Test
	public void testNobility() {
		UserHandler[] players = new UserHandler[3];
		Game game = new Game(players, prop);
		assertEquals(0, game.getMap().getNobility().getBonusOfTheLevel(0).getVictoryBonus());
		assertEquals(1, game.getMap().getNobility().getBonusOfTheLevel(4).getEmporiumNobilityBonus());
		assertEquals(1, game.getMap().getNobility().getBonusOfTheLevel(10).getPickPermitCardBonus());
		assertEquals(1, game.getMap().getNobility().getBonusOfTheLevel(14).getPermitCardBonus());
		assertEquals(2, game.getMap().getNobility().getBonusOfTheLevel(16).getEmporiumNobilityBonus());
		assertEquals(8, game.getMap().getNobility().getBonusOfTheLevel(18).getVictoryBonus());
		assertEquals(3, game.getMap().getNobility().getBonusOfTheLevel(20).getVictoryBonus());

	}

}
