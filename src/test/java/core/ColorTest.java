package core;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Test;

import server.UserHandler;

/**
 * @author Giorgio
 *
 */
public class ColorTest {

	private static final Logger logger = Logger.getLogger(Map.class.getName());
	private static Properties prop = new Properties();

	@BeforeClass
	public static void setUp() {
		try (FileInputStream input = new FileInputStream("server/GameConfigurations/default1.properties")) {
			// load a properties file
			prop.load(input);
		} catch (IOException ex) {
			logger.log(Level.WARNING, "File not found!", ex);
		}
	}

	/**
	 * Test method for {@link core.Color#getOccupied()}.
	 * 
	 * This method test that checkColor and getOccupied works
	 */
	@Test
	public void testCheckColor() {
		char a = 'a';
		char b = 'b';
		City first = new City(1, a, null, null, null);
		City second = new City(1, b, null, null, null);
		ArrayList<City> check = new ArrayList<>();
		check.add(first);
		check.add(second);
		Color color = new Color("blu", 2);
		City[] city = { first, second };
		color.setCities(city);
		first.setColor(color);
		second.setColor(color);
		assertEquals(false, color.getOccupied());
		assertEquals(true, color.checkColor(check));
		assertEquals(true, color.getOccupied());
		assertEquals(false, color.checkColor(check));

	}

	/**
	 * Test method for {@link core.Color#getName()}.
	 * 
	 * This method test that getColor works properly
	 */
	@Test
	public void testGetColor() {
		City first = new City(1, 'a', null, null, null);
		City[] check = new City[1];
		check[0] = first;
		Color color = new Color("blu", 1);
		color.setCities(check);
		first.setColor(color);
		assertEquals("blu", color.getName());
	}

	/**
	 * Test method for {@link core.Color#getBonus()}.
	 * 
	 * This method test that getBonus works properly
	 */
	@Test
	public void testGetBonus() {
		UserHandler[] players = new UserHandler[3];
		Game game = new Game(players, prop);
		assertEquals(20, game.getMap().getColor()[0].getBonus().getVictoryBonus());
	}

}
