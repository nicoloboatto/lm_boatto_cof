package core;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Test;

import server.UserHandler;

/**
 * @author Giorgio
 *
 */
public class MapTest {

	private static final Logger logger = Logger.getLogger(Map.class.getName());
	private static Properties prop = new Properties();

	@BeforeClass
	public static void setUp() {
		try (FileInputStream input = new FileInputStream("server/GameConfigurations/default1.properties")) {
			// load a properties file
			prop.load(input);
		} catch (IOException ex) {
			logger.log(Level.WARNING, "File not found!", ex);
		}
		try (FileInputStream input = new FileInputStream("server/GameConfigurations/default1.properties")) {
			// load a properties file
			prop.load(input);
		} catch (IOException ex) {
			logger.log(Level.WARNING, "File not found!", ex);
		}
	}

	/**
	 * Test method for {@link core.Map#getRegion()}.
	 * 
	 * This test shows that the constructor of Map works with all the length of
	 * the color, region and council
	 */
	@Test
	public void testMap() {
		UserHandler[] players = new UserHandler[3];
		Game game = new Game(players, prop);
		assertEquals(4, game.getMap().getColor().length);
		assertEquals(3, game.getMap().getRegion().length);
		assertEquals(3, game.getMap().getCouncil().length);
		assertEquals(game.getMap().getRegion()[0], game.getMap().getCouncil()[0].getRegion());
	}

	/**
	 * Test method for {@link core.Map#searchCityByChar(char)}.
	 * 
	 * This test shows that the searchCityByChar of Map works with all the
	 * cities in the properties
	 */
	@Test
	public void testSearchForChar() {
		UserHandler[] players = new UserHandler[3];
		Game game = new Game(players, prop);
		assertEquals(game.getMap().getCity()[0], game.getMap().searchCityByChar('A'));
		assertEquals(game.getMap().getCity()[1], game.getMap().searchCityByChar('B'));
		assertEquals(game.getMap().getCity()[2], game.getMap().searchCityByChar('C'));
		assertEquals(game.getMap().getCity()[3], game.getMap().searchCityByChar('D'));
		assertEquals(game.getMap().getCity()[4], game.getMap().searchCityByChar('E'));
		assertEquals(game.getMap().getCity()[5], game.getMap().searchCityByChar('F'));
		assertEquals(game.getMap().getCity()[6], game.getMap().searchCityByChar('G'));
		assertEquals(game.getMap().getCity()[7], game.getMap().searchCityByChar('H'));
		assertEquals(game.getMap().getCity()[8], game.getMap().searchCityByChar('I'));
		assertEquals(game.getMap().getCity()[9], game.getMap().searchCityByChar('J'));
		assertEquals(game.getMap().getCity()[10], game.getMap().searchCityByChar('K'));
		assertEquals(game.getMap().getCity()[11], game.getMap().searchCityByChar('L'));
		assertEquals(game.getMap().getCity()[12], game.getMap().searchCityByChar('M'));
		assertEquals(game.getMap().getCity()[13], game.getMap().searchCityByChar('N'));
		assertEquals(game.getMap().getCity()[14], game.getMap().searchCityByChar('O'));
	}

	/**
	 * Test method for {@link core.Map#setRegion(City)}.
	 * 
	 * This test shows that the setRegion of Map works with all the cities in
	 * the properties
	 */
	@Test
	public void testSetRegion() {
		UserHandler[] players = new UserHandler[3];
		Game game = new Game(players, prop);
		assertEquals(game.getMap().getCity()[0].getRegion(), game.getMap().getRegion()[0]);
		assertEquals(game.getMap().getCity()[1].getRegion(), game.getMap().getRegion()[0]);
		assertEquals(game.getMap().getCity()[2].getRegion(), game.getMap().getRegion()[0]);
		assertEquals(game.getMap().getCity()[3].getRegion(), game.getMap().getRegion()[0]);
		assertEquals(game.getMap().getCity()[4].getRegion(), game.getMap().getRegion()[0]);
		assertEquals(game.getMap().getCity()[5].getRegion(), game.getMap().getRegion()[1]);
		assertEquals(game.getMap().getCity()[6].getRegion(), game.getMap().getRegion()[1]);
		assertEquals(game.getMap().getCity()[7].getRegion(), game.getMap().getRegion()[1]);
		assertEquals(game.getMap().getCity()[8].getRegion(), game.getMap().getRegion()[1]);
		assertEquals(game.getMap().getCity()[9].getRegion(), game.getMap().getRegion()[1]);
		assertEquals(game.getMap().getCity()[10].getRegion(), game.getMap().getRegion()[2]);
		assertEquals(game.getMap().getCity()[11].getRegion(), game.getMap().getRegion()[2]);
		assertEquals(game.getMap().getCity()[12].getRegion(), game.getMap().getRegion()[2]);
		assertEquals(game.getMap().getCity()[13].getRegion(), game.getMap().getRegion()[2]);
		assertEquals(game.getMap().getCity()[14].getRegion(), game.getMap().getRegion()[2]);
	}

	/**
	 * Test method for {@link core.Map#generateDistanziatedGraphOfCities()}.
	 * 
	 * This test shows that the generateDistanziatedGraphOfCities of Map works
	 * with all the cities in the properties
	 */
	@Test
	public void testGenerateDistanziatedGraphOfCities() {
		UserHandler[] players = new UserHandler[3];
		Game game = new Game(players, prop);
		int[][] graph = game.getMap().generateDistanziatedGraphOfCities();
		for (int i = 0; i != graph.length; i++) {
			assertEquals(0, graph[i][i]);
		}
	}
}
