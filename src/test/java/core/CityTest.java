package core;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Giorgio
 *
 */
public class CityTest {

	/**
	 * Test method for {@link core.City#isConnectedTo(core.City)}.
	 * 
	 * This is a test for isConnectedTo, it shows that the first city is
	 * connected to the second and vice versa
	 */
	@Test
	public void testIsConnectedTo11() {
		City first = new City(1, 'a', null, null, null);
		City second = new City(1, 'b', null, null, null);
		first.setConnectedCity(second);
		second.setConnectedCity(first);
		assertEquals(first.isConnectedTo(second), second.isConnectedTo(first));
		assertEquals(second, first.getConnectedCities()[0]);
		assertEquals(first, second.getConnectedCities()[0]);
	}

	/**
	 * Test method for {@link core.City#getColor()}. Test method for
	 * {@link core.City#getCharOfTheCity()}. Test method for
	 * {@link core.City#getRegion()}. Test method for
	 * {@link core.City#getBonus()}.
	 * 
	 * This test shows that all the getSomething works
	 */
	@Test
	public void testSomeGet() {
		int[] bonusTest = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		Bonus bonus = new Bonus(bonusTest);
		City first = new City(1, 'a', null, null, bonus);
		assertEquals(null, first.getColor());
		assertEquals('a', first.getCharOfTheCity());
		assertEquals(null, first.getRegion());
		assertEquals(bonus, first.getBonus());
	}

}
