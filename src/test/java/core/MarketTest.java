package core;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Giorgio
 *
 */
public class MarketTest {

	/**
	 * Test method for {@link core.Market#addOffer(core.Offer)}.
	 * 
	 * This test shows that the addOffer works by generating a new offer and
	 * check if there are in the market
	 */
	@Test
	public void testAddOffer() {
		Market market = new Market();
		Offer offer = new Offer(null, null, null, 0, 0);
		market.addOffer(offer);
		assertEquals(true, market.getOffers().contains(offer));
	}

	/**
	 * Test method for {@link core.Market#removeOffer(core.Offer)}.
	 * 
	 * This test shows that the removeOffer works by generating a new offer and
	 * check if there are in the market and after remove that offer and check
	 * that there arent offer in the market
	 */
	@Test
	public void testRemoveOffer() {
		Market market = new Market();
		Offer offer = new Offer(null, null, null, 0, 0);
		market.addOffer(offer);
		assertEquals(true, market.getOffers().contains(offer));
		market.removeOffer(offer);
		assertEquals(false, market.getOffers().contains(offer));
	}

	/**
	 * Test method for {@link core.Market#getOfferCollected()}.
	 * 
	 * This test shows that the getOfferCollected works by generating a new
	 * offer and check if there are in the market and after call the done() and
	 * check the value of getOggerCollected
	 */
	@Test
	public void testGetOfferCollected() {
		Market market = new Market();
		Offer offer = new Offer(null, null, null, 0, 0);
		market.addOffer(offer);
		assertEquals(true, market.getOffers().contains(offer));
		assertEquals(false, market.getOfferCollected());
		market.done();
		assertEquals(true, market.getOfferCollected());
	}

}
