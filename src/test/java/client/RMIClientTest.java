package client;

import static org.junit.Assert.*;

import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import server.DatabaseManager;
import server.GameServer;

/**
 * @author B
 *
 */
public class RMIClientTest {

	private static final Logger logger = Logger.getLogger(RMIClientTest.class.getName());

	private static RMIClient client;
	private static RMIClient otherClient;

	/**
	 * 
	 * Sets up a GameServer and adds the users needed for testing
	 * 
	 * @throws IOException
	 *             in case starting the server fails
	 */
	@BeforeClass
	public static void setUp() throws IOException {
		GameServer.setup(6666);
		try {
			DatabaseManager.addUser("test", "test");
			DatabaseManager.addUser("alsoTest", "alsoTest");
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "test users already present", e);
		}
		client = new RMIClient("localhost", 1099);
		otherClient = new RMIClient("localhost", 1099);
	}

	/**
	 * Removes test users and shuts down the game server
	 * 
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDown() throws Exception {
		GameServer.shutdown();
		DatabaseManager.removeUser("test");
		DatabaseManager.removeUser("alsoTest");
	}

	/**
	 * 
	 * Makes sure the creation of a client works
	 * 
	 * Test method for {@link client.RMIClient#RMIClient(java.lang.String, int)}
	 * 
	 * @throws RemoteException
	 */
	@Test
	public void testRMIClient() throws RemoteException {
		assertNotNull(client);
	}

	/**
	 * Makes sure a client is able to login on the server
	 * 
	 * Test method for
	 * {@link client.RMIClient#login(java.lang.String, java.lang.String)}.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testLogin() throws Exception {
		assertEquals(true, client.login("test", "test"));

	}

	/**
	 * Makes sure the client login actually adds the username to the client
	 * 
	 * Test method for {@link client.RMIClient#getUsername()}.
	 * 
	 * @throws IOException
	 */
	@Test
	public void testGetUsername() throws IOException {
		assertEquals("test", client.getUsername());
	}

}
