Council Of Four

	by Nicolo' Boatto and Giorgio Capelli
	

- Before running the game it's necessary to install an Eclipse plug-in
 	which allows it to interpret standard ANSI escape codes and colorize the console output.
 	
 	The plug-in can be available at:
 
 	https://marketplace.eclipse.org/content/ansi-escape-console


- The Server can be started by running server.GameServer from Eclipse:
	
	It can be run without parameters and it will start a Game Server with all the default settings:
	
		- Socket Port : 6666
		- RMI Port : 1099
		- Time to take turn : 200 Seconds
	
	Or it can be run in a configuration with the same arguments passed as Strings, separated by a space.
	
	
- The Client can be started by running client.CommandLineHandler from Eclipse and it does not require any input parameter.


- The project implements the following features:
	
	- RMI
	
	 - Sockets
	
	- CLI
	
	- Customizable Map: The configuration files can be added to server/GameConfigurations
		(More information inside the configuration guidelines in server/GameConfigurations/Guidelines.txt)
	
	- User management (login information only)

	
	CLI Legend:
	
		Bonus:
			-h means Helper
			-v means Victory
			-n means Nobility
			-a means Main Action
			-m means Money
			-p means Political Card
			-s is the special bonus which allows you to get the bonus of a city where you own an emporium
			-g is the special bonus which allows you to pick up a Permit Card of your choosing
			-c is the special bonus which allows you to get the bonus of a Permit Card that you own
			
		Color:
			-k means BLACK
			-o means ORANGE
			-w means WHITE
			-p means PURPLE
			-b means BLUE
			-x means MULTICOLOR



	
	
	
	